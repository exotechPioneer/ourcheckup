//
//  D_W_M_Y_View.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 29/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

@IBDesignable
class D_W_M_Y_View: UIView {
    
    typealias D_W_M_Y_ViewAction = (SelectionType)->Void
    
    enum SelectionType {
        case daily
        case weekly
        case monthly
        case yearly
    }

    var contentView:UIView?
    var action : D_W_M_Y_ViewAction?
    @IBInspectable var selectedColor:UIColor?
    @IBInspectable var normalColor:UIColor?
    @IBInspectable var selectedTextColor:UIColor?
    
    @IBInspectable var normalTextColor : UIColor?
    
    var selectionType : SelectionType = .daily{
        didSet{
           // setSelection()
        }
    }
    
    @IBOutlet weak var btnDaily:UIButtonX!
    @IBOutlet weak var btnWeekly:UIButtonX!
    @IBOutlet weak var btnMonthly:UIButtonX!
    @IBOutlet weak var btnYearly:UIButtonX!
    
    @IBAction func btnDailyAction(_ sender:UIButton){
        selectionType = .daily
        setSelection()
    }
    
    @IBAction func btnWeeklyAction(_ sender:UIButton){
        selectionType = .weekly
        setSelection()
    }
    
    @IBAction func btnMonthlyAction(_ sender:UIButton){
        selectionType = .monthly
       setSelection()
    }
    
    @IBAction func btnYearlyAction(_ sender:UIButton){
        selectionType = .yearly
        setSelection()
    }
    
     func setSelection(){
        if let normalColor = normalColor{
            btnDaily.backgroundColor = normalColor
            btnWeekly.backgroundColor = normalColor
            btnMonthly.backgroundColor = normalColor
            btnYearly.backgroundColor = normalColor
            
        }
        if let normalTextColor = normalTextColor{
            btnDaily.setTitleColor(normalTextColor, for: .normal)
            btnWeekly.setTitleColor(normalTextColor, for: .normal)
            btnMonthly.setTitleColor(normalTextColor, for: .normal)
            btnYearly.setTitleColor(normalTextColor, for: .normal)
            
            btnYearly.borderColor = normalTextColor
            btnDaily.borderColor = normalTextColor
            btnWeekly.borderColor = normalTextColor
            btnMonthly.borderColor = normalTextColor
            btnYearly.borderColor = normalTextColor
        }
        
        if let selectedTextColor = selectedTextColor,let selectedColor = selectedColor{
            switch selectionType {
            case .daily:
                btnDaily.setTitleColor(selectedTextColor, for: .normal)
                btnDaily.backgroundColor = selectedColor
            case .weekly:
                btnWeekly.setTitleColor(selectedTextColor, for: .normal)
                btnWeekly.backgroundColor = selectedColor
            case .monthly:
                btnMonthly.setTitleColor(selectedTextColor, for: .normal)
                btnMonthly.backgroundColor = selectedColor
            case .yearly:
                btnYearly.setTitleColor(selectedTextColor, for: .normal)
                btnYearly.backgroundColor = selectedColor
            }
        }
        self.action?(selectionType)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
        setSelection()
    }
    
    func loadViewFromNib() -> UIView? {
         let nibName = String(describing: type(of: self)) //else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
}
