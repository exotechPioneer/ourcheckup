//
//  DateEXT.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 22/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
let formatter = DateFormatter()
extension Date {
    func dateComponents(components: Set<Calendar.Component> = [.day, .month, .year]) -> DateComponents {
        let calendar = Calendar.current
        return calendar.dateComponents(components, from: self)
    }
    
    func dateString(withFormat format: String = "yyyy-MM-dd") -> String {
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}


class CustomDateComponents{
    static let share = CustomDateComponents()
    
    var year:Int = 0
    var month: Int = 0
    var day : Int = 0
    var hours : Int = 0
    var minutes : Int = 0
    
    func get(dateString:String){
        let a1 = dateString.components(separatedBy: " ")
       
        if a1.count > 0{
            let a2 = a1[0].components(separatedBy: "-")
            year = Int(a2[0]) ?? 0
            month = Int(a2[1]) ?? 0
            day = Int(a2[2]) ?? 0
        }
        
        if a1.count > 1{
            let a3 = a1[1].components(separatedBy: ":")
            if a3.count > 0{
                hours = Int(a3[0]) ?? 0
                minutes = Int(a3[1]) ?? 0
            }
        }
    }
}
