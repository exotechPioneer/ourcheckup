//
//  CodeableEXT.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 15/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation
extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
