//
//  StringEXT.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation
import UIKit
extension String{
    func toNib() -> UINib {
        return UINib(nibName: self, bundle: nil)
    }
    func date(withFormat format: String = "yyyy-MM-dd") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
    
    func dateComponents(withFormat format: String = "yyyy-MM-dd", components: Set<Calendar.Component> = [.day, .month, .year]) -> DateComponents? {
        return self.date(withFormat: format)?.dateComponents(components: components)
    }
}
