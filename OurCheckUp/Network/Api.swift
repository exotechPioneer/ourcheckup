//
//  Api.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 06/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation
func showError(error:String?){
    if let err = error{
        displayAlert("Error!", andMessage: err)
    }else{
        displayAlert("Error!", andMessage: "empty error description")
    }
}
//MARK: To Data
func toData(response: AnyObject?)->Data?{
    if let response = response{
        do{
            let data = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
            return data
        }catch let error{
            showError(error: error.localizedDescription)
        }
        
    }
    
    return nil
}
//MARK: To class
func toClass<T:Codable>(to:T.Type,jsonData:Data)->T?{
    do{
        let result = try JSONDecoder().decode(to, from: jsonData)
        return result
    }catch{
        return nil
    }
    
}


class Api{
    
    static let share = Api()
    private init(){}
    private let httpClient = HTTPClient()
    
    let CONSUMER_KEY = "apikey"
    
    //MARK:- API URLS
    let BASE = "http://209.105.231.143"
    let PORT_8080 = "8080"
    
    let BASE_2 = "http://209.105.231.143:900/"
    
    var LOGIN_API : String{
        get{
            return BASE_2+"login"
        }
    }
    var SIGNUP_API : String{
        get{
            return BASE_2+"sign_up"
        }
    }
    
    var CREATE_DEVICE_DATA : String{
        get{
            return BASE_2+"create_device_data"
        }
    }
    
    var GET_DEVICE_DATA : String{
        get{
            return BASE_2+"get_device_data"
        }
    }
    
    var GET_LATEST_DEVICE_DATA :String{
        get{
            return BASE_2+"latest_device_data"
        }
    }
    
    var GET_MEDICAL_REPORT : String{
        get{
            return BASE_2+"get_medical_report"
        }
    }
    
    var GET_CVD_RISK : String{
        get{
            return BASE_2+"get_cvd_risk"
        }
    }
    
    var GET_LAST_SYNC_TIME : String{
        get{
            return BASE_2+"get_last_sync_time"
        }
    }
    
    var GET_MEDICAL_SUMMARY : String{
        get{
            return BASE_2+"get_medical_summary"
        }
    }
    
    var GET_PROGRESS_REPORT : String{
        get{
            return BASE_2+"get_progress_report"
        }
    }
    
    var GET_INITIAL_REPORT : String{
        get{
            return BASE_2+"get_doctor_initial_report"
        }
    }
    
    var CREATE_MEDICATION_SUMMARY:String {
        get{
            return BASE_2+"create_medical_dashboard_summary"
        }
    }
    
    var CREATE_PROGRESS_REPORT:String {
        get{
            return BASE_2+"create_progress_report"
        }
    }
    
    var GET_SUBSCRIBER_DETAILS : String{
        get{
            return BASE_2+"get_subscriber_details"
        }
    }
    
    var UPDATE_SUBSCRIBER_DETAILS : String{
        get{
            return BASE_2+"update_subscriber_details"
        }
    }
    
    var GET_DOCTOR_DETAILS : String{
        get{
            return BASE_2+"get_doctor_details"
        }
    }
    
    
    var UPDATE_DOCTOR_DETAILS : String{
        get{
            return BASE_2+"update_doctor_details"
        }
    }
    
    var OAUTH_ACCESS_TOKEN_API: String{
        get{
            return BASE+":"+PORT_8080+"/oauth/accessToken"
        }
    }
   
    let PATH_API = "/api/"
    
    let PATH_REPORTS = "reports/views/"
    
    let PATH_CHART = "/chart"
    
    func getBaseUrl(port:String? = nil)->String{
        if let port = port{
            return BASE+":"+port
        }else{
            return BASE+":"+PORT_8080
        }
    }
    
    private func postDeviceDataApi(port:String)->String{
        return getBaseUrl(port: port)+"/data"
    }
    
    var BODY_ANALYZER_POST_DATA :String{
        return  postDeviceDataApi(port: "8103")
    }
   
    var SMART_BAND_POST_DATA :String{
        return  postDeviceDataApi(port: "8102")
    }
    
    var BP_MONITOR_POST_DATA :String{
        return  postDeviceDataApi(port: "8101")
    }
    
    func USER_DETAIL(username:String)->String{
        return getBaseUrl()+PATH_API+"users/username/"+username
    }
    
    func getReportAPI(reportId: String)->String {
        return getBaseUrl()+PATH_API+PATH_REPORTS+reportId+PATH_CHART
    }
    
    let REPORTIDBPDAILY = "33-lpHNZeJl1z"
    let REPORTIDBPWEEKLY = "33-1SAfzSEURi"
    let REPORTIDBPMONTHLY = "33-ZJ1RPPUqQP"
    let REPORTIDBPYEARLY = "33-poiECndsPi"
    
    ///HR - Heart rate doctor module ///
    let reportIdHRDaily = "35-Y5rAotyIcq"
    let reportIdHRWeekly = "35-i42ebgJw3F"
    let reportIdHRMonthly = "35-SGcKhOaqGI"
    let reportIdHRYearly = "35-ywlWXXtEq9"
    
    ///SPO2 - doctor module ///
    let reportIdSPODaily = "64-4E1wbAIaaN"
    let reportIdSPOWeekly = "64-lRjzyKd2XA"
    let reportIdSPOMonthly = "64-lRjzyKd2XA"
    let reportIdSPOYearly = "64-sEgXxdjv09"
    
    ///steps taken - st doctor module ///
    let reportIdSTDaily = "63-QkA4gTpujK"
    let reportIdSTWeekly = "63-S5YLTD87qg"
    let reportIdSTMonthly = "63-icSh1mRws6"
    let reportIdSTYearly = "63-YyRaiYtZ8t"
    
    //Sleep
    let reportIdSleepDaily = "66-CAkbucYCz6"
    let reportIdSleepWeekly = "66-5Ves9hZGOW"
    let reportIdSleepMonthly = "66-Xi8gISovyS"
    let reportIdSleepYearly = "66-efhVrqjt7m"
    
    //Body weight
    let reportIdBodyWeightDaily = "65-2PW0l41O4f"
    let reportIdBodyWeightWeekly = "65-kQzsvGWRC3"
    let reportIdBodyWeightMonthly = "65-KDvoOPhL8Q"
    let reportIdBodyWeightYearly = "65-8r1G5aNCLm"
    
    //MARK:- Request Param helpers
    func generateNonce()->String{
        return "\(arc4random())bskjdslldjsjldsl"
    }
    func getTimeStamp()->String{
        return "\(Int64(Date().timeIntervalSince1970 * 1000)/1000)"
    }
    
    func nullResponseError(){
        showError(error: "Null response from server.")
    }
    
   
    
  
    //MARK:- Create Auth
    public func createOAuth_1_0( baseUrl:String,  authToken:String, consumerKey:String)->String{
        let authHeader = "OAuth realm=\""+baseUrl+"\",oauth_consumer_key=\""+consumerKey+"\"," +
            "oauth_token=\""+authToken+"\",oauth_timestamp=\""+getTimeStamp()+"\"," +
            "oauth_nonce=\""+generateNonce()+"\"";
        return authHeader;
    }
    
    //MARK:- Authorization header
    public func getAuthHeader()->[String:String]{
        //let authToken:String = AppPreferences.share.get(forkey: AppPreferences.keys.authToken)!
        return ["Authorization":createOAuth_1_0(baseUrl: getBaseUrl(),
                                                authToken: "authToken",
                                                consumerKey: CONSUMER_KEY)]
    }
    
    //MARK:- Basic Autherization header
    func getBasicAuthHeader()->[String:String]{
        let user:String = AppPreferences.share.get(forkey: .username)!
        let password:String = AppPreferences.share.get(forkey: .password)!
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        return headers
    }
    
    func getApplicationJsonHearder()->[String:String]{
        let headers = ["Content-Type":"application/json"]
        return headers
    }
    
    func getStatus(response:AnyObject?)->Bool{
        if let status = response?["status"] as? Bool{
            return status
        }else{
            return false
        }
    }
    func getMessage(response:AnyObject?)->String{
        if let message = response?["message"] as? String{
            return message
        }else{
            return ""
        }
    }
    
    //MARK: Login
    func doLogin(username:String, password:String,compeletion:@escaping (Bool)->Void){
       /* let params:[String:Any] = ["x_auth_username":username,
                      "x_auth_password":password,
                      "oauth_consumer_key":"apikey",
                      "oauth_nonce":generateNonce(),
                      "oauth_timestamp": getTimeStamp()]
        httpClient.setHeader(headers: [:])
        httpClient.requestMethod(method: .stringPost)
            .execute(url: OAUTH_ACCESS_TOKEN_API,parameters: {
                return params
            },onResponse: { (response) in
                if let rsp = response as? String{
                    if rsp.contains("oauth_token"){
                        let sept = rsp.components(separatedBy: "&")
                        let aouthToken = sept[0]
                        let aouthtokensecret = sept[1]
                        
                        let finalAouthToken = aouthToken.components(separatedBy: "=")[1]
                        let finalAouthtokensecret = aouthtokensecret.components(separatedBy: "=")[1]
                        
                        AppPreferences.share.commit(data: finalAouthToken,
                                                    forkey: .authToken)
                        AppPreferences.share.commit(data: finalAouthtokensecret,
                                                    forkey: .authSecret)
                        AppPreferences.share.commit(data: username,
                                                    forkey: .username)
                        compeletion(true)
                        
                    }else{
                        self.showError(error: rsp)
                    }
                }else{
                    self.nullResponseError()
                }
                compeletion(false)
            }) { (error) in
                compeletion(false)
                self.showError(error: error)
        }*/
        
        let params:[String:Any] = ["email":username,
                                   "password":password]
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: LOGIN_API, parameters: { () -> [String : Any]? in
                return params
            }, onResponse: { (response) in
                if self.getStatus(response: response){
                    if let data = response?["data"] as? [String:Any]{
                        let userId = data["user_id"] as! Int
                        let category_name = data["category_name"] as? String ?? "Subscriber"
                        let firstName = data["firstname"] as? String ?? ""
                        let lastName = data["lastname"] as? String ?? ""
                        let email = data["email"] as? String ?? ""
                        AppPreferences.share.commit(data: firstName, forkey: .userFirstName)
                        AppPreferences.share.commit(data: lastName, forkey: .userLastName)
                        AppPreferences.share.commit(data: email, forkey: .userEmail)
                        AppPreferences.share.commit(data: category_name , forkey: .userCategory)
                        AppPreferences.share.commit(data: userId , forkey: .userID)
                    }
                    compeletion(true)
                }else{
                    compeletion(false)
                    displayAlert("Error", andMessage: self.getMessage(response: response))
                }
            }) { (error) in
        }
    }
    
   
    func doSignUp(firstname:String,lastname:String,email:String, password:String,category:String,compeletion:@escaping (Bool)->Void){
        let params:[String:Any] = ["firstname":firstname,
                                   "lastname":lastname,
                                   "email":email,
                                   "password":password,
                                   "category_name":category]
        httpClient.setHeader(headers: ["Content-Type":"application/json"])
        httpClient.requestMethod(method: .httpPost)
            .execute(url: SIGNUP_API, parameters: { () -> [String : Any]? in
                return params
            }, onResponse: { (response) in
                if self.getStatus(response: response){
                    compeletion(true)
                }else{
                    compeletion(false)
                    displayAlert("Error", andMessage: self.getMessage(response: response))
                }
            }) { (error) in
                
        }
    }
    
    //MARK:- User detail with username
    func getUserDetails(username : String,completion:@escaping (User?)->Void){
        httpClient.setHeader(headers: getAuthHeader())
        httpClient.requestMethod(method: .get)
        .execute(url: USER_DETAIL(username: username),
                 onResponse: { (response) in
                    if let data = toData(response: response){
                        let user : User? = toClass(to: User.self, jsonData: data)
                        completion(user)
                    }else{
                        self.nullResponseError()
                    }
        }) { (error) in
            completion(nil)
            showError(error: error)
        }
    }
    
    //MARK:- Get Device data report
    func getDeviceReport(reportID : String,completion: @escaping (DeviceDataReport?)->Void){
        let url = getReportAPI(reportId: reportID)
        httpClient.setHeader(headers: getAuthHeader())
        httpClient.requestMethod(method: .get)
            .execute(url: url, onResponse: { (response) in
                if let data = toData(response: response){
                    //let str = String(data: data, encoding: String.Encoding.utf8)
                    //print("Report Response: ",str ?? "empty")
                    let modal:DeviceDataReport? = toClass(to: DeviceDataReport.self, jsonData: data)
                    completion(modal)
                }
            }) { (error) in
                completion(nil)
                showError(error: error)
        }
    }
    
    //MARK:- Send Bp data
    /*func sendBPData(model:iChoiceDataPostModel,compeletion:@escaping (Bool)->Void){
        httpClient.setHeader(headers: getBasicAuthHeader())
        httpClient.requestMethod(method: .httpStringPost)
            .execute(url: CREATE_DEVICE_DATA, parameters: { () -> [String : Any]? in
                    return  model.toDictionay()
            }, onResponse: { (response) in
                if let resp = response{
                    if ((resp as! String) == "200"){
                        compeletion(true)
                    }else{
                        compeletion(false)
                    }
                }else{
                    self.nullResponseError()
                }
            }) { (error) in
                self.showError(error: error)
        }
    }
    
    //MARK:- Send Bp data
    func sendBodyAnalyzerData(model:SmartBodyAnalyzerDataPostModel,compeletion:@escaping (Bool)->Void){
        httpClient.setHeader(headers: getBasicAuthHeader())
        httpClient.requestMethod(method: .httpStringPost)
            .execute(url: BODY_ANALYZER_POST_DATA, parameters: { () -> [String : Any]? in
                return  model.toDictionay()
            }, onResponse: { (response) in
                if let resp = response{
                    if ((resp as! String) == "200"){
                        compeletion(true)
                    }else{
                        compeletion(false)
                    }
                }else{
                    self.nullResponseError()
                }
            }) { (error) in
                self.showError(error: error)
        }
    }
    
    //MARK:- Send Bp data
    func sendSmartBandData(model:SmartBandDataPostModel,compeletion:@escaping (Bool)->Void){
        httpClient.setHeader(headers: getBasicAuthHeader())
        httpClient.requestMethod(method: .httpStringPost)
            .execute(url: SMART_BAND_POST_DATA, parameters: { () -> [String : Any]? in
                return  model.toDictionay()
            }, onResponse: { (response) in
                if let resp = response{
                    if (!(resp as! String).isEmpty){
                        compeletion(true)
                    }else{
                        compeletion(false)
                    }
                }else{
                    self.nullResponseError()
                }
            }) { (error) in
                self.showError(error: error)
        }
    }*/
    
    func postDeviceData(dict:[String:Any],compeletion:@escaping (Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: CREATE_DEVICE_DATA, parameters: { () -> [String : Any]? in
                return dict
            }, onResponse: { (response) in
                if let resp = response{
                    if let str = resp as? String{
                        if str.contains("true"){
                            print(str)
                            compeletion(true)
                        }else{
                            compeletion(false)
                            print(str)
                        }
                    }else{
                        if self.getStatus(response: resp){
                              print(self.getMessage(response: resp))
                            compeletion(true)
                        }else{
                            print(self.getMessage(response: resp))
                            compeletion(false)
                        }
                    }
                }else{
                    self.nullResponseError()
                }
            }) { (error) in
                showError(error: error)
        }
    }
    
    
   /* func getDeviceReportData2(params:[String:Any],completion:@escaping (DeviceHealthDataMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_DEVICE_DATA, parameters: { () -> [String : Any]? in
                return params
            }, onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = DeviceHealthDataMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
        
    }
    */
    func getBPData(params:[String:Any],completion:@escaping (BPGetDataMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_DEVICE_DATA, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = BPGetDataMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getLatestDevicesData(params:[String:Any],completion:@escaping (LatestDataMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_LATEST_DEVICE_DATA, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = LatestDataMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getMedicalReport(params:[String:Any],completion:@escaping (QuarterlyReportMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_MEDICAL_REPORT, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = QuarterlyReportMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getInitialReport(params:[String:Any],completion:@escaping (QuarterlyReportMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_INITIAL_REPORT, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = QuarterlyReportMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getLastSyncDateTime(params:[String:Any],completion:@escaping (Date?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_LAST_SYNC_TIME, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    if let data = dict["data"] as? NSArray{
                        if data.count > 0{
                            if let dic = data[0] as? [String:Any]{
                                if let dateStr = dic["date"] as? String{
                                   let date = dateStr.date(withFormat: "yyyy-MM-dd HH:mm:ss")
                                    completion(date,true)
                                }else{
                                     completion(nil,false)
                                }
                            }else{
                                 completion(nil,false)
                            }
                        }else{
                             completion(nil,false)
                        }
                    }else{
                         completion(nil,false)
                    }
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getCVDRisk(params:[String:Any],completion:@escaping (CVDRiskMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_CVD_RISK, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = CVDRiskMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getMedicalSummary(params:[String:Any],completion:@escaping (SubscriberMedicalSummaryMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_MEDICAL_SUMMARY, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = SubscriberMedicalSummaryMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getProgressReport(params:[String:Any],completion:@escaping (ProgressReportMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_PROGRESS_REPORT, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = ProgressReportMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func createMedicalSummary(params:[String:Any],completion:@escaping (String?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: CREATE_MEDICATION_SUMMARY, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                    completion(self.getMessage(response: response),self.getStatus(response: response))
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func createProgressReport(params:[String:Any],completion:@escaping (String?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: CREATE_PROGRESS_REPORT, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                completion(self.getMessage(response: response),self.getStatus(response: response))
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getSubscriberDetails(params:[String:Any],completion:@escaping (SubscriberDetailsMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_SUBSCRIBER_DETAILS, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = SubscriberDetailsMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func updateSubscriberDetails(params:[String:Any],completion:@escaping (String?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: UPDATE_SUBSCRIBER_DETAILS, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                completion(self.getMessage(response: response),self.getStatus(response: response))
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func getDoctorDetails(params:[String:Any],completion:@escaping (DoctorDetailsMdl?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: GET_DOCTOR_DETAILS, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                if let dict = response as? [String:Any] {
                    let result = DoctorDetailsMdl(fromDictionary: dict)
                    completion(result,true)
                }else{
                    self.nullResponseError()
                    completion(nil,false)
                }
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
    
    func updateDoctorDetails(params:[String:Any],completion:@escaping (String?,Bool)->Void){
        httpClient.setHeader(headers: getApplicationJsonHearder())
        httpClient.requestMethod(method: .httpPost)
            .execute(url: UPDATE_DOCTOR_DETAILS, parameters: { () -> [String : Any]? in
                return params
            },onResponse: { (response) in
                completion(self.getMessage(response: response),self.getStatus(response: response))
            }) { (error) in
                showError(error: error)
                completion(nil,false)
        }
    }
}

