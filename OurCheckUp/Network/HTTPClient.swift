//
//  ApiLinker.swift
//  Duziscan
//
//  Created by Kapil Dhawan on 14/10/17.
//  Copyright © 2017 Sachtech. All rights reserved.
//


import UIKit
import Alamofire

/*
 *Request Type Methods
 */
enum ApiMethod {
    case get
    case post
    case httpPost
    case httpStringPost
    case stringGet
    case stringPost
    
}



/*
 *Class For Server Tasks
 */
class HTTPClient: NSObject {
    
    
    fileprivate var requestCode:Int!
    fileprivate var requestMethod = ApiMethod.post
    fileprivate var url:String!
    fileprivate var headers = [String:String]()
    
    fileprivate var responseHeaderFields:[String:String]!
    
    override init() {
        
    }
    
    public func getHearders()->[String:String]{
        return responseHeaderFields
    }
    
    
    /*
     *Set Request Method Type to Cummunicate with server api
     */
    func requestMethod(method:ApiMethod)->HTTPClient{
        self.requestMethod = method
        return self
    }
    
    /*
     *Execute the task  acc. to current set parameters
     *Useful in case of post methods
     */
    func execute(url:String,parameters: () -> [String:Any]?,onResponse:@escaping (_ response:AnyObject?)->Void,onError:@escaping (_ error:String?)->Void){
        self.url = url

        let params = parameters()
        
        switch requestMethod {
        case .get:
            get(response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .post:
            post(params: params!, response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .httpPost:
            httpPost(params: params, response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .httpStringPost:
            httpStringPost(params: params, response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .stringGet:
            stringGet(response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .stringPost:
            stringPost(params: params, response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        }
        
        
    }
    
    /*
     *Execute the task  acc. to current set parameters
     *Useful in case of post and get methods
     */
    func execute(url:String,onResponse:@escaping (_ response:AnyObject?)->Void,onError:@escaping (_ error:String?)->Void){
        self.url = url
        
        switch requestMethod {
        case .get:
            get(response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .post:
            post(params: nil, response: { (response) in
                onResponse(response)
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .httpPost:
            httpPost(params: nil, response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
        case .httpStringPost:
            httpStringPost(params: nil, response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        case .stringGet:
            stringGet(response: { (response) in
                onResponse(response)
                
            }, error: { (error) in
                onError(error)
                
            })
            break
        default:
            break
        }
        
        
    }
    
    /*
     *Execute the task  acc. to current set parameters
     *Useful in case of post multipart request methods
     */
    func execute(url:String,image:UIImage,withName:String,parameters: @escaping () -> [String:Any],onResponse:@escaping (_ response:AnyObject?)->Void,onError:@escaping (_ error:String?)->Void){
        self.url = url
        
        DispatchQueue.global(qos: .background).async {
            self.postRequestMultipart(withName: withName,image: image, parameters: parameters(), response: { (resp) in
                onResponse(resp)
                
            }, error: { (err) in
                onError(err)
                
            })
            
        }
    }
    
    /*
     *For create get request to the server
     */
    fileprivate func get(response:@escaping (_ response:AnyObject?)->Void,error:@escaping (_ error:String?)->Void){
        Alamofire.request(url ,headers: headers)
            .responseJSON{ respons in
            if (respons.result.error == nil){
                let responseObject = respons.result.value
                response(responseObject as AnyObject)
                
            }else{
                error(respons.result.error.debugDescription)
                
            }
        }
    }
    fileprivate func stringGet(response:@escaping (_ response:AnyObject?)->Void,error:@escaping (_ error:String?)->Void){
        Alamofire.request(url ,headers: headers).responseString{resposne  in
            if (resposne.result.error == nil){
                let responseObject = resposne.result.value
                response(responseObject as AnyObject)
                
            }else{
                error(resposne.result.error.debugDescription)
                
            }
        }
    }
    
    /*
     *For create post request to the server
     */
    fileprivate func post(params:[String:Any]?,response:@escaping (_ response:AnyObject?)->Void,error:@escaping (_ error:String?)->Void){
        var parameters = [String:Any]()
        if let p = params{
            parameters = p
        }
        
        Alamofire.request(url,method:.post,parameters: parameters,/*encoding: JSONEncoding.default,*/headers:getHeaders()).responseString{response in
            
            }
            .responseJSON{ resp in
                
                if (resp.result.error == nil){
                    let responseObject = resp.result.value
                    
                    response(responseObject as AnyObject)
                }else{
                    error(resp.result.error.debugDescription)
                }
        }
    }
    
    /*
     *For create post request to the server
     */
    fileprivate func stringPost(params:[String:Any]?,response:@escaping (_ response:AnyObject?)->Void,error:@escaping (_ error:String?)->Void){
        var parameters = [String:Any]()
        if let p = params{
            parameters = p
        }
        
        Alamofire.request(url,method:.post,parameters: parameters,/*encoding: JSONEncoding.default,*/headers:getHeaders()).responseString{responses in
            
            
            if (responses.result.error == nil){
                let responseObject = responses.result.value
                
                response(responseObject as AnyObject)
            }else{
                error(responses.result.error.debugDescription)
            }
        }
        
    }
    
    /*
     *For create http post request to the server
     */
    fileprivate func httpPost(params:[String:Any]?,response:@escaping (_ response:AnyObject?)->Void,error:@escaping (_ error:String?)->Void){
        var parameters = [String:Any]()
        if let p = params{
            parameters = p
        }
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (resp) in
            
            if let headerFields = resp.response?.allHeaderFields as? [String: String]
            {
                self.responseHeaderFields = headerFields
            }
            
            switch resp.result {
            case .failure(let err):
                print(err)
                    error(err.localizedDescription)
            case .success(let responseObject):
                response(responseObject as AnyObject)
            }
        }
    }
    
    func httpPostArray(params:[[String:Any]]?,response:@escaping (_ response:AnyObject?)->Void,error:@escaping (_ error:String?)->Void){
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        var parameters = [[String:Any]]()
        if let p = params{
            parameters = p
        }
        
        let data = try! JSONSerialization.data(withJSONObject: parameters)
        request.httpBody = data
 
        Alamofire.request(request).responseJSON { (resp) in
            
            if let headerFields = resp.response?.allHeaderFields as? [String: String]
            {
                self.responseHeaderFields = headerFields
            }
            
            switch resp.result {
            case .failure(let err):
                print(err)
                error(err.localizedDescription)
                
            case .success(let responseObject):
                response(responseObject as AnyObject)
                
            }
        }
    }
    
    
    fileprivate func httpStringPost(params:[String:Any]?,response:@escaping (_ response:AnyObject?)->Void,error:@escaping (_ error:String?)->Void){
        var parameters = [String:Any]()
        if let p = params{
            parameters = p
        }
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString { (resp) in
            
            if let headerFields = resp.response?.allHeaderFields as? [String: String]
            {
                self.responseHeaderFields = headerFields
            }
            
            switch resp.result {
            case .failure(let err):
                print(err)
                error(err.localizedDescription)
                
            case .success(let responseObject):
                if (responseObject.isEmpty){
                     response("\(resp.response!.statusCode)" as AnyObject)
                }else{
                     response(responseObject as AnyObject)
                }
               
            }
        }
    }
    /**
     *Create multipart post request to server
     */
    fileprivate func postRequestMultipart(withName:String,image:UIImage!,parameters:[String:Any],response: @escaping (_ response:AnyObject)->Void,error:@escaping (_ error:String)->Void){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImageJPEGRepresentation(image, 0.5)!, withName: withName, fileName: "file0.jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data!(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    //print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { resp in
                    
                    
                    if let JSON = resp.result.value {
                        
                        response(JSON as AnyObject)
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
                error(encodingError.localizedDescription)
                
                
            }
            
        }
    }
    
    
    
    func download(downloadUrl:String,fileName:String,dirName:String, completion: ((Bool, String?) -> Void)?){
        
        
        let destination: DownloadRequest.DownloadFileDestination  = {_,_ in
            
            if(createDirectoryAtDocumentDirectory(dirName))
            {
                print("btk created")
            }else{
                print("error when creating btk")
                
            }
            
            /* let directoryUrl = getDocumentDirectoryStringPath().appendingFormat("/%@", dirName)
             let file = directoryUrl.appendingFormat("/%@", fileName)*/
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(dirName)
            let file = directoryURL.appendingPathComponent(fileName, isDirectory: false)
            
            return (file, [.createIntermediateDirectories, .removePreviousFile])
            
        }
        
        Alamofire.download(downloadUrl, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil, to:destination ).downloadProgress { (progress) in
            print(progress)
            }.responseString { (response) in
                
                if response.result.error == nil {
                    completion!(true,response.result.value)
                } else {
                    print(response.result.error!.localizedDescription)
                    completion!(false,nil)
                }
        }
    }
    
    
    
    
    func setHeader(headers:[String:String]){
        self.headers = headers
    }
    
    fileprivate func getHeaders() -> [String:String] {
        return headers
    }
    
    
    
}



