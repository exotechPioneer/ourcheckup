//
//  OxygenMeasurement.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "OxygenMeasurement.h"

@implementation OxygenMeasurement
@synthesize nPulse, nOxygen;

- (MEASUREMENT_TYPE)getType
{
	return MEASUREMENT_OXYGEN;
}

-(NSString*)toString
{
	return @"Oxygen measurement";
}

-(id)initWithOxygen:(double)oxygen andPulse:(double)pulse
{
	self = [super init];
	
	if (self == nil)
		return nil;

	self.nPulse = pulse;
	self.nOxygen = oxygen;
	self.date = [NSDate date];
		
	return self;
}


@end
