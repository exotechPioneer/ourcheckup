//
//  Measurement.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "Measurement.h"

@implementation Measurement

-(Boolean) getFlag:(int)flag From:(Byte)flags
{
	return (((flags >> flag) & 1) == 1);
}

- (MEASUREMENT_TYPE)getType
{
	return MEASUREMENT_NONE;
}

@end
