//
//  Device.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "Device.h"

@implementation Device

@synthesize record;

- (id)initWithRecord:(DeviceRecord*)_record
{
	self = [super init];
	
	if (self == nil)
		return nil;
	
	self.record = _record;
	
	return self;
}

-(void)NotifyPairResult:(bool)bPairedSuccessful
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDevicePaired" object:nil userInfo:@{@"Sender":@"Device", @"result":[NSNumber numberWithBool:bPairedSuccessful], @"DeviceAddress":record.address}];
}

-(void)Pair
{
	[self NotifyPairResult:YES];
}

- (void)Connect
{
	
}

- (void)Write:(NSData*)data toCharacteristic:(CBUUID*)charac toService:(CBUUID*)service
{
	//[ApplicationDelegate.m_BTcontroller Write:data toCharacteristic:charac toService:service toDevice:record];
}

@end
