//
//  WeightMeasurement.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "WeightMeasurement.h"

@implementation WeightMeasurement
@synthesize nWeight, nUserID, date, unitsKG, userIDPresent, timePresent;

- (MEASUREMENT_TYPE)getType
{
	return MEASUREMENT_WEIGHT;
}

-(id)initWithWeight:(double)weight
{
	self = [super init];
	
	if (self == nil)
		return nil;
	
	self.nWeight = weight;
	self.nUserID = -1;
	self.date = [[NSDate alloc] init];
	self.unitsKG = true;
	self.userIDPresent = false;
	self.timePresent = false;

	return self;
}

@end
