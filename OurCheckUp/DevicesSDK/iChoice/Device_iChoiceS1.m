//
//  Device_iChoiceS1.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "Device_iChoiceS1.h"
#import "GattAttributes.h"
#import "Utility.h"
#import "WeightMeasurement.h"

@implementation Device_iChoiceS1

- (void)Connect
{
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnConnected:)
												 name:@"OnDeviceConnected"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnIncomingData:)
												 name:@"OnIncomingData"
											   object:nil];
	
	[super Connect];
}

- (void)OnConnected:(NSNotification*)notification
{
	NSLog(@"Device_iChoiceS1::OnDeviceConnected()");
	
	if (![Utility CheckNotification:notification ToBeFrom:@"ConnectionManager"])
	{
		return;
	}
	
	DeviceRecord* inputRecord = [notification object];
	
	if ([self.record.address isEqual:inputRecord.address])
	{
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			[self RequestPass];
		});
	}
}

- (void)OnIncomingData:(NSNotification*)notification
{
	CBPeripheral* peripheral = [notification object];
	
	if ([self.record.address isEqual:peripheral.identifier] && notification.userInfo != nil)
	{
		NSData* data = [notification.userInfo objectForKey:@"Data"];
		if (data != nil)
		{
			[self ProcessData: data];
		}
	}
	
}

- (void)ProcessData:(NSData*)data
{
	Byte* bytes = (Byte*)[data bytes];//55 AA 05 40 01 C0 00 06 //Weight received = 49152
	
	if ([data length] == 8 && bytes[0] == 0x55 && bytes[1] == 0xAA && bytes[2] == 0x05 && bytes[3] == 0x40) // measurement package
	{
		double nWeight = (double)(((int)bytes[6] << 8) + bytes[5]) / 10;
		
		NSLog(@"Weight received = %f", nWeight);
		
		WeightMeasurement* measurement = [[WeightMeasurement alloc] initWithWeight:nWeight];

		[[NSNotificationCenter defaultCenter] postNotificationName:@"OnIncomingMeasurement" object:measurement];
	}
}

- (void)RequestPass
{
	NSLog(@"Device_iChoiceS1::RequestPass()");
	Byte cmd[7] = {0xAA, 0x55, 0x04, 0xB1, 0x00, 0x00, 0xB5 };
	NSData* data = [NSData dataWithBytes:cmd length:sizeof(cmd)];
	CBUUID* uuid = [CBUUID UUIDWithString:GUID_CHOICE_WRITE_CHARACTERISTIC];
	[self Write:data toCharacteristic:uuid toService:nil];
}

@end
