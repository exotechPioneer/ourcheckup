//
//  DeviceRecord.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

typedef enum { DEVICE_TYPE_NONE, DEVICE_TYPE_WEIGHT_SCALE, DEVICE_TYPE_BPM, DEVICE_TYPE_OXIMETER } DEVICE_TYPE;

@interface DeviceRecord : NSObject<NSCoding>

@property (strong, nonatomic) NSString  *name;
@property (strong, nonatomic) NSUUID    *address;
@property (strong, nonatomic) NSDate    *lastReadingReceivedDate;
@property (strong, nonatomic) NSString	*lastConnectedDate;
@property (strong, nonatomic) NSString	*deviceID;
@property (nonatomic) DEVICE_TYPE m_deviceType;

+ (NSString*)GetPictureNameForDeviceType:(DEVICE_TYPE)type;
+ (NSString*)GetRoundPictureNameForDeviceType:(DEVICE_TYPE)type;

- (id)initWithName:(NSString*)name andAddress:(NSUUID*)address andDeviceType:(DEVICE_TYPE)type;
- (NSString*)GetPictureName;
- (NSString*)GetRoundPictureName;
- (NSString*)GetDeviceDisplayName;

@end

@interface DeviceConnectionRecord : DeviceRecord

@property (strong, nonatomic) CBPeripheral* targetPeripheral;

- (id)initWithName:(NSString*)name andAddress:(NSUUID*)address andPeripheral:(CBPeripheral*)peripheral;

@end
