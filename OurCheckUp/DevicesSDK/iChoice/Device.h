//
//  Device.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import <Foundation/Foundation.h>
//#import "AppDelegate.h"
#import "DeviceRecord.h"

@interface Device : NSObject

@property (strong, nonatomic) DeviceRecord  *record;

- (id)initWithRecord:(DeviceRecord*)_record;

- (void)Pair;
- (void)Connect;
- (void)Write:(NSData*)data toCharacteristic:(CBUUID*)charac toService:(CBUUID*)service;

@end
