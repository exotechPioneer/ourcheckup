//
//  GattAttributes.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#ifndef GattAttributes_h
#define GattAttributes_h

#define CLIENT_CHARACTERISTIC_CONFIG   @"00002902-0000-1000-8000-00805f9b34fb"

#define GUID_CHOICE_WRITE_CHARACTERISTIC    @"0000CD20-0000-1000-8000-00805F9B34FB"
#define GUID_CHOICE_READ_CHARACTERISTIC_1	@"0000CD01-0000-1000-8000-00805F9B34FB"
#define GUID_CHOICE_READ_CHARACTERISTIC_2	@"0000CD02-0000-1000-8000-00805F9B34FB"
#define GUID_CHOICE_READ_CHARACTERISTIC_3	@"0000CD03-0000-1000-8000-00805F9B34FB"
#define GUID_CHOICE_READ_CHARACTERISTIC_4	@"0000CD04-0000-1000-8000-00805F9B34FB"

#endif /* GattAttributes_h */
