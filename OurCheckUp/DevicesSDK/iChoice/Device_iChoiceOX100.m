//
//  Device_iChoiceOX100.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "Device_iChoiceOX100.h"
#import "GattAttributes.h"
#import "Utility.h"
#import "OxygenMeasurement.h"

@interface Device_iChoiceOX100 ()

@property (nonatomic) BOOL m_bIsPairing;
@property (nonatomic) BOOL m_bWaitingForData;

@end

@implementation Device_iChoiceOX100

@synthesize m_bIsPairing, m_bWaitingForData;

- (void)Connect
{
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnConnected:)
												 name:@"OnDeviceConnected"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnIncomingData:)
												 name:@"OnIncomingData"
											   object:nil];
	
	m_bWaitingForData = NO;
	[super Connect];
}

//- (void)Pair
//{
//	m_bIsPairing = YES;
//	
//	[[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(OnConnected:)
//												 name:@"OnDeviceConnected"
//											   object:nil];
//
//	[[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(OnIncomingData:)
//												 name:@"OnIncomingData"
//											   object:nil];
//	
//	[self Connect];
//}

- (void)OnConnected:(NSNotification*)notification
{
	NSLog(@"Device_iChoiceOX100::OnDeviceConnected()");
	
	if (![Utility CheckNotification:notification ToBeFrom:@"ConnectionManager"])
	{
		return;
	}
	
	DeviceRecord* inputRecord = [notification object];
	
	if ([self.record.address isEqual:inputRecord.address])
	{
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			[self RequestPass];
		});
	}
}

- (void)OnIncomingData:(NSNotification*)notification
{
	CBPeripheral* peripheral = [notification object];
	
	if ([self.record.address isEqual:peripheral.identifier] && notification.userInfo != nil)
	{
		NSData* data = [notification.userInfo objectForKey:@"Data"];
		if (data != nil)
		{
			[self ProcessData: data];
		}
	}

}

- (void)ProcessData:(NSData*)data
{
	Byte* bytes = (Byte*)[data bytes];
	if ([data length] == 6 && bytes[0] == 0x55 && bytes[1] == 0xAA && bytes[2] == 0x03)
	{
		if (m_bWaitingForData)
		{
			int nOxygen = bytes[3];
			int nPulse = bytes[4];
			
			OxygenMeasurement* measurement = [[OxygenMeasurement alloc] initWithOxygen:nOxygen andPulse:nPulse];
			
			[[NSNotificationCenter defaultCenter] postNotificationName:@"OnIncomingMeasurement" object:measurement];
			
			m_bWaitingForData = false;
		}
		else
		{
			[self RequestDeviceID];
		}
	}
}

- (void)RequestPass
{
	NSLog(@"Device_iChoiceOX100::RequestPass()");
	Byte cmd[7] = {0xAA, 0x55, 0x04, 0xB1, 0x00, 0x00, 0xB5 };
	NSData* data = [NSData dataWithBytes:cmd length:sizeof(cmd)];
	CBUUID* uuid = [CBUUID UUIDWithString:GUID_CHOICE_WRITE_CHARACTERISTIC];
	[self Write:data toCharacteristic:uuid toService:nil];
}

- (void)RequestDeviceID
{
	NSLog(@"Device_iChoiceOX100::RequestDeviceID()");
	
	m_bWaitingForData = YES;
	
	Byte cmd[5] = {0xAA, 0x55, 0x02, 0xC0, 0xC2};
	NSData* data = [NSData dataWithBytes:cmd length:sizeof(cmd)];
	CBUUID* uuid = [CBUUID UUIDWithString:GUID_CHOICE_WRITE_CHARACTERISTIC];
	[self Write:data toCharacteristic:uuid toService:nil];
}

@end
