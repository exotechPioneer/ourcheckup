//
//  WeightMeasurement.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "Measurement.h"

@interface WeightMeasurement : Measurement

@property (nonatomic) double nWeight; // with precision of 2 decimal places

// Measurement flags
@property Boolean unitsKG;		 // Units in kg (true) or lbs (false)

-(id)initWithWeight:(double)weight;

@end
