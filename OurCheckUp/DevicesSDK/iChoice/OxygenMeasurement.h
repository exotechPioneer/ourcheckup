//
//  OxygenMeasurement.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import <Foundation/Foundation.h>
#import "Measurement.h"

@interface OxygenMeasurement : Measurement

@property (nonatomic) double nOxygen; // Measurement oxygen
@property (nonatomic) double nPulse;  // Measurement pulse

-(id)initWithOxygen:(double)oxygen andPulse:(double)pulse;

@end
