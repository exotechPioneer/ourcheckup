//
//  Device_iChoiceBP1.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "Device_iChoiceBP1.h"
#import "GattAttributes.h"
#import "Utility.h"
#import "BloodPressureMeasurement.h"

@implementation Device_iChoiceBP1

- (void)Connect
{
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnConnected:)
												 name:@"OnDeviceConnected"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnIncomingData:)
												 name:@"OnIncomingData"
											   object:nil];
	
	[super Connect];
}

- (void)OnConnected:(NSNotification*)notification
{
	NSLog(@"Device_iChoiceBP1::OnDeviceConnected()");
	
	if (![Utility CheckNotification:notification ToBeFrom:@"ConnectionManager"])
	{
		return;
	}
	
	DeviceRecord* inputRecord = [notification object];
	
	if ([self.record.address isEqual:inputRecord.address])
	{
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			[self RequestPass];
		});
	}
}

- (void)OnIncomingData:(NSNotification*)notification
{
	CBPeripheral* peripheral = [notification object];
	
	if ([self.record.address isEqual:peripheral.identifier] && notification.userInfo != nil)
	{
		NSData* data = [notification.userInfo objectForKey:@"Data"];
		if (data != nil)
		{
			[self ProcessData: data];
		}
	}
	
}

- (void)ProcessData:(NSData*)data
{
	Byte* bytes = (Byte*)[data bytes];
	if ([data length] == 6 && bytes[0] == 0x55 && bytes[1] == 0xAA && bytes[2] == 0x03) // answer on pass request - success
	{
		[self RequestMeasurements];
	}
	else if ([data length] == 12 && bytes[0] == 0x55 && bytes[1] == 0xAA && bytes[2] == 0x09 && bytes[3] == 0x34) // measurement package
	{
		int nSys = ((int)bytes[4] << 8) + bytes[5];
		int nDia = ((int)bytes[6] << 8) + bytes[7];
		int nPulse = ((int)bytes[8] << 8) + bytes[9];
		
		BloodPressureMeasurement* measurement = [[BloodPressureMeasurement alloc] initWithSys:nSys andDia:nDia andPulse:nPulse];
		measurement.pulsePresent = YES;
		
		NSLog(@"Incoming BP measurement %@", [measurement toString]);
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"OnIncomingMeasurement" object:measurement];
	}
}

- (void)RequestPass
{
	NSLog(@"Device_iChoiceBP1::RequestPass()");
	Byte cmd[7] = {0xAA, 0x55, 0x04, 0xB1, 0x00, 0x00, 0xB5 };
	NSData* data = [NSData dataWithBytes:cmd length:sizeof(cmd)];
	CBUUID* uuid = [CBUUID UUIDWithString:GUID_CHOICE_WRITE_CHARACTERISTIC];
	[self Write:data toCharacteristic:uuid toService:nil];
}

- (void)RequestMeasurements
{
	NSLog(@"Device_iChoiceBP1::RequestMeasurements()");
	Byte cmd[6] = {0xAA, 0x55, 0x03, 0x30, 0x00, 0x33 };
	NSData* data = [NSData dataWithBytes:cmd length:sizeof(cmd)];
	CBUUID* uuid = [CBUUID UUIDWithString:GUID_CHOICE_WRITE_CHARACTERISTIC];
	[self Write:data toCharacteristic:uuid toService:nil];
}

@end
