//
//  ConnectionManager.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "ConnectionManager.h"
//#import "AppDelegate.h"
#import "BluetoothController.h"
#import "BloodPressureMeasurement.h"
#import "WeightMeasurement.h"
#import "OxygenMeasurement.h"
#import "Utility.h"
#import "Device_iChoiceOX100.h"
#import "Device_iChoiceBP1.h"
#import "Device_iChoiceS1.h"


@interface ConnectionManager ()
@property (strong, nonatomic) NSMutableDictionary* m_devices;
@end

@implementation ConnectionManager

NSDate* lastReceivedReadingDate;
NSDate* savedLastReceivedReadingDate;

- (id)init
{
	NSLog(@"ConnectionManager::init()");
	
	self = [super init];

	if (self)
	{
		//Init stuff
		[self RegisterForNotifications];
		
		lastReceivedReadingDate = [NSDate dateWithTimeIntervalSince1970:0];
		savedLastReceivedReadingDate = [NSDate dateWithTimeIntervalSince1970:0];
		self.m_devices = [[NSMutableDictionary alloc] init];
	}
	return self;
}

- (void)RegisterForNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnDeviceAdded:)
												 name:@"OnDeviceAdded"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnDeviceRemoved:)
												 name:@"OnDeviceRemoved"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnIncomingMeasurement:)
												 name:@"OnIncomingMeasurement"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnDeviceDisconnected:)
												 name:@"OnDeviceDisconnected"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnDeviceConnected:)
												 name:@"OnDeviceConnected"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(OnDeviceSystemID:)
												 name:@"OnDeviceSystemID"
											   object:nil];
}

- (void)UnregisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)OnDeviceAdded:(NSNotification*)notification
{
	NSLog(@"ConnectionManager::OnDeviceAdded()");
	
	DeviceRecord* record = [notification object];
	
	[self ConnectDevice:record];
}

// Create specific device and connect to it
- (void)ConnectDevice:(DeviceRecord*)record
{
	if (record.m_deviceType == DEVICE_TYPE_OXIMETER)
	{
		Device_iChoiceOX100* device = [[Device_iChoiceOX100 alloc] initWithRecord:record];
		[_m_devices setObject:device forKey:record.address];
		[device Connect];
	}
	else if (record.m_deviceType == DEVICE_TYPE_BPM)
	{
		Device_iChoiceBP1* device = [[Device_iChoiceBP1 alloc] initWithRecord:record];
		[_m_devices setObject:device forKey:record.address];
		[device Connect];
	}
	else if (record.m_deviceType == DEVICE_TYPE_WEIGHT_SCALE)
	{
		Device_iChoiceS1* device = [[Device_iChoiceS1 alloc] initWithRecord:record];
		[_m_devices setObject:device forKey:record.address];
		[device Connect];
	}
}

- (void)OnDeviceRemoved:(NSNotification*)notification
{
	NSLog(@"ConnectionManager::OnDeviceRemoved()");
	
	//DeviceRecord* record = [notification object];
	
	//[ApplicationDelegate.m_BTcontroller DisconnectDevice:record];
}

- (void)OnDeviceFound:(NSNotification*)notification
{
	NSLog(@"ConnectionManager::OnDeviceFound()");
}

- (void)OnDeviceSystemID:(NSNotification*)notification
{
	NSLog(@"ConnectionManager::OnDeviceSystemID()");
	
	if (![Utility CheckNotification:notification ToBeFrom:@"BluetoothController"])
	{
		return;
	}
	
	CBPeripheral* peripheral = [notification object];
	
	if (peripheral == nil)
	{
		NSLog(@"[E]No peripheral");
		return;
	}
	
	/*NSInteger indx = [ApplicationDelegate.m_savedDevicesList indexOfObjectPassingTest:^BOOL(DeviceRecord *record, NSUInteger idx, BOOL *stop) {
		return [record.address isEqual:peripheral.identifier];
	}];
	
	if (indx != NSNotFound)
	{
		NSDictionary* userInfo = notification.userInfo;
		
		if (userInfo != nil)
		{
			NSString* stringID = [userInfo objectForKey:@"SystemID"];
			
			//DeviceRecord* record = [ApplicationDelegate.m_savedDevicesList objectAtIndex:indx];
			
			//record.deviceID = stringID;
			//[ApplicationDelegate.m_savedDevicesList replaceObjectAtIndex:indx withObject:record];
			
			NSLog(@"Device deviceID = %@", record.deviceID);
		}
		else
		{
			NSLog(@"[E]User info not found");
		}
	}
	else
	{
		NSLog(@"[E]Device record not found");
	}*/

}

- (void)OnDeviceConnected:(NSNotification*)notification
{
	NSLog(@"ConnectionManager::OnDeviceConnected()");
	
	if (![Utility CheckNotification:notification ToBeFrom:@"BluetoothController"])
	{
		return;
	}
	
	CBPeripheral* peripheral = [notification object];
	
	if (peripheral == nil)
	{
		NSLog(@"[E]No peripheral");
		return;
	}
	
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceConnected_BP" object:peripheral];
    
/*	NSInteger indx = [ApplicationDelegate.m_savedDevicesList indexOfObjectPassingTest:^BOOL(DeviceRecord *record, NSUInteger idx, BOOL *stop) {
		return [record.address isEqual:peripheral.identifier];
	}];
	
	if (indx != NSNotFound)
	{
		DeviceRecord* record = [ApplicationDelegate.m_savedDevicesList objectAtIndex:indx];
		savedLastReceivedReadingDate = record.lastReadingReceivedDate;
		
		record.lastConnectedDate = @"Now";
		[ApplicationDelegate.m_savedDevicesList replaceObjectAtIndex:indx withObject:record];
		
		NSLog(@"Device savedLastReceivedReadingDate = %@", savedLastReceivedReadingDate);
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceConnected" object:record userInfo:@{@"Sender":@"ConnectionManager"}];
	}
	else
	{
		NSLog(@"[E]Device record not found");
	}*/
}

- (void)OnDeviceDisconnected:(NSNotification*)notification
{
	NSLog(@"ConnectionManager::OnDeviceDisconnected()");
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceDisConnected_BP" object:nil];
	if (![Utility CheckNotification:notification ToBeFrom:@"BluetoothController"])
	{
		return;
	}
	
	//CBPeripheral* peripheral = [notification object];
	
	/*if (peripheral != nil)
	{
		NSInteger indx = [ApplicationDelegate.m_savedDevicesList indexOfObjectPassingTest:^BOOL(DeviceRecord *record, NSUInteger idx, BOOL *stop) {
			return [record.address isEqual:peripheral.identifier];
		}];
		
		if (indx != NSNotFound)
		{
			DeviceRecord* device = [ApplicationDelegate.m_savedDevicesList objectAtIndex:indx];
			
			//Save new time
			device.lastReadingReceivedDate = lastReceivedReadingDate;
			
			device.lastConnectedDate = [NSDateFormatter localizedStringFromDate:[NSDate date]
																	  dateStyle:NSDateFormatterShortStyle
																	  timeStyle:NSDateFormatterShortStyle];
			
			NSLog(@"Device save new date: savedLastReceivedReadingDate = %@, lastconnectedTime = %@", lastReceivedReadingDate, device.lastConnectedDate);
			
			[ApplicationDelegate.m_savedDevicesList replaceObjectAtIndex:indx withObject:device];
			
			//ConnectAgain
			[self ConnectDevice:device];
			
			[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceDisconnected" object:device userInfo:@{@"Sender":@"ConnectionManager"}];
		}
		else
		{
			NSLog(@"[E]Device record not found");
		}
	}*/
}

- (void)OnIncomingMeasurement:(NSNotification*)notification
{
	NSLog(@"ConnectionManager::OnIncomingMeasurement()");
	
	Measurement* measurement = [notification object];
	
	if (measurement == nil)
	{
		NSLog(@"[E]Measurement is nil");
		return;
	}
	
	if ([measurement getType] == MEASUREMENT_NONE)
	{
		NSLog(@"[E]Error: unknown measurement type");
		return;
	}
	
	NSLog(@"LastReadingDate = %@, local last received = %@, now = %@", savedLastReceivedReadingDate, lastReceivedReadingDate, [NSDate date]);
	
	if ((savedLastReceivedReadingDate == nil || [measurement.date compare:savedLastReceivedReadingDate] == NSOrderedDescending)		// Filter already got readings
		&& [measurement.date compare:[NSDate date]] != NSOrderedDescending)							// Filter readings from future; NB! possible problems with timezones
	{
		NSLog(@"Save reading");
		
		// Save last get measurement data for future
		if ([measurement.date compare:lastReceivedReadingDate] == NSOrderedDescending)
		{
			lastReceivedReadingDate = measurement.date;
		}
		
		// Pass measurement to HealthKit
		[self SaveMeasurement:measurement];
	}
}

- (void)SaveMeasurement:(Measurement*)measurement
{
	if ([measurement getType] == MEASUREMENT_BP)
	{
		BloodPressureMeasurement* reading = (BloodPressureMeasurement*)measurement;
		NSLog(@"ConnectionManager::Incoming measurement: sys = %zd, dia = %zd, pulse = %zd, date = %@", reading.nSysBP, reading.nDiaBP, reading.nPulse, reading.date);
		[[NSNotificationCenter defaultCenter] postNotificationName:@"OnIncomingBPData" object:reading];
		[self saveBloodPressureMeasurement:reading];
	}
	else if ([measurement getType] == MEASUREMENT_WEIGHT)
	{
		WeightMeasurement* reading = (WeightMeasurement*)measurement;
		NSLog(@"ConnectionManager::Incoming measurement: weight = %f, isKG = %d, date = %@", reading.nWeight, reading.unitsKG, reading.date);
		
		[self SaveWeightMeasurement:reading];
	}
	else if ([measurement getType] == MEASUREMENT_OXYGEN)
	{
		OxygenMeasurement* reading = (OxygenMeasurement*)measurement;
		NSLog(@"ConnectionManager::Incoming measurement: oxygen = %f, pulse = %f, date = %@", reading.nOxygen, reading.nPulse, reading.date);
		
		[self SaveOxygenMeasurement:reading];
	}
}

- (void)SaveOxygenMeasurement:(OxygenMeasurement*)measurement
{
	/*if ([HKHealthStore isHealthDataAvailable]) {
		HKHealthStore * healthStore = ApplicationDelegate.healthStore;
		
		HKQuantityType *oxygenSaturationType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierOxygenSaturation];
		HKQuantityType *pulseType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
		HKAuthorizationStatus statusOxygen = [healthStore authorizationStatusForType:oxygenSaturationType];
		HKAuthorizationStatus statusPulse = [healthStore authorizationStatusForType:pulseType];
		
		if (statusOxygen == HKAuthorizationStatusSharingAuthorized
			&& statusPulse == HKAuthorizationStatusSharingAuthorized)
		{
			[self SaveOxygenSaturationIntoHealthStoreWithOxygen:measurement.nOxygen WithPulse:measurement.nPulse WithDate:measurement.date WithHealthStore:healthStore];
		}
		else
		{
			NSSet *shareTypes = [NSSet setWithObjects:oxygenSaturationType, pulseType, nil];
			NSSet *readTypes = [NSSet new];
			
			[healthStore requestAuthorizationToShareTypes:shareTypes readTypes:readTypes
											   completion:^(BOOL success, NSError *error) {
												   if (!success) {
													   NSLog(@"An error occured getting HealthKit authorization. The error was: %@.", error);
													   return;
												   }
												   
												   NSLog(@"Access granted. Saving data...");
												   [self SaveOxygenSaturationIntoHealthStoreWithOxygen:measurement.nOxygen WithPulse:measurement.nPulse WithDate:measurement.date WithHealthStore:healthStore];
											   }
			 ];
		}
		
	}
	*/
}

/*- (void)SaveOxygenSaturationIntoHealthStoreWithOxygen:(double)oxygen WithPulse:(double)pulse WithDate:(NSDate*)date WithHealthStore:(HKHealthStore *)healthStore {
	HKUnit *OxygenUnit = [HKUnit percentUnit];
	HKUnit *PulseUnit = [[HKUnit countUnit] unitDividedByUnit:[HKUnit minuteUnit]];
	
	HKQuantity *OxygenQuantity = [HKQuantity quantityWithUnit:OxygenUnit doubleValue:oxygen * 10];
	HKQuantity *PulseQuantity = [HKQuantity quantityWithUnit:PulseUnit doubleValue:pulse];
	
	HKQuantityType *OxygenType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierOxygenSaturation];
	HKQuantityType *PulseType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
	
	HKQuantitySample *OxygenSample = [HKQuantitySample quantitySampleWithType:OxygenType quantity:OxygenQuantity startDate:date endDate:date];
	HKQuantitySample *PulseSample = [HKQuantitySample quantitySampleWithType:PulseType quantity:PulseQuantity startDate:date endDate:date];
	
	NSArray *objects=[NSArray arrayWithObjects:OxygenSample,PulseSample, nil];
	
	[healthStore saveObjects:objects withCompletion:^(BOOL success, NSError *error) {
		if (!success) {
			NSLog(@"An error occured saving the oxygen sample %@. The error was: %@.", OxygenSample, error);
			return;
		}
		NSLog(@"Data saved to HealthKit.");
	}];
}*/

- (void)saveBloodPressureMeasurement:(BloodPressureMeasurement*)measurement
{
	/*if ([HKHealthStore isHealthDataAvailable]) {
		HKHealthStore * healthStore = ApplicationDelegate.healthStore;
		
		HKQuantityType *bpSystolicType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodPressureSystolic];
		HKQuantityType *bpDiastolicType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodPressureDiastolic];
		HKAuthorizationStatus statusBPSys = [healthStore authorizationStatusForType:bpSystolicType];
		HKAuthorizationStatus statusBPDia = [healthStore authorizationStatusForType:bpDiastolicType];
		
		if (statusBPSys == HKAuthorizationStatusSharingAuthorized
			&& statusBPDia == HKAuthorizationStatusSharingAuthorized)
		{
			[self SaveBloodPressureIntoHealthStoreWithSys:measurement.nSysBP WithDia:measurement.nDiaBP WithDate:measurement.date WithHealthStore:healthStore];
		}
		else
		{
			NSSet *shareTypes = [NSSet setWithObjects:bpSystolicType, bpDiastolicType, nil];
			NSSet *readTypes = [NSSet new];
			
			[healthStore requestAuthorizationToShareTypes:shareTypes readTypes:readTypes
											   completion:^(BOOL success, NSError *error) {
												   if (!success) {
													   NSLog(@"An error occured getting HealthKit authorization. The error was: %@.", error);
													   return;
												   }
												   
												   NSLog(@"Access granted. Saving data...");
												   [self SaveBloodPressureIntoHealthStoreWithSys:measurement.nSysBP WithDia:measurement.nDiaBP WithDate:measurement.date WithHealthStore:healthStore];
											   }
			 ];
		}
		
	}*/

}

/*- (void)SaveBloodPressureIntoHealthStoreWithSys:(double)Systolic WithDia:(double)Diastolic WithDate:(NSDate*)date WithHealthStore:(HKHealthStore *)healthStore {
	HKUnit *BloodPressureUnit = [HKUnit millimeterOfMercuryUnit];
	
	HKQuantity *SystolicQuantity = [HKQuantity quantityWithUnit:BloodPressureUnit doubleValue:Systolic];
	HKQuantity *DiastolicQuantity = [HKQuantity quantityWithUnit:BloodPressureUnit doubleValue:Diastolic];
	
	HKQuantityType *SystolicType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodPressureSystolic];
	HKQuantityType *DiastolicType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodPressureDiastolic];
	
	HKQuantitySample *SystolicSample = [HKQuantitySample quantitySampleWithType:SystolicType quantity:SystolicQuantity startDate:date endDate:date];
	HKQuantitySample *DiastolicSample = [HKQuantitySample quantitySampleWithType:DiastolicType quantity:DiastolicQuantity startDate:date endDate:date];
	
	NSSet *objects=[NSSet setWithObjects:SystolicSample,DiastolicSample, nil];
	HKCorrelationType *bloodPressureType = [HKObjectType correlationTypeForIdentifier:
											HKCorrelationTypeIdentifierBloodPressure];
	HKCorrelation *BloodPressure = [HKCorrelation correlationWithType:bloodPressureType startDate:date endDate:date objects:objects];
	
	[healthStore saveObject:BloodPressure withCompletion:^(BOOL success, NSError *error) {
		if (!success) {
			NSLog(@"An error occured saving the BP sample %@. The error was: %@.", BloodPressure, error);
			return;
		}
		NSLog(@"Data saved to HealthKit.");
	}];
}*/

- (void)SaveWeightMeasurement:(WeightMeasurement*)measurement
{
	/*if ([HKHealthStore isHealthDataAvailable]) {
		HKHealthStore * healthStore = ApplicationDelegate.healthStore;
		
		HKQuantityType *weightType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
		HKAuthorizationStatus statusWeight = [healthStore authorizationStatusForType:weightType];
		
		if (statusWeight == HKAuthorizationStatusSharingAuthorized)
		{
			[self SaveWeightIntoHealthStoreWithWeight:measurement.nWeight WithDate:measurement.date IsMetric:measurement.unitsKG WithHealthStore:healthStore];
		}
		else
		{
			NSSet *shareTypes = [NSSet setWithObjects:weightType, nil];
			NSSet *readTypes = [NSSet new];
			
			[healthStore requestAuthorizationToShareTypes:shareTypes readTypes:readTypes
											   completion:^(BOOL success, NSError *error) {
												   if (!success) {
													   NSLog(@"An error occured getting HealthKit authorization. The error was: %@.", error);
													   return;
												   }
												   
												   NSLog(@"Access granted. Saving data...");
												   [self SaveWeightIntoHealthStoreWithWeight:measurement.nWeight WithDate:measurement.date IsMetric:measurement.unitsKG WithHealthStore:healthStore];
											   }
			 ];
		}
		
	}
	*/
}

/*- (void)SaveWeightIntoHealthStoreWithWeight:(double)weight WithDate:(NSDate*)date IsMetric:(BOOL)bMetric WithHealthStore:(HKHealthStore*)healthStore
{
	HKQuantitySample *WeightSample = [self CreateWeightHealthKitSample:weight isMetric:bMetric withDate:date];
	
	NSArray *objects = [NSArray arrayWithObjects:WeightSample, nil];
	
	[healthStore saveObjects:objects withCompletion:^(BOOL success, NSError *error) {
		if (!success) {
			NSLog(@"An error occured saving the weight samples. The error was: %@.", error);
			return;
		}
		NSLog(@"Data saved to HealthKit.");
	}];
}

- (HKQuantitySample*)CreateWeightHealthKitSample:(double)weight isMetric:(BOOL)bMetric withDate:(NSDate*)date
{
	HKUnit *WeightUnit;
	
	if (bMetric)
	{
		WeightUnit = [HKUnit gramUnit];
		weight *= 1000; // weight in KG actually
	}
	else
	{
		WeightUnit = [HKUnit poundUnit];
	}
	
	HKQuantity *WeightQuantity = [HKQuantity quantityWithUnit:WeightUnit doubleValue:weight];
	HKQuantityType *weightType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
	HKQuantitySample *WeightSample = [HKQuantitySample quantitySampleWithType:weightType quantity:WeightQuantity startDate:date endDate:date];

	return WeightSample;
}

- (HKQuantitySample*)CreateBMIHealthKitSample:(double)BMI withDate:(NSDate*)date
{
	HKUnit *BMIUnit = [HKUnit countUnit];
	HKQuantity *BMIQuantity = [HKQuantity quantityWithUnit:BMIUnit doubleValue:BMI];
	HKQuantityType *weightBMIType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMassIndex];
	HKQuantitySample *BMISample = [HKQuantitySample quantitySampleWithType:weightBMIType quantity:BMIQuantity startDate:date endDate:date];
	
	return BMISample;
}
*/
@end
