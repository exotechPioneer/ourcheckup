//
//  BloodPressureMeasurement.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import <Foundation/Foundation.h>
#import "Measurement.h"

@interface BloodPressureMeasurement : Measurement

// Blood pressure components
@property (nonatomic) NSInteger nSysBP;
@property (nonatomic) NSInteger nDiaBP;
@property (nonatomic) NSInteger nMAP;

@property (nonatomic) NSInteger nPulse;

// Measurement flags
@property Boolean unitskPa;		 // Units in kPa (true) or mmHg (false)
@property Boolean pulsePresent;
@property Boolean statusPresent;
@property Boolean HSDPresent;

// Measurement status
@property Boolean irregularPulseDetected;
@property (nonatomic) NSInteger pulseRateStatus; // 0: In range; 1: Too high; 2: Too low; else:  Unknown"
@property Boolean HSDDetected;

-(id)initWithSys:(NSInteger)sys andDia:(NSInteger)dia andPulse:(NSInteger)pulse;
-(NSString*)toString;

@end
