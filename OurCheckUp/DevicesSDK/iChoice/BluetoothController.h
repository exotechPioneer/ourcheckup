//
//  BluetoothController.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "DeviceRecord.h"

@interface BluetoothController : NSObject<CBCentralManagerDelegate, CBPeripheralDelegate>

- (void)Scan;
- (void)StopScan;

- (void)ConnectDevice:(DeviceRecord*)device;
- (void)DisconnectDevice:(DeviceRecord*)device;

- (void)Write:(NSData*)data toCharacteristic:(CBUUID*)charac toService:(CBUUID*)service toDevice:(DeviceRecord*)record;

- (BOOL)IsBluetoothOn;

@end
