//
//  BluetoothController.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "BluetoothController.h"
#import "GattAttributes.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "BloodPressureMeasurement.h"
#import "WeightMeasurement.h"

@interface BluetoothController () <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (strong, nonatomic) CBCentralManager      *centralManager;
@property (strong, nonatomic) NSMutableDictionary	*processingPeripherals;
@property (strong, nonatomic) NSMutableDictionary	*discoveredPeripherals;
@property (strong, nonatomic) NSMutableDictionary	*discoveredCharacteristicsForProcessingPeripherals;
@property (strong, nonatomic) NSMutableArray		*requestedConnectionsList;

@end

@implementation BluetoothController
@synthesize centralManager, processingPeripherals, discoveredPeripherals, requestedConnectionsList, discoveredCharacteristicsForProcessingPeripherals;

BOOL m_bScanRequested;
BOOL m_bConnectRequested;
int m_nScanRequestedTimes;	//There are several objects that want to Scan & StopScan, let them not screw each other

#pragma mark - Lifecycle

- (id)init
{
	self = [super init];
	
	if (self == nil)
		return nil;
	
	self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_queue_create("CentralManagerQueue", DISPATCH_QUEUE_SERIAL) options:@{CBCentralManagerOptionRestoreIdentifierKey:@"BluetoothControllerCentralManager"}];
	
	processingPeripherals = [[NSMutableDictionary alloc] init];
	discoveredPeripherals = [[NSMutableDictionary alloc] init];
	discoveredCharacteristicsForProcessingPeripherals = [[NSMutableDictionary alloc] init];
	
	m_bScanRequested = NO;
	m_bConnectRequested = NO;
	requestedConnectionsList = [[NSMutableArray alloc] init];
	
	NSLog(@"BluetoothController::Inited");
	
	return self;
}

- (void)Stop
{
	if ([self.centralManager isScanning])
		[self StopScan];
	
	NSLog(@"BluetoothController::Stopped");
}

#pragma mark - Data management
- (void)NotifyFoundDevice:(DeviceRecord*)record
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceFound" object:record userInfo:@{@"Sender":@"BluetoothController"}];
}

- (void)NotifyIncomingMeasurement:(Measurement*)measurement
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnIncomingMeasurement" object:measurement userInfo:@{@"Sender":@"BluetoothController"}];
}

- (void)NotifyDeviceDisconnect:(CBPeripheral*)peripheral
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceDisconnected" object:peripheral userInfo:@{@"Sender":@"BluetoothController"}];
}

- (void)NotifyDeviceConnected:(CBPeripheral*)peripheral
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceConnected" object:peripheral userInfo:@{@"Sender":@"BluetoothController"}];
}

- (void)NotifySystemID:(NSString*)systemID ForDevice:(CBPeripheral*)peripheral
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnDeviceSystemID" object:peripheral userInfo:@{@"Sender":@"BluetoothController", @"SystemID":systemID}];
}

- (void)NotifyIncomingData:(NSData*)data ForDevice:(CBPeripheral*)peripheral
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OnIncomingData" object:peripheral userInfo:@{@"Sender":@"BluetoothController", @"Data":data}];
}

- (NSString*)ConvertToString:(NSData*)data
{
	NSUInteger bytesCount = data.length;
	if (bytesCount) {
		const char *hexChars = "0123456789ABCDEF";
		const unsigned char *dataBuffer = data.bytes;
		char *chars = malloc(sizeof(char) * (bytesCount * 2 + 1));
		char *s = chars;
		for (unsigned i = 0; i < bytesCount; ++i) {
			*s++ = hexChars[((*dataBuffer & 0xF0) >> 4)];
			*s++ = hexChars[(*dataBuffer & 0x0F)];
			dataBuffer++;
		}
		*s = '\0';
		NSString *hexString = [NSString stringWithUTF8String:chars];
		free(chars);
		return hexString;
	}
	return @"";
}

-(void)ConnectDevice:(DeviceRecord*)device
{
	NSLog(@"BluetoothController::ConnectDevice() to device \"%@\"", [device name]);
	
	if ([self.centralManager state] != CBCentralManagerStatePoweredOn)
	{
		NSLog(@"Add to connection list");
		
		m_bConnectRequested = YES;
		if (![requestedConnectionsList containsObject:device])
		{
			[requestedConnectionsList addObject:device];
		}
		
		return;
	}
	
	[self cleanup:device.address];
	
	NSArray *targetUUID = @[device.address];
	NSArray *peripherals = [self.centralManager retrievePeripheralsWithIdentifiers:targetUUID];
	
	if ([peripherals count] < 1)
	{
		NSLog(@"[E]Error searching peripheral!");
	}
	else if ([peripherals count] > 1)
	{
		NSLog(@"[E]More than 1 peripheral with the same address!");
	}
	else
	{
		CBPeripheral* targetPeripheral = [peripherals objectAtIndex:0];
		DeviceConnectionRecord* record = [[DeviceConnectionRecord alloc] initWithName:[targetPeripheral name] andAddress:[targetPeripheral identifier] andPeripheral:targetPeripheral];
		
		[processingPeripherals setObject:record forKey:record.address];
		
		if (targetPeripheral != nil)
		{
			[self.centralManager connectPeripheral:targetPeripheral options:nil];
		}
		else
		{
			NSLog(@"[E]Error searching peripheral!");
		}
	}
}

-(void)DisconnectDevice:(DeviceRecord*)device
{
	NSLog(@"BluetoothController::disconnect device %@", [device name]);
	[self cleanup:device.address];
}

- (void)Write:(NSData*)data toCharacteristic:(CBUUID*)charac toService:(CBUUID*)service toDevice:(DeviceRecord*)record
{
	NSLog(@"BluetoothController::Write to device %@", record.name);
	
	DeviceConnectionRecord* connectionRecord = [processingPeripherals objectForKey:record.address];
	NSMutableDictionary* dict = [discoveredCharacteristicsForProcessingPeripherals objectForKey:record.address];
	CBCharacteristic* cbCharac = [dict objectForKey:charac];
	
	if (cbCharac == nil)
	{
		NSLog(@"[E] null charac to write");
	}
	
	CBCharacteristicWriteType writeType;
	if ([cbCharac properties] & CBCharacteristicPropertyWrite)
	{
		NSLog(@"Write with response");
		writeType = CBCharacteristicWriteWithResponse;
	}
	else if ([cbCharac properties] & CBCharacteristicPropertyWriteWithoutResponse)
	{
		NSLog(@"Write without response");
		writeType = CBCharacteristicWriteWithoutResponse;
	}
	else
	{
		NSLog(@"Attempt to write nonwritable characteristic");
		return;
	}
	
	[connectionRecord.targetPeripheral writeValue:data forCharacteristic:cbCharac type:writeType];
	NSLog(@"Really write");
}

- (BOOL)IsBluetoothOn
{
	return self.centralManager.state == CBCentralManagerStatePoweredOn;
}

#pragma mark - Central Methods : Discovery

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
	NSLog(@"BluetoothController::centralManagerDidUpdateState(%d)", (int)central.state);
	
	if (central.state != CBCentralManagerStatePoweredOn) {
		NSMutableDictionary* temp = [[NSMutableDictionary alloc]initWithDictionary:processingPeripherals];
		
		for (NSUUID* uuid in temp)
		{
			DeviceConnectionRecord* record = [processingPeripherals objectForKey:uuid];
			
			NSLog(@"Disconnect device \"%@\"", [record name]);
			
			[self centralManager:self.centralManager didDisconnectPeripheral:record.targetPeripheral error:nil];
		}
		
		return;
	}
	
	if (m_bScanRequested)
	{
		m_bScanRequested = NO;
		[self Scan];
	}
	
	if (m_bConnectRequested)
	{
		m_bConnectRequested = NO;
		
		for (DeviceRecord* record in requestedConnectionsList)
		{
			[self ConnectDevice:record];
		}
		
		[requestedConnectionsList removeAllObjects];
	}
}

- (void)centralManager:(CBCentralManager * _Nonnull)central
	  willRestoreState:(NSDictionary<NSString *,
						id> * _Nonnull)dict
{
	NSLog(@"CentralManager::willRestoreState()");
}

- (void)Scan
{
	m_nScanRequestedTimes++;
	NSLog(@"BluetoothController::Scan(), m_nScanRequestedTimes = %d", m_nScanRequestedTimes);
	
	if ([self.centralManager state] != CBCentralManagerStatePoweredOn)
	{
		m_bScanRequested = YES;
		return;
	}
	
	[self.discoveredPeripherals removeAllObjects];
	
	if (![self.centralManager isScanning])
	{
		[self.centralManager scanForPeripheralsWithServices:nil
													options:@{ CBCentralManagerScanOptionAllowDuplicatesKey: @NO }];
	}
	
	NSLog(@"BluetoothController::Scanning started");
}

-(void)StopScan
{
	m_nScanRequestedTimes--;
	NSLog(@"BluetoothController::StopScan(), m_nScanRequestedTimes = %d", m_nScanRequestedTimes);
	
	if (m_nScanRequestedTimes == 0 && [self.centralManager isScanning])
	{
		[self.centralManager stopScan];
	}
	
	NSLog(@"BluetoothController::Scanning stopped");
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
	NSLog(@"BluetoothController::Discovered %@ at %@", peripheral.name, RSSI);
	
	if (peripheral == nil)
		return;
	
	NSMutableArray* uuids = [advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey];
	
	DEVICE_TYPE type = DEVICE_TYPE_NONE;
	
	for (CBUUID* uuid in uuids)
	{
		NSLog(@"uuid = %@", uuid);
		
		if ([self IsiChoiceService:uuid])
		{
			type = [self GetiChoiceDeviceType:uuid];
		}
	}
	
	if ([self.discoveredPeripherals objectForKey:[peripheral identifier]] == nil && type != DEVICE_TYPE_NONE)
	{
		[self.discoveredPeripherals setObject:peripheral forKey:[peripheral identifier]];
		
		DeviceRecord *record = [[DeviceRecord alloc] initWithName:[peripheral name] andAddress:[peripheral identifier] andDeviceType:type];
		[self NotifyFoundDevice:record];
	}
}

- (bool)IsiChoiceService:(CBUUID*)uuid
{
	if ([[uuid UUIDString] containsString:@"BA11F08C-5F14-0B0D"])
	{
		NSLog(@"iChoice service found: %@", uuid);
		return true;
	}
	
	return false;
}

// Check device type by iChoice UUID
- (DEVICE_TYPE)GetiChoiceDeviceType:(CBUUID*)uuid
{
	NSData* deviceTypeAndStatus = [[uuid data] subdataWithRange:NSMakeRange(9, 1)];
	Byte nDeviceType = *((Byte*)[deviceTypeAndStatus bytes]);
	
	NSLog(@"ichoice device type %d", nDeviceType);
	
	if (nDeviceType >= 0x80 && nDeviceType <= 0x9F) // OX100
	{
		return DEVICE_TYPE_OXIMETER;
	}
	else if (nDeviceType >= 0xA0 && nDeviceType <= 0xAF)
	{
		return DEVICE_TYPE_BPM;
	}
	else if (nDeviceType >= 0x70 && nDeviceType <= 0x7F)
	{
		return DEVICE_TYPE_WEIGHT_SCALE;
	}
	
	return DEVICE_TYPE_NONE;
}


#pragma mark - Central Methods : Connection

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
	NSLog(@"BluetoothController::Failed to connect to %@. (%@)", peripheral, [error localizedDescription]);
	[self cleanup: peripheral.identifier];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
	NSLog(@"BluetoothController::Peripheral Connected");
	
	if ([self.processingPeripherals objectForKey:peripheral.identifier] == nil)
	{
		NSLog(@"targetPeripheral is empty");
		return;
	}
	
	peripheral.delegate = self;
	
	[peripheral discoverServices:nil];
	[discoveredCharacteristicsForProcessingPeripherals setObject:@{} forKey:peripheral.identifier];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
	if (error) {
		NSLog(@"BluetoothController::Error discovering services: %@", [error localizedDescription]);
		[self cleanup: peripheral.identifier];
		return;
	}
	
	for (CBService *service in peripheral.services) {
		[peripheral discoverCharacteristics:nil forService:service];
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
	NSLog(@"BluetoothController::didDiscoverCharacteristicsForService");
	
	if (error) {
		NSLog(@"BluetoothController::Error discovering characteristics: %@", [error localizedDescription]);
		[self cleanup: peripheral.identifier];
		return;
	}
	
	for (CBCharacteristic *characteristic in service.characteristics) {
		if (([characteristic properties]) & CBCharacteristicPropertyIndicate
			|| ([characteristic properties]) & CBCharacteristicPropertyNotify)
		{
			[peripheral setNotifyValue:YES forCharacteristic:characteristic];
		}
		
		NSDictionary* dict = [discoveredCharacteristicsForProcessingPeripherals objectForKey:peripheral.identifier];
		NSMutableDictionary* dict2 = [[NSMutableDictionary alloc] initWithDictionary:dict];
		[dict2 setObject:characteristic forKey:characteristic.UUID];
		[discoveredCharacteristicsForProcessingPeripherals setObject:dict2 forKey:peripheral.identifier];
	}
	
	bool bConnected = true;
	
	for (CBService* service in peripheral.services)
	{
		if ([service.characteristics count] == 0)
		{
			bConnected = false;
			break;
		}
	}
	
	if (bConnected)
	{
		[self NotifyDeviceConnected:peripheral];
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
	if (error) {
		NSLog(@"BluetoothController::Error discovering characteristics: %@", [error localizedDescription]);
		return;
	}
	
	NSString *message = [self ConvertToString:characteristic.value];
	NSLog(@"BluetoothController::didUpdateValueForCharacteristic message = %@", message);
	[self NotifyIncomingData:characteristic.value ForDevice:peripheral];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
	if (error) {
		NSLog(@"Error changing notification state: %@", error.localizedDescription);
	}

	if (characteristic.isNotifying)
	{
		NSLog(@"Notification began on %@", characteristic);
	}
	else
	{
		NSLog(@"Notification stopped on %@.  Disconnecting", characteristic);
		[self.centralManager cancelPeripheralConnection:peripheral];
	}
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
	NSLog(@"Peripheral Disconnected");

	[self.discoveredCharacteristicsForProcessingPeripherals removeObjectForKey:peripheral.identifier];
	[self.processingPeripherals removeObjectForKey:peripheral.identifier];
	[self NotifyDeviceDisconnect:peripheral];
}

- (void)cleanup:(NSUUID*)address
{
	DeviceConnectionRecord* savedRecord = [self.processingPeripherals objectForKey:address];
	if (savedRecord == nil)
		return;
	
	if ([savedRecord.targetPeripheral state] == CBPeripheralStateDisconnected) {
		return;
	}

	if (savedRecord.targetPeripheral.services != nil)
	{
		for (CBService *service in savedRecord.targetPeripheral.services)
		{
			if (service.characteristics != nil)
			{
				for (CBCharacteristic *characteristic in service.characteristics)
				{
					if (characteristic.isNotifying)
					{
						[savedRecord.targetPeripheral setNotifyValue:NO forCharacteristic:characteristic];
					}
				}
			}
		}
	}
	
	[self.centralManager cancelPeripheralConnection:savedRecord.targetPeripheral];
	
	[self.processingPeripherals removeObjectForKey:address];
}

@end
