//
//  Device_iChoiceOX100.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "Device.h"

@interface Device_iChoiceOX100 : Device

-(void)Pair;

@end
