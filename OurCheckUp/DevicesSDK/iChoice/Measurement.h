//
//  Measurement.h
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import <Foundation/Foundation.h>

typedef enum { MEASUREMENT_NONE, MEASUREMENT_WEIGHT, MEASUREMENT_BP, MEASUREMENT_OXYGEN } MEASUREMENT_TYPE;

@interface Measurement : NSObject

@property (nonatomic) NSInteger nUserID;
@property (strong, nonatomic) NSDate	*date;

//Flags
@property Boolean timePresent;
@property Boolean userIDPresent;   // User ID info is present

- (Boolean) getFlag:(int)flag From:(Byte)flags;  // flag 0 means 0 bit, flag 1 means 1 bit, ...
- (MEASUREMENT_TYPE)getType;

@end
