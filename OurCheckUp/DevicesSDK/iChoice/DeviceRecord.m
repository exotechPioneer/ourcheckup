//
//  DeviceRecord.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import <Foundation/Foundation.h>
#import "DeviceRecord.h"

#define CODER_KEY_NAME @"name"
#define CODER_KEY_ADDRESS @"address"
#define CODER_KEY_LAST_READING_RECEIVED_DATE @"lastReadingReceivedDate"
#define CODER_KEY_LAST_CONNECTED_DATE @"lastConnectedDate"
#define CODER_KEY_DEVICE_ID @"DeviceID"
#define CODER_KEY_DEVICE_TYPE @"DeviceType"

@implementation DeviceRecord

@synthesize name, address, m_deviceType, lastConnectedDate, lastReadingReceivedDate, deviceID;

-(id)initWithName:(NSString*)_name andAddress:(NSUUID*)_address
{

	
	return self;
}

-(id)initWithName:(NSString*)_name andAddress:(NSUUID*)_address andDeviceType:(DEVICE_TYPE)type
{
	self = [super init];
	
	if (self == nil)
		return nil;
	
	self.name = _name;
	self.address = _address;
	self.lastReadingReceivedDate = [NSDate dateWithTimeIntervalSince1970:0];
	self.lastConnectedDate = @"Never";
	self.m_deviceType = type;
	
	return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super init]) {
		self.name = [decoder decodeObjectForKey:CODER_KEY_NAME];
		self.address = [decoder decodeObjectForKey:CODER_KEY_ADDRESS];
		self.lastReadingReceivedDate = [decoder decodeObjectForKey:CODER_KEY_LAST_READING_RECEIVED_DATE];
		self.lastConnectedDate = [decoder decodeObjectForKey:CODER_KEY_LAST_CONNECTED_DATE];
		self.deviceID = [decoder decodeObjectForKey:CODER_KEY_DEVICE_ID];
		self.m_deviceType = (DEVICE_TYPE)[decoder decodeIntegerForKey:CODER_KEY_DEVICE_TYPE];
	}
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:self.name forKey:CODER_KEY_NAME];
	[encoder encodeObject:self.address forKey:CODER_KEY_ADDRESS];
	[encoder encodeObject:self.lastReadingReceivedDate forKey:CODER_KEY_LAST_READING_RECEIVED_DATE];
	[encoder encodeObject:self.lastConnectedDate forKey:CODER_KEY_LAST_CONNECTED_DATE];
	[encoder encodeObject:self.deviceID forKey:CODER_KEY_DEVICE_ID];
	[encoder encodeInteger:self.m_deviceType forKey:CODER_KEY_DEVICE_TYPE];
	
	NSLog(@"DeviceRecord::encodeWithCoder(): name = %@, lastReadingDate = %@, lastConnectedDate = %@, deviceID = %@", self.name, self.lastReadingReceivedDate, self.lastConnectedDate, self.deviceID);
}

- (NSString*)GetPictureName
{
	return [DeviceRecord GetPictureNameForDeviceType:m_deviceType];
}

- (NSString*)GetRoundPictureName
{
	return [DeviceRecord GetRoundPictureNameForDeviceType:m_deviceType];
}

+ (NSString*)GetPictureNameForDeviceType:(DEVICE_TYPE)type
{
	switch (type)
	{
		case DEVICE_TYPE_BPM:
			return @"dev_small_iChoice_BP1";
			
		case DEVICE_TYPE_WEIGHT_SCALE:
			return @"dev_small_iChoice_S1";
			
		case DEVICE_TYPE_OXIMETER:
			return @"dev_small_iChoice_OX100";
			
		default:
			NSLog(@"[E] Unknown device type %d", type);
			return @"";
	}
}

- (NSString*)GetDeviceDisplayName
{
	switch (m_deviceType)
	{
		case DEVICE_TYPE_BPM:
			return @"iChoice BP1";
			
		case DEVICE_TYPE_WEIGHT_SCALE:
			return @"iChoice S1";
			
		case DEVICE_TYPE_OXIMETER:
			return @"iChoice OX100";
			
		default:
			NSLog(@"[E] Unknown device type %d", m_deviceType);
			return @"Unknown Device";
	}
}

+ (NSString*)GetRoundPictureNameForDeviceType:(DEVICE_TYPE)type
{
	switch (type)
	{
		case DEVICE_TYPE_BPM:
			return @"dev_round_iChoice_BP1";
			
		case DEVICE_TYPE_WEIGHT_SCALE:
			return @"dev_round_iChoice_S1";

		case DEVICE_TYPE_OXIMETER:
			return @"dev_round_iChoice_OX100";
			
		default:
			NSLog(@"[E] Unknown device type %d", type);
			return @"";
	}
}

@end


@implementation DeviceConnectionRecord

@synthesize targetPeripheral;

-(id)initWithName:(NSString*)name andAddress:(NSUUID*)address andPeripheral:(CBPeripheral*)peripheral
{
	self = [super initWithName:name andAddress:address andDeviceType:DEVICE_TYPE_NONE];
	
	if (self == nil)
		return nil;
	
	self.targetPeripheral = peripheral;
	
	return self;
}

@end
