//
//  BloodPressureMeasurement.m
//  iChoiceDevices
//
// Copyright (C) 2016 MedM Global, (C) 2016 ChoiceMMed
//

#import "BloodPressureMeasurement.h"

@implementation BloodPressureMeasurement
@synthesize nSysBP, nDiaBP, nMAP, nUserID, nPulse, date, unitskPa, userIDPresent, pulsePresent, statusPresent, timePresent, HSDPresent;
@synthesize irregularPulseDetected, pulseRateStatus, HSDDetected;

- (MEASUREMENT_TYPE)getType
{
	return MEASUREMENT_BP;
}

-(NSString*)toString
{
	NSMutableString* output = [[NSMutableString alloc] init];
	
	if (self.userIDPresent)
		[output appendFormat:@"User %ld: ", (long)self.nUserID];
		 
	[output appendFormat:@"%ld/%ld ", (long)self.nSysBP, (long)self.nDiaBP];
	
	if (self.unitskPa)
	{
		[output appendString:@"kPa"];
	}
	else
	{
		[output appendString:@"mmHg"];
	}
	
	if (self.pulsePresent)
	{
		[output appendFormat:@" ( %ld BPM", (long)self.nPulse];
		
		if (self.statusPresent)
		{
			if (self.irregularPulseDetected)
			{
				[output appendString:@", IRR"];
			}
			
			if (self.pulseRateStatus == 0)
			{
				[output appendString:@", In range"];
			}
			else if (self.pulseRateStatus == 1)
			{
				[output appendString:@", Too high"];
			}
			else if (self.pulseRateStatus == 2)
			{
				[output appendString:@", Too low"];
			}
			else
			{
				[output appendString:@", Unknown"];
			}
			
			if (self.HSDDetected)
			{
				[output appendString:@", HSD"];
			}
		}
		
		[output appendString:@")"];
	}
	
	if (self.nMAP > 0)
	{
		[output appendFormat:@", mean %ld", (long)self.nMAP];
	}
	
	if (self.timePresent)
	{
		NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
		dateFormatter.dateFormat = @"dd-MMM-yyyy, h:mm a";
		
		[output appendFormat:@" at %@", [dateFormatter stringFromDate:self.date]];
	}
	
	return output;
}

-(id)initWithSys:(NSInteger)sys andDia:(NSInteger)dia andPulse:(NSInteger)pulse
{
	self = [super init];
	
	if (self == nil)
		return nil;
	
	self.nSysBP = sys;
	self.nDiaBP = dia;
	self.nMAP = 0;
	self.nUserID = -1;
	self.nPulse = pulse;
	self.date = [[NSDate alloc] init];
	self.unitskPa = false;
	self.userIDPresent = false;
	self.pulsePresent = false;
	self.statusPresent = false;
	self.timePresent = false;
	self.HSDPresent = false;
	self.irregularPulseDetected = false;
	self.pulseRateStatus = false;
	self.HSDDetected = false;

	return self;
}

@end
