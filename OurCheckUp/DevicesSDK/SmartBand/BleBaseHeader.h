//
//  BleBaseHeader.h
//  BleSDK
//
//  Created by wakeup on 2018/10/25.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#ifndef BleBaseHeader_h
#define BleBaseHeader_h

#import "BleManager.h"              //蓝牙类
#import "BleModel.h"                //蓝牙模型
#import "LongTimeSitModel.h"        //久坐提醒模型
#import "IgnoreModel.h"             //勿扰模式模型
#import "SleepTimeRangeModel.h"     //睡眠时间范围模型
#import "UserDateModel.h"           //用户时间模型
#import "UserInfoNormalModel.h"     //用户信息模型(wearfit 1.0)
#import "UserInfoAlertBpmModel.h"   //用户信息模型(wearfit 2.0)
#import "TimeAlertModel.h"          //闹钟模型
#import "DateModel.h"               //整点数据时间模型(普通设备)
#import "BpmDateModel.h"            //整点数据时间模型(连续心率设备)
#import "SleepDateModel.h"          //睡眠数据时间模型

//接收数据模型
#import "RealTimeDataModel.h"       //实时数据模型
#import "OffLineHeartModel.h"       //设备单机测量心率模型
#import "OffLineOxygenModel.h"      //设备单机测量血氧模型
#import "OffLineBloodModel.h"       //设备单机测量血压模型
#import "AllDataModel.h"            //返回整点数据时间模型
#import "SleepDataModel.h"          //睡眠数据模型
#import "VersionModel.h"            //版本号模型

#endif
