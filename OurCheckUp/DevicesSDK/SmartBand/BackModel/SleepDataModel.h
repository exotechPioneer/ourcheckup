//
//  SleepDataModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/7.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//睡眠数据模型
@interface SleepDataModel : NSObject

@property (nonatomic, assign) int year;   //年
@property (nonatomic, assign) int month;  //月
@property (nonatomic, assign) int day;    //日
@property (nonatomic, assign) int hour;   //时
@property (nonatomic, assign) int minute; //分
@property (nonatomic, assign) int state;  //睡眠状态(1:进入睡眠 2:进入深睡)
@property (nonatomic, assign) int time;   //睡眠时间(单位:分钟)

/**
 特殊参数注释:(control)
 注释：1、state为1时表示进入了睡眠，所谓的持续时间是指入睡到醒来的时间段，此段时间包括了浅度和深度（如果有）睡眠；
      2、state为2表示进入了深度睡眠，持续时间是指深度睡眠时间。
      3、总的睡眠时间为所有的state为1的持续时间的总和；
      4、总的深度睡眠时间为所有的state为2的持续时间的总和；
      5、总的浅度睡眠时间=总的睡眠时间-总的深度睡眠时间。
      6、醒来次数可通过state为1的时间差来计算
 */

@end
