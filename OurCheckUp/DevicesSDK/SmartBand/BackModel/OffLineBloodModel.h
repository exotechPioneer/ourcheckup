//
//  OffLineBloodModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/6.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//设备单机测量血压模型
@interface OffLineBloodModel : NSObject

@property (nonatomic, assign) int year;     //年
@property (nonatomic, assign) int month;    //月
@property (nonatomic, assign) int day;      //日
@property (nonatomic, assign) int hour;     //时
@property (nonatomic, assign) int minute;   //分
@property (nonatomic, assign) int minBlood; //收缩压
@property (nonatomic, assign) int manBlood; //舒张压

@end
