//
//  AllDataModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/6.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//返回整点数据时间模型
@interface AllDataModel : NSObject

@property (nonatomic, assign) int year;     //年
@property (nonatomic, assign) int month;    //月
@property (nonatomic, assign) int day;      //日
@property (nonatomic, assign) int hour;     //时
@property (nonatomic, assign) int min;
@property (nonatomic, assign) int step;     //步数
@property (nonatomic, assign) int cal;      //卡路里
@property (nonatomic, assign) int heart;    //心率(连续心率设备没有此参数)
@property (nonatomic, assign) int oxygen;   //血氧(连续心率设备没有此参数)
@property (nonatomic, assign) int maxBlood; //收缩压(连续心率设备没有此参数)
@property (nonatomic, assign) int minBlood; //舒张压(连续心率设备没有此参数)
@property (nonatomic, assign) int shallowSleep;
@property (nonatomic, assign) int deepSleep;
@property (nonatomic, assign) int sleephours;
@property (nonatomic, assign) int sleepMins;
@property (nonatomic, assign) int wakeupTimes;
@end
