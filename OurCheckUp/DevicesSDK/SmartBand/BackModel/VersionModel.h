//
//  VersionModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/8.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//版本号模型
@interface VersionModel : NSObject

/**
 可通过设备type来区分设备适用于我司wearfit 1.0的设备还是wearfit 2.0的设备
 wearfit 1.0(type < 30 || type > 99 && type < 200) 其余type均为wearfit 2.0
 */
@property (nonatomic, assign) int version;               //设备固件版本号
@property (nonatomic, assign) int type;                  //设备type
@property (nonatomic, copy) NSString *mac;               //设备MAC

//以下参数 wearfit 1.0 YES:是 NO:否   wearfit 2.0 YES:否 NO:是
@property (nonatomic, assign) BOOL isHaveStepLength;     //是否可设置步长
@property (nonatomic, assign) BOOL isHaveSleepRange;     //是否可设置睡眠区间
@property (nonatomic, assign) BOOL isHaveHour;           //是否可设置12和24小时制切换

//以下参数 YES:是 NO:否
@property (nonatomic, assign) BOOL isHaveWeChat;         //是否支持苹果健康和微信运动
@property (nonatomic, assign) BOOL isHaveAlertBPM;       //是否可设置心率报警
@property (nonatomic, assign) BOOL isHanTianXia;         //是否汉天下芯片

//以下参数用来判断设备是什么类型的设备,都不支持,那就是默认的普通设备 YES:是 NO:否
@property (nonatomic, assign) BOOL isHaveECG;            //是否心电设备
@property (nonatomic, assign) BOOL isHaveBPM;            //是否连续心率设备
@property (nonatomic, assign) BOOL isHaveBpmBlood;       //是否连续心率、血压设备
@property (nonatomic, assign) BOOL isHaveBpmBloodOxygen; //是否连续心率、血压、血氧设备
@property (nonatomic, assign) BOOL isHaveBpmOxygen;      //是否连续心率、血氧设备

//以下参数 YES:支持 NO:不支持
@property (nonatomic, assign) BOOL isHaveMessager;       //是否支持Messager、Facebook Page manager
@property (nonatomic, assign) BOOL isHaveInstagram;      //是否支持Instagram
@property (nonatomic, assign) BOOL isHaveSitInterval;    //是否可设置久坐间隔
@property (nonatomic, assign) BOOL isHaveBpmPowerSaving; //是否可设置连续心率省电模式

@end
