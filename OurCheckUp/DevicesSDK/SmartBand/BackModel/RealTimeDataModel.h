//
//  RealTimeDataModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/6.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//实时数据模型
@interface RealTimeDataModel : NSObject

@property (nonatomic, assign) int step;             //Step count
@property (nonatomic, assign) int cal;              //Calorie
@property (nonatomic, assign) int lightSleepHour;   //Shallow sleep hours
@property (nonatomic, assign) int lightSleepMinute; //Shallow sleep minutes
@property (nonatomic, assign) int deepSleepHour;    //Deep sleep hours
@property (nonatomic, assign) int deepSleepMinute;  //Deep sleep minutes
@property (nonatomic, assign) int wakeupNumber;     //Wake up

@end
