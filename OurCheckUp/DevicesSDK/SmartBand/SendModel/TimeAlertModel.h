//
//  TimeAlertModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/1.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//闹钟模型
@interface TimeAlertModel : NSObject

@property (nonatomic, assign) int  identifier; //闹钟id。最多可设8个闹钟(id范围:0~7)
@property (nonatomic, assign) BOOL isOn;       //YES:打开闹钟 NO:关闭闹钟
@property (nonatomic, assign) int  hour;       //小时
@property (nonatomic, assign) int  minute;     //分钟
@property (nonatomic, assign) int  control;    //控制当前闹钟是否重复

/**
 特殊参数注释:(control) bit0~bit6分别代表从周一到周日的设置，Bit位为1时表示重复，为0时表示关闭。Bit7为1时表示只提
            醒一次(即只当天有效)。
 
            如: 每天提醒，     则bit7-bit0为 0 1 1 1 1 1 1 1
                周一到周五提醒，则bit7-bit0为 0 0 0 1 1 1 1 1
                只提醒一次，   则bit7-bit0为 1 0 0 0 0 0 0 0
 
 加强理解:蓝牙发送指令是以byte为单位发送的，一个byte等于8个bit，bit是左高右低的，例如 0 1 1 1 1 1 1 1 分别表示
         bit7~bit0
 
         control是整数，蓝牙类内部作为一个byte发送给设备，所以其实control也是有范围的，范围是0~255，转化为二进制也就是低八位的数值，此时和bit一致
 
         例如: 每天提醒，直接给control赋值127即可，    低八位二进制和bit一致 0 1 1 1 1 1 1 1
              周一到周五提醒，直接给control赋值31即可，低八位二进制和bit一致 0 0 0 1 1 1 1 1
              只提醒一次，直接给control赋值128即可，   低八位二进制和bit一致 1 0 0 0 0 0 0 0
 
         这些参数可以通过powf函数控制，例如: 128 (int)powf(2, 7)
 
         兄弟，再不懂百度，我尽力了
 */

@end
