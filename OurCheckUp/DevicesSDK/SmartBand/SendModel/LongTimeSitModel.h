//
//  LongTimeSitModel.h
//  BleSDK
//
//  Created by wakeup on 2018/10/29.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//久坐提醒模型
@interface LongTimeSitModel : NSObject

@property (nonatomic, assign) BOOL on;         //YES:打开 NO:关闭
@property (nonatomic, assign) int startHour;   //开始小时
@property (nonatomic, assign) int startMinute; //开始分钟
@property (nonatomic, assign) int endHour;     //结束小时
@property (nonatomic, assign) int endMinute;   //结束分钟

@end
