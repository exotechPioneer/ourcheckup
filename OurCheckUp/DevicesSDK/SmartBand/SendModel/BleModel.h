//
//  BleModel.h
//  BleSDK
//
//  Created by wakeup on 2018/10/25.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

//蓝牙模型
@interface BleModel : NSObject

@property (nonatomic, strong) CBPeripheral *peripheral;        //扫描到的周边设备
@property (nonatomic, strong) NSDictionary *advertisementData; //广播包
@property (nonatomic, strong) NSNumber *rssi;                  //设备信号
@property (nonatomic, copy) NSString *name;                    //设备名
@property (nonatomic, copy) NSString *mac;                     //设备MAC

@end
