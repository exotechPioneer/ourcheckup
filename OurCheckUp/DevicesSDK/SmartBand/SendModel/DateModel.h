//
//  DateModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/5.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//整点数据时间模型(普通设备)
@interface DateModel : NSObject

@property (nonatomic, assign) int year;   //年
@property (nonatomic, assign) int month;  //月
@property (nonatomic, assign) int day;    //日
@property (nonatomic, assign) int hour;   //时
@property (nonatomic, assign) int minute; //分

@end
