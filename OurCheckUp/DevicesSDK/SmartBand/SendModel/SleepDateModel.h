//
//  SleepDateModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/5.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//睡眠数据时间模型
@interface SleepDateModel : NSObject

@property (nonatomic, assign) int year;   //年
@property (nonatomic, assign) int month;  //月
@property (nonatomic, assign) int day;    //日

@end
