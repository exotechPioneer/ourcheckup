//
//  BpmDateModel.h
//  BleSDK
//
//  Created by wakeup on 2018/11/10.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//整点数据时间模型(连续心率设备)
@interface BpmDateModel : NSObject

//整点数据的时间
@property (nonatomic, assign) int year;   //年
@property (nonatomic, assign) int month;  //月
@property (nonatomic, assign) int day;    //日
@property (nonatomic, assign) int hour;   //时
@property (nonatomic, assign) int minute; //分

//连续心率的时间
@property (nonatomic, assign) int yearBpm;   //年
@property (nonatomic, assign) int monthBpm;  //月
@property (nonatomic, assign) int dayBpm;    //日
@property (nonatomic, assign) int hourBpm;   //时
@property (nonatomic, assign) int minuteBpm; //分

@end
