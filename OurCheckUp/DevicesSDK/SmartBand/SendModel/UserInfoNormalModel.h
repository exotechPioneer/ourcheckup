//
//  UserInfoNormalModel.h
//  BleSDK
//
//  Created by wakeup on 2018/10/31.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//用户信息模型(wearfit 1.0)
@interface UserInfoNormalModel : NSObject

@property (nonatomic, assign) int stepLength;    //步长(单位:cm 范围:20~150)
@property (nonatomic, assign) int age;           //年龄(范围:2~130)
@property (nonatomic, assign) int height;        //身高(单位:cm 范围:30~230)
@property (nonatomic, assign) float weight;      //体重(单位:kg 范围:10~220)
@property (nonatomic, assign) BOOL distanceUnit; //距离单位 YES:公里 NO:英里
@property (nonatomic, assign) int goal;          //目标步数(每天定的走路步数目标 范围:1000~40000 以1000递增)

//特殊参数注释:(goal)贵司设备是否支持目标步数,请咨询我司固件开发人员,如不支持,忽略此参数

@end
