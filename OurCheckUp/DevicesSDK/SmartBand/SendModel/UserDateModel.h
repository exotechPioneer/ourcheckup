//
//  UserDateModel.h
//  BleSDK
//
//  Created by wakeup on 2018/10/30.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>

//用户时间模型
@interface UserDateModel : NSObject

@property (nonatomic, assign) int year;   //年
@property (nonatomic, assign) int month;  //月
@property (nonatomic, assign) int day;    //日
@property (nonatomic, assign) int hour;   //时
@property (nonatomic, assign) int minute; //分
@property (nonatomic, assign) int second; //秒

@end
