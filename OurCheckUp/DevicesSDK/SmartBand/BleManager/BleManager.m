//
//  BleManager.m
//  BleSDK
//
//  Created by wakeup on 2018/10/25.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import "BleManager.h"

//正常使用时的UUID
#define oServiceUUID     @"6e400001-b5a3-f393-e0a9-e50e24dcca9e"
#define oWriteUUID       @"6e400002-b5a3-f393-e0a9-e50e24dcca9e"
#define oNotifyUUID      @"6e400003-b5a3-f393-e0a9-e50e24dcca9e"

//进入ISP模式后使用的UUID
#define hServiceUUID     @"00001234-0000-1000-8000-00805f9b34fb"
#define hWriteUUID       @"ff02"
#define hNotifyUUID      @"ff04"
#define resetUUID        @"ff01"

//UUID变量
static NSString *ServiceUUID = @"";
static NSString *WriteUUID = @"";
static NSString *NotifyUUID = @"";

//功能类型
typedef NS_ENUM(NSUInteger, CommandType) {
    //配对
    CommandTypeBondSystem = 0x21,
    //同步时间
    CommandTypeUserTime = 0x93,
    //用户信息
    CommandTypeUserInfo = 0x74,
    //设备闹钟
    CommandTypeTimeAlert = 0x73,
    //查找设备
    CommandTypeFind = 0x71,
    //久坐提醒开关
    CommandTypeSit = 0x75,
    //勿扰模式开关
    CommandTypeIgnore = 0x76,
    //睡眠范围
    CommandTypeSleepTimeSet = 0x7f,
    //抬手亮屏开关
    CommandTypeLight = 0x77,
    //整点测量开关
    CommandTypeMeasure = 0x78,
    //拍照
    CommandTypeTakePic = 0x79,
    //防丢提醒
    CommandTypeAntiLost = 0x7a,
    //中英文切换
    CommandTypeSwitchLang = 0x7b,
    //时间制度切换
    CommandTypeSwitchTimeFormatter = 0x7c,
    //获取电量
    CommandTypeBatteryLevel = 0x91,
    //获取版本号
    CommandTypeVersion = 0x92,
    //删除手环数据
    CommandTypeDeleteData = 0x23,
    //恢复出厂设置(不删除数据)
    CommandTypeInit = 0xff,
    //一键测量
    CommandTypeMeasureAll = 0x32,
    //获取实时心率
    CommandTypeDurationHeart = 0x84,
    //连续心率省电模式
    CommandTypePowerSaving = 0x96,
};

typedef NS_ENUM(NSUInteger, MeasureType) {
    //单次测量心率
    MeasureTypeHearRate = 0x09,
    //实时测量心率
    MeasureTypeHearRateReal = 0x0a,
    //单次测量血压
    MeasureTypeBlood = 0x21,
    //实时测量血压
    MeasureTypeBloodReal = 0x22,
    //单次测量血氧
    MeasureTypeOxygen = 0x11,
    //实时测量血氧
    MeasureTypeOxygenReal = 0x12,
};

typedef NS_ENUM(NSUInteger, NotifyType) {
    NotifyTypeCall = 1,
    NotifyTypeMessage = 3,
    NotifyTypeQQ = 7,
    NotifyTypeSkype = 8,
    NotifyTypeWechat = 9,
    NotifyTypeWhatsApp = 10,
    NotifyTypeLine = 14,
    NotifyTypeTwitter = 15,
    NotifyTypeFacebook = 16,
    NotifyTypeFacebookMessager = 17,
    NotifyTypeInstagram = 18,
    NotifyTypeWeibo = 19,
    NotifyTypeKakaoTalk = 20,
    NotifyTypeSnapchat = 21,
};

@interface BleManager ()<CBCentralManagerDelegate, CBPeripheralDelegate>

//升级时使用
@property (nonatomic,strong)CBCharacteristic *fileService;
@property (nonatomic,strong)CBCharacteristic *resetService;

//搜索到的所有设备(包括重复的,用来作为过滤条件)
@property (nonatomic, strong) NSMutableArray *peripheralArray;

//搜索到的设备(已经过滤掉重复的)
@property (nonatomic, strong) NSMutableArray *bleArray;

//手机是否打开蓝牙(暂时不使用)
@property (nonatomic, assign) BOOL on;

//是否连接上设备(暂时不使用)
@property (nonatomic, assign) BOOL connectStatus;

@end

@implementation BleManager

#pragma mark - 初始化
+ (instancetype)sharedBleManager {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        self.peripheralArray = [[NSMutableArray alloc] init];
        self.bleArray = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - 开始搜索设备
- (void)ble_startScanBleDevicesWithDuration:(NSInteger)duration {
    [self.peripheralArray removeAllObjects];
    [self.bleArray removeAllObjects];
    [self.centralManager scanForPeripheralsWithServices:nil options:nil];
    [self performSelector:@selector(after) withObject:nil afterDelay:duration];
}

- (void)after {
    [self ble_stopScanBleDevices];
}

#pragma mark - 停止搜索设备
- (void)ble_stopScanBleDevices {
    [self.centralManager stopScan];
}

#pragma mark - 连接设备
- (void)ble_connectBleDevice:(CBPeripheral *)peripheral {
    if ([peripheral.name isEqualToString:@"Huntersun-BLE"]) {
        ServiceUUID = hServiceUUID;
        WriteUUID = hWriteUUID;
        NotifyUUID = hNotifyUUID;
    }
    else {
        ServiceUUID = oServiceUUID;
        WriteUUID = oWriteUUID;
        NotifyUUID = oNotifyUUID;
    }
    
    [self.centralManager stopScan];
    self.activePeripheral = peripheral;
    [self.centralManager connectPeripheral:peripheral options:nil];
}

#pragma mark - 断开设备
- (void)ble_disconnectBleDevice {
    [self.centralManager cancelPeripheralConnection:self.activePeripheral];
}

#pragma mark - 获取实时蓝牙信号
- (void)ble_readRSSI {
    [self.activePeripheral readRSSI];
}

#pragma mark - CBCentralManagerDelegate
#pragma  mark - 检测是否打开蓝牙
- (void) centralManagerDidUpdateState:(CBCentralManager *)centralManager {
    if (centralManager.state == CBCentralManagerStatePoweredOn) {
        self.on = YES;
        if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_state_On)]) {
            [self.delegate bleBack_state_On];
        }
    }
    else {
        self.on = NO;
        self.connectStatus = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_state_Off)]) {
            [self.delegate bleBack_state_Off];
        }
    }
}

/*
 peripheral：扫描到的周边设备
 advertisementData：响应数据  跟广播包所放的内容有关系
 RSSI即Received Signal Strength Indication：接收的信号强度指示
 */
#pragma mark - 搜索到的设备
- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    NSLog(@"广播蓝牙名:%@  信号:%@  advertisementData:%@", peripheral.name, RSSI,  advertisementData);
    
    if ([RSSI intValue] == 127) {
        return;
    }
    
    NSString *name = advertisementData[CBAdvertisementDataLocalNameKey];
    if ((peripheral.name.length == 0) || (name.length == 0)) {
        return;
    }
    
    if (![self.peripheralArray containsObject:peripheral]) {
        [self.peripheralArray addObject:peripheral];
        
        NSData *tempMac = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
        uint8_t dataVal[100] = {0x0};
        [tempMac getBytes:&dataVal length:tempMac.length];
        
        BleModel *model = [[BleModel alloc] init];
        if (tempMac.length == 8) {
            NSString *mac = [[NSString stringWithFormat:@"%02x:%02x:%02x:%02x:%02x:%02x",dataVal[2],dataVal[3],dataVal[4],dataVal[5],dataVal[6],dataVal[7]] lowercaseString];
            model.mac = [mac uppercaseString];
        }
        
        model.peripheral = peripheral;
        model.advertisementData = advertisementData;
        model.rssi = RSSI;
        model.name = advertisementData[CBAdvertisementDataLocalNameKey];
        [self.bleArray addObject:model];
        [self.bleArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            BleModel *model1 = (BleModel *)obj1;
            BleModel *model2 = (BleModel *)obj2;
            return model1.rssi < model2.rssi;
        }];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_scanDevicesWithBleArray:)]) {
            [self.delegate bleBack_scanDevicesWithBleArray:self.bleArray];
        }
        
    }
    
}

#pragma  mark - 成功连接设备
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    
    self.connectStatus = YES;
    peripheral.delegate = self;
    [peripheral discoverServices:@[[CBUUID UUIDWithString:ServiceUUID]]];
    self.UUIDString = peripheral.identifier.UUIDString;
    if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_connectDeviceSuccessWithPeripheral:)]) {
        [self.delegate bleBack_connectDeviceSuccessWithPeripheral:peripheral];
    }
    
}

#pragma mark - 实时蓝牙信号
- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error {
    if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_readRSSI:error:)]) {
        [self.delegate bleBack_readRSSI:RSSI error:error];
    }
}

#pragma mark - 设备已断开
- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    
    self.connectStatus = NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_disconnectDeviceWithPeripheral:error:)]) {
        [self.delegate bleBack_disconnectDeviceWithPeripheral:peripheral error:error];
    }
    
}

#pragma mark - CBPeripheralDelegate
#pragma  mark - 接收到已连接设备的服务
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    
    if (error) {
        NSLog(@"%s,%@",__func__, error);
    }
    
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:nil forService:service];
    }
    
}

#pragma  mark - 获取服务特征
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    
    if (error) {
        NSLog(@"%s,%@",__func__, error);
    }
    
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        
        if ([characteristic.UUID.UUIDString.lowercaseString isEqualToString:NotifyUUID]) {
            self.fileService = characteristic;
        }
        else if ([characteristic.UUID.UUIDString.lowercaseString isEqualToString:resetUUID]) {
            self.resetService = characteristic;
        }
    }
    
}

#pragma mark - 写指令
- (void)sendCommand:(NSData *)data toPeripheral:(CBPeripheral *)peripheral {
    [self writeCharacteristic:peripheral sUUID:ServiceUUID cUUID:WriteUUID data:data response:NO];
}

- (void)writeCharacteristic:(CBPeripheral *)peripheral sUUID:(NSString *)sUUID cUUID:(NSString *)cUUID data:(NSData *)data response:(BOOL)response {
    for ( CBService *service in peripheral.services) {
        if ([service.UUID isEqual:[CBUUID UUIDWithString:sUUID]]) {
            for ( CBCharacteristic *characteristic in service.characteristics ) {
                if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:cUUID]]) {
                    if (response) {
                        [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    }
                    else {
                        [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
                    }
                }
            }
        }
    }
}

#pragma mark - 读取返回来的数据
-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    NSLog(@"蓝牙返回的数据：%@",characteristic.value);
    if (error) {
        NSLog(@"%s,%@",__func__, error);
        return;
    }
    
    int length = (int)characteristic.value.length;
    Byte byte[length];
    [characteristic.value getBytes:byte length:length];
    
    //汉天下芯片升级
    if ([peripheral.name isEqualToString:@"Huntersun-BLE"]) {
        NSData *data = characteristic.value;
        int type = 0;
        if (characteristic == self.fileService) {
            type = 1;
        }
        else if (characteristic == self.resetService) {
            type = 2;
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getOtaData:type:)]) {
            [self.delegate bleBack_getOtaData:[NSMutableData dataWithData: data] type:type];
        }
        return;
    }
    
    //正常数据接收
    if (byte[0] == 0xab) {
        if (byte[4] == 0x51) {
            //设备实时数据
            if (byte[5] == 0x08) {
                RealTimeDataModel *realTimeData = [[RealTimeDataModel alloc] init];
                realTimeData.step = (byte[6] << 16) + (byte[7] << 8) + byte[8];
                realTimeData.cal = (byte[9] << 16) + (byte[10] << 8) + byte[11];
                realTimeData.lightSleepHour = byte[12];
                realTimeData.lightSleepMinute = byte[13];
                realTimeData.deepSleepHour = byte[14];
                realTimeData.deepSleepMinute = byte[15];
                realTimeData.wakeupNumber = byte[16];
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getRealTimeData:)]) {
                    [self.delegate bleBack_getRealTimeData:realTimeData];
                }
            }
            //设备单机心率
            else if (byte[5] == 0x11) {
                if (byte[11] > 0) {
                    OffLineHeartModel *offLineHeart = [[OffLineHeartModel alloc] init];
                    offLineHeart.year = byte[6] + 2000;
                    offLineHeart.month = byte[7];
                    offLineHeart.day = byte[8];
                    offLineHeart.hour = byte[9];
                    offLineHeart.minute = byte[10];
                    offLineHeart.heart = byte[11];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getOffLineHeart:)]) {
                        [self.delegate bleBack_getOffLineHeart:offLineHeart];
                    }
                }
            }
            //设备单机血压
            else if (byte[5] == 0x14) {
                if ((byte[11] > 0) && (byte[12] > 0)) {
                    OffLineBloodModel *offLineBlood = [[OffLineBloodModel alloc] init];
                    offLineBlood.year = byte[6] + 2000;
                    offLineBlood.month = byte[7];
                    offLineBlood.day = byte[8];
                    offLineBlood.hour = byte[9];
                    offLineBlood.minute = byte[10];
                    offLineBlood.manBlood = byte[11];
                    offLineBlood.minBlood = byte[12];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getOffLineBlood:)]) {
                        [self.delegate bleBack_getOffLineBlood:offLineBlood];
                    }
                }
            }
            //设备单机血氧
            else if (byte[5] == 0x12) {
                if (byte[11] > 0) {
                    OffLineOxygenModel *offLineOxygen = [[OffLineOxygenModel alloc] init];
                    offLineOxygen.year = byte[6] + 2000;
                    offLineOxygen.month = byte[7];
                    offLineOxygen.day = byte[8];
                    offLineOxygen.hour = byte[9];
                    offLineOxygen.minute = byte[10];
                    offLineOxygen.oxygen = byte[11];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getOffLineOxygen:)]) {
                        [self.delegate bleBack_getOffLineOxygen:offLineOxygen];
                    }
                }
            }
            //整点数据
            else if (byte[5] == 0x20) {
                //整点数据需要加一个小时
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"YYYY-MM-dd HH"];
                NSString *str = [NSString stringWithFormat:@"%d-%d-%d %d",byte[6]+2000,byte[7],byte[8],byte[9]];
                NSDate *date = [formatter dateFromString:str];
                NSTimeInterval time = [date timeIntervalSince1970];
                NSDate *currentDate = [NSDate dateWithTimeIntervalSince1970:time+60*60];
                NSString *currentTime = [formatter stringFromDate:currentDate];
                int year = [[currentTime substringWithRange:NSMakeRange(0,4)] intValue];
                int month = [[currentTime substringWithRange:NSMakeRange(5,2)] intValue];
                int day = [[currentTime substringWithRange:NSMakeRange(8,2)] intValue];
                int hour = [[currentTime substringWithRange:NSMakeRange(11,2)] intValue];
                
                AllDataModel *allData = [[AllDataModel alloc] init];
                allData.year = year;
                allData.month = month;
                allData.day = day;
                allData.hour = hour;
                allData.step = byte[10] * 256 * 256 + byte[11] * 256 + byte[12];
                allData.cal = byte[13] * 256 * 256 + byte[14] * 256 + byte[15];
                allData.heart = byte[16];
                allData.oxygen = byte[17];
                allData.maxBlood = byte[18];
                allData.minBlood = byte[19];
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getAllData:)]) {
                    [self.delegate bleBack_getAllData:allData];
                }
            }
        }
        //详细睡眠数据
        else if (byte[4] == 0x52) {
            SleepDataModel *sleepData = [[SleepDataModel alloc] init];
            sleepData.year = byte[6] + 2000;
            sleepData.month = byte[7];
            sleepData.day = byte[8];
            sleepData.hour = byte[9];
            sleepData.minute = byte[10];
            sleepData.state = byte[11];
            sleepData.time = byte[12] * 256 + byte[13];
            if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getSleepWithSleepDataModel:)]) {
                [self.delegate bleBack_getSleepWithSleepDataModel:sleepData];
            }
        }
        //版本号
        else if (byte[4] == 0x92) {
            VersionModel *versionModel = [[VersionModel alloc] init];
            versionModel.version = byte[6] + byte[7]/100.0;
            versionModel.type = byte[8];
            versionModel.mac = [NSString stringWithFormat:@"%02x:%02x:%02x:%02x:%02x:%02x", byte[9], byte[10], byte[11], byte[12], byte[13], byte[14]];
            
            int byteLength = (int)characteristic.value.length;
            int temp15 = byte[15];
            int temp16 = 0;
            int temp17 = 0;
            if (byteLength > 16) {
                temp16 = byte[16];
            }
            if (byteLength > 17) {
                temp17 = byte[17];
            }
            
            //byte[15]
            //是否可设置步长
            if ((temp15 & 0x01) == 0x01) {
                versionModel.isHaveStepLength = YES;
            }
            else {
                versionModel.isHaveStepLength = NO;
            }
            
            //是否可设置睡眠区间
            if ((temp15 & 0x02) == 0x02) {
                versionModel.isHaveSleepRange = YES;
            }
            else {
                versionModel.isHaveSleepRange = NO;
            }
            
            //是否可设置12和24小时制切换
            if ((temp15 & 0x04) == 0x04) {
                versionModel.isHaveHour = YES;
            }
            else {
                versionModel.isHaveHour = NO;
            }
            
            //是否支持苹果健康和微信运动
            if ((temp15 & 0x08) == 0x08) {
                versionModel.isHaveWeChat = NO;
            }
            else {
                versionModel.isHaveWeChat = YES;
            }
            
            //是否可设置心率报警
            if ((temp15 & 0x10) == 0x10) {
                versionModel.isHaveAlertBPM = YES;
            }
            else {
                versionModel.isHaveAlertBPM = NO;
            }
            
            //是否汉天下芯片
            if ((temp15 & 0x20) == 0x20) {
                versionModel.isHanTianXia = YES;
            }
            else {
                versionModel.isHanTianXia = NO;
            }
            
            //byte[16]
            //是否心电设备
            if (temp16 == 12) {
                versionModel.isHaveECG = YES;
            }
            else {
                versionModel.isHaveECG = NO;
            }
            
            //是否连续心率设备
            if (temp16 == 11) {
                versionModel.isHaveBPM = YES;
            }
            else {
                versionModel.isHaveBPM = NO;
            }
            
            //是否连续心率、血压设备
            if (temp16 == 13) {
                versionModel.isHaveBpmBlood = YES;
            }
            else {
                versionModel.isHaveBpmBlood = NO;
            }
            
            //是否连续心率、血压、血氧设备
            if (temp16 == 14) {
                versionModel.isHaveBpmBloodOxygen = YES;
            }
            else {
                versionModel.isHaveBpmBloodOxygen = NO;
            }
            
            //是否连续心率、血氧设备
            if (temp16 == 15) {
                versionModel.isHaveBpmOxygen = YES;
            }
            else {
                versionModel.isHaveBpmOxygen = NO;
            }
            
            //byte[17]
            //是否支持Messager、Facebook Page manager
            if ((temp17 & 0x01) == 0x01) {
                versionModel.isHaveMessager = YES;
            }
            else {
                versionModel.isHaveMessager = NO;
            }
            
            //是否支持Instagram
            if ((temp17 & 0x02) == 0x02) {
                versionModel.isHaveInstagram = YES;
            }
            else {
                versionModel.isHaveInstagram = NO;
            }
            
            //是否可设置久坐间隔
            if ((temp17 & 0x04) == 0x04) {
                versionModel.isHaveSitInterval = YES;
            }
            else {
                versionModel.isHaveSitInterval = NO;
            }
            
            //是否可设置连续心率省电模式
            if ((temp17 & 0x08) == 0x08) {
                versionModel.isHaveBpmPowerSaving = YES;
            }
            else {
                versionModel.isHaveBpmPowerSaving = NO;
            }
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getVersion:)]) {
                [self.delegate bleBack_getVersion:versionModel];
            }
        }
        //电池电量
        else if (byte[4] == 0x91) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getBatteryLevelWithState:value:)]) {
                [self.delegate bleBack_getBatteryLevelWithState:byte[6] value:byte[7]];
            }
        }
        //设备查找手机
        else if (byte[4] == 0x7d) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getDeviceSearchIphoneWithState:)]) {
                [self.delegate bleBack_getDeviceSearchIphoneWithState:byte[6]];
            }
        }
        //摇一摇拍照
        else if (byte[4] == 0x79) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getTakePicture)]) {
                [self.delegate bleBack_getTakePicture];
            }
        }
        //一键测量
        else if (byte[4] == 0x32) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getMeasureAllWithHeart:oxygen:maxBlood:minBlood:)]) {
                [self.delegate bleBack_getMeasureAllWithHeart:byte[6] oxygen:byte[7] maxBlood:byte[8] minBlood:byte[9]];
            }
        }
        //获取实时心率
        else if (byte[4] == 0x84) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getRealTimeDurationHeart:)]) {
                [self.delegate bleBack_getRealTimeDurationHeart:byte[6]];
            }
        }
        else if (byte[4] == 0x31) {
            //单次测量心率
            if (byte[5] == 0x09) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getOneMeasureHeart:)]) {
                    [self.delegate bleBack_getOneMeasureHeart:byte[6]];
                }
            }
            //实时测量心率
            else if (byte[5] == 0x0a) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getRealTimeMeasureHeart:)]) {
                    [self.delegate bleBack_getRealTimeMeasureHeart:byte[6]];
                }
            }
            //单次测量血压
            else if (byte[5] == 0x21) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getOneMeasureBloodWithMaxBlood:minBlood:)]) {
                    [self.delegate bleBack_getOneMeasureBloodWithMaxBlood:byte[6] minBlood:byte[7]];
                }
            }
            //实时测量血压
            else if (byte[5] == 0x22) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getRealTimeMeasureBloodWithMaxBlood:minBlood:)]) {
                    [self.delegate bleBack_getRealTimeMeasureBloodWithMaxBlood:byte[6] minBlood:byte[7]];
                }
            }
            //单次测量血氧
            else if (byte[5] == 0x11) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getOneMeasureOxygen:)]) {
                    [self.delegate bleBack_getOneMeasureOxygen:byte[6]];
                }
            }
            //实时测量血氧
            else if (byte[5] == 0x12) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(bleBack_getRealTimeMeasureOxygen:)]) {
                    [self.delegate bleBack_getRealTimeMeasureOxygen:byte[6]];
                }
            }
        }
        
    }
    
}

#pragma mark - 协议通讯指令
//配对
- (void)ble_sendBondSystem {
    Byte byte[7] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 4;
    byte[3] = 0xff;
    byte[4] = CommandTypeBondSystem;
    byte[5] = 0x80;
    byte[6] = 0;
    NSData *data = [NSData dataWithBytes:byte length:7];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//同步时间
- (void)ble_sendUserTime:(UserDateModel *)userDateModel {
    Byte byte[14] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 11;
    byte[3] = 0xff;
    byte[4] = CommandTypeUserTime;
    byte[5] = 0x80;
    byte[7] = ((userDateModel.year & 0xff00) >> 8);
    byte[8] = (userDateModel.year & 0xff);
    byte[9] = (userDateModel.month & 0xff);
    byte[10] = (userDateModel.day & 0xff);
    byte[11] = (userDateModel.hour & 0xff);
    byte[12] = (userDateModel.minute & 0xff);
    byte[13] = (userDateModel.second & 0xff);
    NSData *data = [NSData dataWithBytes:byte length:14];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//用户信息(适用于我司wearfit 1.0的设备)
- (void)ble_sendUserInfoNormal:(UserInfoNormalModel *)userInfo {
    Byte byte[14] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 11;
    byte[3] = 0xff;
    byte[4] = CommandTypeUserInfo;
    byte[5] = 0x80;
    byte[6] = userInfo.stepLength;
    byte[7] = userInfo.age;
    byte[8] = userInfo.height;
    byte[9] = (int)userInfo.weight;
    byte[10] = 115;//收缩压(几乎不使用,故设默认值)
    byte[11] = 75; //舒张压(几乎不使用,故设默认值)
    byte[12] = userInfo.distanceUnit;
    byte[13] = userInfo.goal/1000;
    NSData *data = [NSData dataWithBytes:byte length:14];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//用户信息(适用于我司wearfit 2.0的设备)
- (void)ble_sendUserInfoAlertBPM:(UserInfoAlertBpmModel *)userInfo {
    Byte byte[14] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 11;
    byte[3] = 0xff;
    byte[4] = CommandTypeUserInfo;
    byte[5] = 0x80;
    byte[6] = userInfo.stepLength;
    byte[7] = userInfo.age;
    byte[8] = userInfo.height;
    byte[9] = (int)userInfo.weight;
    byte[10] = userInfo.distanceUnit;
    byte[11] = userInfo.goal/1000;
    byte[12] = userInfo.minBPM;
    byte[13] = userInfo.maxBPM;
    NSData *data = [NSData dataWithBytes:byte length:14];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//设备闹钟
- (void)ble_sendAlert:(TimeAlertModel *)timeAlertModel {
    Byte byte[11] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 8;
    byte[3] = 0xff;
    byte[4] = CommandTypeTimeAlert;
    byte[5] = 0x80;
    byte[6] = timeAlertModel.identifier;
    byte[7] = timeAlertModel.isOn;
    byte[8] = timeAlertModel.hour;
    byte[9] = timeAlertModel.minute;
    byte[10] = timeAlertModel.control;
    NSData *data = [NSData dataWithBytes:byte length:11];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//查找设备
- (void)ble_sendFindDevice {
    Byte byte[6] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 3;
    byte[3] = 0xff;
    byte[4] = CommandTypeFind;
    byte[5] = 0x80;
    NSData *data = [NSData dataWithBytes:byte length:6];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//久坐提醒
- (void)ble_sendLongTimeSit:(LongTimeSitModel*)longTimeSitModel {
    Byte byte[11] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 8;
    byte[3] = 0xff;
    byte[4] = CommandTypeSit;
    byte[5] = 0x80;
    byte[6] = longTimeSitModel.on;
    byte[7] = longTimeSitModel.startHour;
    byte[8] = longTimeSitModel.startMinute;
    byte[9] = longTimeSitModel.endHour;
    byte[10] = longTimeSitModel.endMinute;
    NSData *data = [NSData dataWithBytes:byte length:11];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//勿扰模式
- (void)ble_sendIgnore:(IgnoreModel *)ignoreModel {
    Byte byte[11] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 8;
    byte[3] = 0xff;
    byte[4] = CommandTypeIgnore;
    byte[5] = 0x80;
    byte[6] = ignoreModel.on;
    byte[7] = ignoreModel.startHour;
    byte[8] = ignoreModel.startMinute;
    byte[9] = ignoreModel.endHour;
    byte[10] = ignoreModel.endMinute;
    NSData *data = [NSData dataWithBytes:byte length:11];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//设置睡眠时间范围
- (void)ble_sendSleepTimeRange:(SleepTimeRangeModel *)sleepTimeRangeModel {
    Byte byte[11] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 8;
    byte[3] = 0xff;
    byte[4] = CommandTypeSleepTimeSet;
    byte[5] = 0x80;
    byte[6] = sleepTimeRangeModel.on;
    byte[7] = sleepTimeRangeModel.startHour;
    byte[8] = sleepTimeRangeModel.startMinute;
    byte[9] = sleepTimeRangeModel.endHour;
    byte[10] = sleepTimeRangeModel.endMinute;
    NSData *data = [NSData dataWithBytes:byte length:11];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//抬手亮屏
- (void)ble_sendLightWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeLight on:isOn];
}

//整点测量
- (void)ble_sendHourMeasureWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeMeasure on:isOn];
}

//摇摇拍照
- (void)ble_sendTakePictureWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeTakePic on:isOn];
}

//防丢提醒
- (void)ble_sendAntiLostWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeAntiLost on:isOn];
}

//中英文切换
- (void)ble_sendSwitchLanguageWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeSwitchLang on:isOn];
}

//时间制度切换
- (void)ble_sendSwitchTimeFormatterWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeSwitchTimeFormatter on:isOn];
}

//获取电量
- (void)ble_sendBatteryLevel {
    [self sendCommand:CommandTypeBatteryLevel on:YES];
}

//获取版本号
- (void)ble_sendVersion {
    [self sendCommand:CommandTypeVersion on:YES];
}

//清空设备数据
- (void)ble_sendDeleteDeviceData {
    [self sendCommand:CommandTypeDeleteData on:YES];
}

//恢复出厂设置(不删除数据)
- (void)ble_sendDeviceInit {
    [self sendCommand:CommandTypeInit on:YES];
}

//一键测量
- (void)ble_sendMeasureAllWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeMeasureAll on:isOn];
}

//获取实时心率(连续心率设备)
- (void)ble_sendRealTimeDurationHeartWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypeDurationHeart on:isOn];
}

//连续心率省电模式
- (void)ble_sendBpmPowerSavingModeWithOn:(BOOL)isOn {
    [self sendCommand:CommandTypePowerSaving on:isOn];
}

#pragma mark 整点数据、睡眠数据指令(51、52)
//请求整点数据(普通设备)
- (void)ble_sendAllDataWithDateModel:(DateModel *)dateModel {
    Byte byte[12] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 9;
    byte[3] = 0xff;
    byte[4] = 0x51;
    byte[5] = 0x80;
    //控制位 1 byte
    
    //数据值
    byte[7] = dateModel.year - 2000;
    byte[8] = dateModel.month;
    byte[9] = dateModel.day;
    byte[10] = dateModel.hour;
    byte[11] = dateModel.minute;
    NSData *data = [NSData dataWithBytes:byte length:12];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//请求整点数据(连续心率设备)
- (void)ble_sendAllDataBpmWithDateModel:(BpmDateModel *)bpmDateModel {
    Byte byte[17] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 14;
    byte[3] = 0xff;
    byte[4] = 0x51;
    byte[5] = 0x80;
    //控制位 1 byte
    
    //数据值
    byte[7] = bpmDateModel.year - 2000;
    byte[8] = bpmDateModel.month;
    byte[9] = bpmDateModel.day;
    byte[10] = bpmDateModel.hour;
    byte[11] = bpmDateModel.minute;
    
    byte[12] = bpmDateModel.yearBpm - 2000;
    byte[13] = bpmDateModel.monthBpm;
    byte[14] = bpmDateModel.dayBpm;
    byte[15] = bpmDateModel.hourBpm;
    byte[16] = bpmDateModel.minuteBpm;
    
    NSData *data = [NSData dataWithBytes:byte length:17];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//请求详细的睡眠数据
- (void)ble_sendSleepWithDateModel:(SleepDateModel *)sleepDateModel {
    Byte byte[10] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 7;
    byte[3] = 0xff;
    byte[4] = 0x52;
    byte[5] = 0x80;
    //控制位 1 byte
    
    //通过时间戳默认把时间提前一天
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *str = [NSString stringWithFormat:@"%d-%d-%d",sleepDateModel.year,sleepDateModel.month,sleepDateModel.day];
    NSDate *date = [formatter dateFromString:str];
    NSTimeInterval time = [date timeIntervalSince1970];
    NSDate *currentDate = [NSDate dateWithTimeIntervalSince1970:time-86400];
    NSString *currentTime = [formatter stringFromDate:currentDate];
    int year = [[currentTime substringWithRange:NSMakeRange(0,4)] intValue];
    int month = [[currentTime substringWithRange:NSMakeRange(5,2)] intValue];
    int day = [[currentTime substringWithRange:NSMakeRange(8,2)] intValue];
    
    byte[7] = year - 2000;
    byte[8] = month;
    byte[9] = day;
    
    NSData *data = [NSData dataWithBytes:byte length:10];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

#pragma mark 单次测量、实时测量指令
//单次测量心率
- (void)ble_sendOneMeasureHeartWithOn:(BOOL)isOn {
    [self sendMeasure:MeasureTypeHearRate on:isOn];
}

//实时测量心率
- (void)ble_sendRealTimeMeasureHeartWithOn:(BOOL)isOn {
    [self sendMeasure:MeasureTypeHearRateReal on:isOn];
}

//单次测量血压
- (void)ble_sendOneMeasureBloodWithOn:(BOOL)isOn {
    [self sendMeasure:MeasureTypeBlood on:isOn];
}

//实时测量血压
- (void)ble_sendRealTimeMeasureBloodWithOn:(BOOL)isOn {
    [self sendMeasure:MeasureTypeBloodReal on:isOn];
}

//单次测量血氧
- (void)ble_sendOneMeasureOxygenWithOn:(BOOL)isOn {
    [self sendMeasure:MeasureTypeOxygen on:isOn];
}

//实时测量血氧
- (void)ble_sendRealTimeMeasureOxygenWithOn:(BOOL)isOn {
    [self sendMeasure:MeasureTypeOxygenReal on:isOn];
}

#pragma mark 消息提醒指令
//来电提醒
- (void)ble_sendNotificationCallWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeCall on:isOn];
}

//短信提醒
- (void)ble_sendNotificationMessageWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeMessage on:isOn];
}

//QQ消息提醒
- (void)ble_sendNotificationQQWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeQQ on:isOn];
}

//Skype消息提醒
- (void)ble_sendNotificationSkypeWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeSkype on:isOn];
}

//微信消息提醒
- (void)ble_sendNotificationWechatWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeWechat on:isOn];
}

//WhatsApp消息提醒
- (void)ble_sendNotificationWhatsAppWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeWhatsApp on:isOn];
}

//Line消息提醒
- (void)ble_sendNotificationLineWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeLine on:isOn];
}

//Twitter消息提醒
- (void)ble_sendNotificationTwitterWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeTwitter on:isOn];
}

//Facebook消息提醒
- (void)ble_sendNotificationFacebookWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeFacebook on:isOn];
}

//FacebookMessager消息提醒
- (void)ble_sendNotificationFacebookMessagerWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeFacebookMessager on:isOn];
}

//Instagram消息提醒
- (void)ble_sendNotificationInstagramWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeInstagram on:isOn];
}

//微博消息提醒
- (void)ble_sendNotificationWeiboWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeWeibo on:isOn];
}

//KakaoTalk消息提醒
- (void)ble_sendNotificationKakaoTalkWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeKakaoTalk on:isOn];
}

//Snapchat消息提醒
- (void)ble_sendNotificationSnapchatWithOn:(BOOL)isOn {
    [self sendNotify:NotifyTypeSnapchat on:isOn];
}

#pragma mark - 通讯指令二次封装
//CommandType
- (void)sendCommand:(CommandType)type on:(BOOL)isOn {
    Byte byte[7] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 4;
    byte[3] = 0xff;
    byte[4] = type;
    byte[5] = 0x80;
    byte[6] = isOn;
    NSData *data = [NSData dataWithBytes:byte length:7];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//MeasureType
- (void)sendMeasure:(MeasureType)type on:(BOOL)isOn {
    Byte byte[7] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 4;
    byte[3] = 0xff;
    byte[4] = 0x31;
    byte[5] = type;
    byte[6] = isOn;
    NSData *data = [NSData dataWithBytes:byte length:7];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//NotifyType
- (void)sendNotify:(NotifyType)notifyid on:(BOOL)isOn {
    Byte byte[8] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 5;
    byte[3] = 0xff;
    byte[4] = 0x72;
    byte[5] = 0x80;
    byte[6] = notifyid;
    byte[7] = isOn;
    NSData *data = [NSData dataWithBytes:byte length:8];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

#pragma mark - 固件升级指令
//进入ISP模式
- (void)ble_sendEnterISP {
    Byte byte[7] = {0};
    byte[0] = 0xab;
    byte[1] = 0;
    byte[2] = 4;
    byte[3] = 0xff;
    byte[4] = 0x25;
    byte[5] = 0x80;
    byte[6] = 0x01;
    NSData *data = [NSData dataWithBytes:byte length:7];
    [self sendCommand:data toPeripheral:self.activePeripheral];
}

//发送文件数据
- (int)ble_sendFileData:(NSMutableData*)data length:(int)length {
    int pos = 0;
    int tmp = length % 20;
    NSMutableData *packet_data = [[NSMutableData alloc]initWithLength:20];
    for (int i = 0; i < length / 20; i++) {
        packet_data = [self arrayCopy:data andArray2:packet_data andstart1:pos andlength:20];
        [self sendCommand:packet_data toPeripheral:self.activePeripheral];
        pos = pos + 20;
    }
    if(tmp != 0) {
        NSMutableData *packet_data = [[NSMutableData alloc]initWithLength:tmp];
        packet_data = [self arrayCopy:data andArray2:packet_data andstart1:pos andlength:tmp];
        [self sendCommand:packet_data toPeripheral:self.activePeripheral];
    }
    return 0;
}

//设备复位
- (void)ble_sendCmdData:(NSMutableData *)data {
    int tmp1 = (int)data.length % 20;
    int pos1 = 0;
    if (tmp1 != 0) {
        NSMutableData *packet_data = [[NSMutableData alloc]initWithLength:tmp1];
        packet_data = [self arrayCopy:data andArray2:packet_data andstart1:pos1 andlength:tmp1];
        [self.activePeripheral writeValue:packet_data forCharacteristic:self.resetService type:CBCharacteristicWriteWithoutResponse];
    }
}

//升级数据分包
- (NSMutableData *)arrayCopy:(NSMutableData *)array1 andArray2:(NSMutableData *)array2 andstart1:(int)startindex1 andlength:(int)length {
    
    Byte *byte1 = (Byte *)[array1 bytes];
    Byte *byte2 = (Byte *)[array2 bytes];
    if (byte1 !=nil) {
        for (int i=0; i < length; i++) {
            byte2[i] = byte1[i+startindex1];
        }
    }
    return [[NSMutableData alloc] initWithBytes:byte2 length:array2.length];
}

@end
