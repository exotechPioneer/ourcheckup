//
//  BleManager.h
//  BleSDK
//
//  Created by wakeup on 2018/10/25.
//  Copyright © 2018年 mr mao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BleModel.h"
#import "LongTimeSitModel.h"
#import "IgnoreModel.h"
#import "SleepTimeRangeModel.h"
#import "UserDateModel.h"
#import "UserInfoNormalModel.h"
#import "UserInfoAlertBpmModel.h"
#import "TimeAlertModel.h"
#import "DateModel.h"
#import "BpmDateModel.h"
#import "SleepDateModel.h"
#import "RealTimeDataModel.h"
#import "OffLineHeartModel.h"
#import "OffLineOxygenModel.h"
#import "OffLineBloodModel.h"
#import "AllDataModel.h"
#import "SleepDataModel.h"
#import "VersionModel.h"

#define bleSelf [BleManager sharedBleManager]

@protocol BleManagerDelegate <NSObject>

@optional

#pragma mark - 蓝牙库回调
/**
 手机蓝牙状态为打开的回调方法
 */
- (void)bleBack_state_On;

/**
 手机蓝牙状态为关闭的回调方法
 */
- (void)bleBack_state_Off;

/**
 搜索到设备回调方法
 @param bleArray 设备数组(数组里面是BleModel)
 */
- (void)bleBack_scanDevicesWithBleArray:(NSMutableArray *)bleArray;

/**
 连接设备成功回调方法
 @param peripheral 设备对象
 */
- (void)bleBack_connectDeviceSuccessWithPeripheral:(CBPeripheral *)peripheral;

/**
 断开设备回调方法
 @param peripheral 设备对象
 @param error      错误信息
 */
- (void)bleBack_disconnectDeviceWithPeripheral:(CBPeripheral *)peripheral error:(NSError *)error;

/**
 获取实时蓝牙信号回调方法
 @param RSSI  信号值
 @param error 错误信息
 */
- (void)bleBack_readRSSI:(NSNumber *)RSSI error:(NSError *)error;


#pragma mark - 协议通讯指令回调
/**
 获取版本号(wearfit 1.0 和 wearfit 2.0返回来的步长、睡眠区间、12和24小时制度切换是不一致的,详见VersionModel)
 @param versionModel 版本号模型
 */
- (void)bleBack_getVersion:(VersionModel *)versionModel;

/**
 电池电量
 @param state 电池状态(0:未充电 1:充电中)
 @param value 电量值(固定为:0、20、40、60、80、100六个值)
 */
- (void)bleBack_getBatteryLevelWithState:(int)state value:(int)value;

/**
 设备查找手机
 @param state 查找状态(0:停止查找 1:开始查找)
 */
- (void)bleBack_getDeviceSearchIphoneWithState:(int)state;

/**
 摇一摇拍照
 */
- (void)bleBack_getTakePicture;

/**
 一键测量
 @param heart    心率值
 @param oxygen   血氧值
 @param maxBlood 收缩压
 @param minBlood 舒张压
 */
- (void)bleBack_getMeasureAllWithHeart:(int)heart oxygen:(int)oxygen maxBlood:(int)maxBlood minBlood:(int)minBlood;

/**
 获取实时心率(连续心率设备)
 @param heart 心率值
 */
- (void)bleBack_getRealTimeDurationHeart:(int)heart;

#pragma mark 整点数据、睡眠数据指令回调(51、52)
/**
 实时数据
 @param realTimeDataModel 实时数据模型
 */
- (void)bleBack_getRealTimeData:(RealTimeDataModel *)realTimeDataModel;

/**
 设备单机测量的心率(普通手环为每个整点一条数据,连续心率默认为5分钟一条数据)
 @param offLineHeartModel 设备单机测量心率模型
 */
- (void)bleBack_getOffLineHeart:(OffLineHeartModel *)offLineHeartModel;

/**
 设备单机测量的血压
 @param offLineBloodModel 设备单机测量血压模型
 */
- (void)bleBack_getOffLineBlood:(OffLineBloodModel *)offLineBloodModel;

/**
 设备单机测量的血氧
 @param offLineOxygenModel 设备单机测量血氧模型
 */
- (void)bleBack_getOffLineOxygen:(OffLineOxygenModel *)offLineOxygenModel;

/**
 设备整点数据
 @param allDataModel 返回整点数据时间模型
 */
- (void)bleBack_getAllData:(AllDataModel *)allDataModel;

/**
 详细睡眠数据
 */
- (void)bleBack_getSleepWithSleepDataModel:(SleepDataModel *)sleepDataModel;

#pragma mark 单次测量、实时测量指令回调
/**
 单次测量心率
 @param heart 心率值
 */
- (void)bleBack_getOneMeasureHeart:(int)heart;

/**
 实时测量心率
 @param heart 心率值
 */
- (void)bleBack_getRealTimeMeasureHeart:(int)heart;

/**
 单次测量血压
 @param maxBlood 收缩压
 @param minBlood 舒张压
 */
- (void)bleBack_getOneMeasureBloodWithMaxBlood:(int)maxBlood minBlood:(int)minBlood;

/**
 实时测量血压
 @param maxBlood 收缩压
 @param minBlood 舒张压
 */
- (void)bleBack_getRealTimeMeasureBloodWithMaxBlood:(int)maxBlood minBlood:(int)minBlood;

/**
 单次测量血氧
 @param oxygen 血氧值
 */
- (void)bleBack_getOneMeasureOxygen:(int)oxygen;

/**
 实时测量血氧
 @param oxygen 血氧值
 */
- (void)bleBack_getRealTimeMeasureOxygen:(int)oxygen;

#pragma mark 固件升级指令回调
/**
 OTA数据
 @param data 数据
 @param type 数据类型(0:错误数据 1:正常数据 2:复位数据)
 */
- (void)bleBack_getOtaData:(NSMutableData *)data type:(int)type;

@end


@interface BleManager : NSObject

@property (nonatomic, weak) id <BleManagerDelegate> delegate;

//蓝牙管理者
@property (nonatomic, strong) CBCentralManager *centralManager;

//处于连接状态的设备
@property (nonatomic, strong) CBPeripheral *activePeripheral;

//蓝牙的信息，用来重连(此重连方法只局限于当前手机重连,不懂通过UUIDString重连的,可先百度)
@property (nonatomic, copy) NSString *UUIDString;


+ (instancetype)sharedBleManager;


#pragma mark - 蓝牙库指令
/**
 开始搜索设备(当调用连接设备方法会自动停止搜索)
 @param duration 持续时间(单位:秒)
 */
- (void)ble_startScanBleDevicesWithDuration:(NSInteger)duration;

/**
 停止搜索设备
 */
- (void)ble_stopScanBleDevices;

/**
 连接设备
 @param peripheral 需要连接的设备
 */
- (void)ble_connectBleDevice:(CBPeripheral *)peripheral;

/**
 断开设备
 */
- (void)ble_disconnectBleDevice;

/**
 获取实时蓝牙信号
 */
- (void)ble_readRSSI;


#pragma mark - 协议通讯指令
/**
 配对
 */
- (void)ble_sendBondSystem;

/**
 同步时间
 @param userDateModel 用户时间模型
 */
- (void)ble_sendUserTime:(UserDateModel *)userDateModel;

/**
 用户信息(适用于我司wearfit 1.0的设备,具体情况请咨询我司固件开发人员,或看VersionModel模型的type属性)
 @param userInfo 用户信息模型
 */
- (void)ble_sendUserInfoNormal:(UserInfoNormalModel *)userInfo;

/**
 用户信息(适用于我司wearfit 2.0的设备,具体情况请咨询我司固件开发人员,或看VersionModel模型的type属性)
 @param userInfo 用户信息模型
 */
- (void)ble_sendUserInfoAlertBPM:(UserInfoAlertBpmModel *)userInfo;

/**
 设备闹钟(每次APP连上设备,设备都会清空所有的闹钟设置,所以APP每次连接上设备都需要重新下发所有的闹钟设置)
 @param timeAlertModel 闹钟对象
 */
- (void)ble_sendAlert:(TimeAlertModel *)timeAlertModel;

/**
 查找设备
 */
- (void)ble_sendFindDevice;

/**
 久坐提醒
 @param longTimeSitModel 久坐提醒模型
 */
- (void)ble_sendLongTimeSit:(LongTimeSitModel*)longTimeSitModel;

/**
 勿扰模式
 @param ignoreModel 勿扰模式模型
 */
- (void)ble_sendIgnore:(IgnoreModel *)ignoreModel;

/**
 设置睡眠时间范围
 @param sleepTimeRangeModel 睡眠时间范围模型
 */
- (void)ble_sendSleepTimeRange:(SleepTimeRangeModel *)sleepTimeRangeModel;

/**
 抬手亮屏
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendLightWithOn:(BOOL)isOn;

/**
 整点测量(普通设备)
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendHourMeasureWithOn:(BOOL)isOn;

/**
 摇一摇拍照
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendTakePictureWithOn:(BOOL)isOn;

/**
 防丢提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendAntiLostWithOn:(BOOL)isOn;

/**
 中英文切换
 @param isOn YES:英文 NO:中文
 */
- (void)ble_sendSwitchLanguageWithOn:(BOOL)isOn;

/**
 时间制度切换
 @param isOn YES:12小时制 NO:24小时制
 */
- (void)ble_sendSwitchTimeFormatterWithOn:(BOOL)isOn;

/**
 获取电量
 */
- (void)ble_sendBatteryLevel;

/**
 获取版本号
 */
- (void)ble_sendVersion;

/**
 清空设备数据
 */
- (void)ble_sendDeleteDeviceData;

/**
 恢复出厂设置(不删除数据)
 */
- (void)ble_sendDeviceInit;

/**
 一键测量(普通设备；开始测量1分钟后,需要下发关闭测量指令,才会有一个值返回)
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendMeasureAllWithOn:(BOOL)isOn;

/**
 获取实时心率(连续心率设备)
 @param isOn YES:开始获取 NO:停止获取
 */
- (void)ble_sendRealTimeDurationHeartWithOn:(BOOL)isOn;

/**
 连续心率省电模式(连续心率设备)
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendBpmPowerSavingModeWithOn:(BOOL)isOn;

#pragma mark 整点数据、睡眠数据指令(51、52)
/**
 整点数据指令注意：
 1、请求整点数据，返回来的数据除了整点数据外，还有实时数据、单机测量数据、实时测量数据
 2、如果未开启整点测量就不会有整点数据，但是不管什么设备依旧会返回实时数据、单机测量数据、实时测量数据
 3、请求整点数据分为普通设备和连续心率设备两种情况
 4、普通设备：需要先通过开启整点测量(ble_sendHourMeasureWithOn)，并且测有整点数据的情况下才会返回整点数据
 5、连续心率设备：连续心率设备无需开启或关闭整点测量(关闭不了)，默认就是开启的
 6、连续心率设备发送整点数据指令，获取的整点数据没有心率、血压、血氧，其他数据如果有，则返回
 7、设备实时数据不做保存，其他数据只保存7天
 8、例子：发送2018年11月1日0点时间给设备，设备返回2018年11月1日0点到2018年11月7日0点的数据(如果有)
 
 睡眠数据指令注意：
 1、睡眠数据，设备也是只保存7天
 2、例子：发送2018年11月2日时间给设备，设备返回2018年11月1日到2018年11月7日0点的数据
 */

/**
 请求整点数据(普通设备)
 @param dateModel 时间模型
 */
- (void)ble_sendAllDataWithDateModel:(DateModel *)dateModel;

/**
 请求整点数据(连续心率设备)
 @param bpmDateModel 连续心率时间模型
 */
- (void)ble_sendAllDataBpmWithDateModel:(BpmDateModel *)bpmDateModel;

/**
 请求详细的睡眠数据
 @param sleepDateModel 睡眠时间模型
 */
- (void)ble_sendSleepWithDateModel:(SleepDateModel *)sleepDateModel;

#pragma mark 单次测量、实时测量指令
/**
 注意：1、以下单次测量、实时测量指令只有普通设备和心电设备才有，连续心率设备没有
 2、开始测量40秒后,需要下发关闭测量指令,才会有一个值返回
 */

/**
 单次测量心率
 @param isOn YES:开始测量 NO:关闭测量
 */
- (void)ble_sendOneMeasureHeartWithOn:(BOOL)isOn;

/**
 实时测量心率
 @param isOn YES:开始测量 NO:关闭测量
 */
- (void)ble_sendRealTimeMeasureHeartWithOn:(BOOL)isOn;

/**
 单次测量血压
 @param isOn YES:开始测量 NO:关闭测量
 */
- (void)ble_sendOneMeasureBloodWithOn:(BOOL)isOn;

/**
 实时测量血压
 @param isOn YES:开始测量 NO:关闭测量
 */
- (void)ble_sendRealTimeMeasureBloodWithOn:(BOOL)isOn;

/**
 单次测量血氧
 @param isOn YES:开始测量 NO:关闭测量
 */
- (void)ble_sendOneMeasureOxygenWithOn:(BOOL)isOn;

/**
 实时测量血氧
 @param isOn YES:开始测量 NO:关闭测量
 */
- (void)ble_sendRealTimeMeasureOxygenWithOn:(BOOL)isOn;

#pragma mark 消息提醒指令
/**
 注意：1、消息提醒指令有些设备只支持部分消息提醒，具体情况请咨询我司固件开发人员
 2、下发获取版本号指令(ble_sendVersion)，可根据代理方法的VersionModel模型参数判断是否支持
 */

/**
 来电提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationCallWithOn:(BOOL)isOn;

/**
 短信提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationMessageWithOn:(BOOL)isOn;

/**
 QQ消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationQQWithOn:(BOOL)isOn;

/**
 Skype消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationSkypeWithOn:(BOOL)isOn;

/**
 微信消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationWechatWithOn:(BOOL)isOn;

/**
 WhatsApp消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationWhatsAppWithOn:(BOOL)isOn;

/**
 Line消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationLineWithOn:(BOOL)isOn;

/**
 Twitter消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationTwitterWithOn:(BOOL)isOn;

/**
 Facebook消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationFacebookWithOn:(BOOL)isOn;

/**
 FacebookMessager消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationFacebookMessagerWithOn:(BOOL)isOn;

/**
 Instagram消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationInstagramWithOn:(BOOL)isOn;

/**
 微博消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationWeiboWithOn:(BOOL)isOn;

/**
 KakaoTalk消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationKakaoTalkWithOn:(BOOL)isOn;

/**
 Snapchat消息提醒
 @param isOn YES:打开 NO:关闭
 */
- (void)ble_sendNotificationSnapchatWithOn:(BOOL)isOn;

#pragma mark 固件升级指令
/**
 进入ISP模式
 */
- (void)ble_sendEnterISP;

/**
 发送文件数据
 @param data   数据
 @param length 数据长度
 */
- (int)ble_sendFileData:(NSMutableData*)data length:(int)length;

/**
 设备复位
 */
- (void)ble_sendCmdData:(NSMutableData *)data;

@end
