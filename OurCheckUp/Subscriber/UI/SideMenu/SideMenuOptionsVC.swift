//
//  SideMenuOptionsVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 06/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class SideMenuOptionsVC: BaseVC {

    enum Option : String {
        case Home = "Home"
        case Notification = "Notification"
        case Account_Profile = "Account/Profile"
        case Version = "Version"
        case TermsofService = "Terms of Service"
        case PrivacyPolicy = "Privacy Policy"
        case Setting = "Setting"
        case Help_Support = "Help & Support"
        case Logout = "Logout"
    }
    
    @IBOutlet weak var tableView : UITableView!
   
    var menuOptions:[Option] = [.Home,.Account_Profile,.Notification,.Version,.PrivacyPolicy,.Setting,.Help_Support,.Logout]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let parent = self.parent as? DrSideDrawerVC{
            //if parent.isDoctorMod{
                menuOptions = [.Home,.Account_Profile,.Notification,.Version,.PrivacyPolicy,.Setting,.Help_Support,.Logout]
            //}
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }

}
//MARK:- UITableViewDataSource
extension SideMenuOptionsVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return reportNameCell(tableView,cellForRowAt:indexPath)
    }
    
    func reportNameCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = menuOptions[indexPath.row].rawValue
        return cell!
    }
}
//MARK:- UITableViewDelegate
extension SideMenuOptionsVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch menuOptions[indexPath.row] {
        case .Home:
            if let controllers = self.navigationController?.viewControllers{
                for c in controllers{
                    if c is SubscriberDashboardVC{
                        navigationController?.popToViewController(c, animated: true)
                        break
                    }
                }
            }
            
            break
        case .Account_Profile:
            self.performSegue(id: .subscriberProfile)
            break
        case .Notification:
            break
        case .Version:
            displayAlert(Option.Version.rawValue, andMessage: appVersion())
            break
        case .PrivacyPolicy:
            self.performSegue(id: .privacypolicy)
            break
        case .Setting:
            let st = UIStoryboard(name: "Doctor", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "SettingsVC")
            self.navigationController?.pushViewController(vc, animated: true)
        case .Logout:
            AppPreferences.share.commit(data: false, forkey: .isLogin)
            AppPreferences.share.clear()
            self.dismiss(animated: true)
            break
        default:
            break
        }
        SideDrawerVC.share?.closeDrawer()
    }
    
}
