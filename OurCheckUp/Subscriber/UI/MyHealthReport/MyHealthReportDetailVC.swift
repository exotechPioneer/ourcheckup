//
//  MyHealthReportVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class MyHealthReportDetailVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    let CELL_ID = "cellid"
    
    var reportList : [QuarterlyReportMdl.Data]?
    
    var reportType :String?
    var reportYear : String?
    var isInitialReport = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: "MY HEALTH REPORT")
        //createTestList()
        setUpTableView()
        if isInitialReport{
            getInitialReport()
        }else{
            getReport()
        }
        
    }
    
    func setUpTableView(){
        self.tableView.register("MyHealthReportDetailTC".toNib(), forCellReuseIdentifier: CELL_ID)
        self.tableView.tableFooterView = UIView()
        self.tableView.dataSource = self
    }
    
    func getInitialReport(){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        var params  = [String:Any]()
        params["user_id"] = userID
        self.showProgress()
        Api.share.getInitialReport(params: params) { (dataModal, status) in
            self.hideProgress()
            if status{
                if let dataModal = dataModal{
                    self.reportList = []
                    if dataModal.data.count > 0{
                        for element in dataModal.data{
                            if element.vital == "deep_sleep" ||
                                element.vital == "shallow_sleep" ||
                                element.vital == "sleep_hours" ||
                                element.vital == "sleep_minutes"{
                                //self.reportList?.remove(at: i)
                            }else if element.vital == "blood_oxygen_real_time"{
                                // self.reportList?.remove(at: i)
                            }else if element.vital == "heart_rate_real_time"{
                                //self.reportList?.remove(at: i)
                            }else{
                                self.reportList?.append(element)
                            }
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        self.getCVD()
                    }else{
                        DispatchQueue.main.async {
                            showError(error: "Report is not available for selected preferences.")
                        }
                    }
                    
                }
            }
        }
    }
    
    func getReport(){
        if let reportType = self.reportType,let year = self.reportYear{
            let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
            var params  = [String:Any]()
            params["user_id"] = userID
            params["quarter"] = reportType
            params["year"] = year
            
            self.showProgress()
            Api.share.getMedicalReport(params: params) { (dataModal, status) in
                self.hideProgress()
                if status{
                    if let dataModal = dataModal{
                        self.reportList = []
                        if dataModal.data.count > 0{
                            for element in dataModal.data{
                                if element.vital == "deep_sleep" ||
                                element.vital == "shallow_sleep" ||
                                element.vital == "sleep_hours" ||
                                    element.vital == "sleep_minutes"{
                                    //self.reportList?.remove(at: i)
                                }else if element.vital == "blood_oxygen_real_time"{
                                   // self.reportList?.remove(at: i)
                                }else if element.vital == "heart_rate_real_time"{
                                    //self.reportList?.remove(at: i)
                                }else{
                                    self.reportList?.append(element)
                                }
                            }
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                            self.getCVD()
                        }else{
                            DispatchQueue.main.async {
                                showError(error: "Report is not available for selected preferences.")
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    
    func getCVD(){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        var params  = [String:Any]()
        params["user_id"] = "\(userID)"
        self.showProgress()
        Api.share.getCVDRisk(params: params) { (cvd, status) in
            self.hideProgress()
            if status{
                if let data = cvd?.data{
                    if data.count > 0{
                         self.reportList?.append(QuarterlyReportMdl.Data(vital: "CVD risk percentage ", cvd: data[0].tenYearsCvd ?? 0))
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    /*func createTestList(){
        
        initialReportList.append(InitialReportModal(vital: "Body Weight", initial: "49",  target: "45",
                                                    performancePercen: "62.5%",performanceString: "Target reached"))
        
        initialReportList.append(InitialReportModal( vital: "Heart Rate", initial: "80", target: "60-90",
                                                     performancePercen: "65%",  performanceString: "In Target range"))
        
        initialReportList.append(InitialReportModal( vital: "Blood Pressure",
                                                     initial: "",  target: "",  performancePercen: "",  performanceString: ""))
        initialReportList.append(InitialReportModal( vital: "Systolic", initial: "145",
                                                     target: "110-135",  performancePercen: "79.6%",  performanceString: "In Target range"))
        initialReportList.append(InitialReportModal( vital: "Diastolic", initial: "90",
                                                     target: "75-80",  performancePercen: "76.9%",  performanceString: "In Target range"))
        
        initialReportList.append(InitialReportModal( vital: "BMI", initial: "28",
                                                     target: "25",  performancePercen: "33%",  performanceString: "Target reached"))
        
        initialReportList.append(InitialReportModal( vital: "BMR", initial: "1780",
                                                     target: "1980-2238",  performancePercen: "50%",  performanceString: "In Target range"))
        
        initialReportList.append(InitialReportModal( vital: "Visceral Fat", initial: "18",
                                                     target: "<13",  performancePercen: "60%",  performanceString: "Target reached"))
        
        initialReportList.append(InitialReportModal( vital: "Body Water", initial: "60",
                                                     target: "45-50",  performancePercen: "66.7%",  performanceString: "Target Reached"))
        
        initialReportList.append(InitialReportModal( vital: "Muscles Mass", initial: "35.6",
                                                     target: ">30",  performancePercen: "100%",  performanceString: "Target Reached"))
        
        initialReportList.append(InitialReportModal( vital: "Metabolic Age", initial: "45",
                                                     target: "40",  performancePercen: "",  performanceString: "Unmatched"))
        
        initialReportList.append(InitialReportModal( vital: "Average Steps taken", initial: "NA",
                                                     target: "10000",  performancePercen: "34%",  performanceString: "In Target range"))
        
        initialReportList.append(InitialReportModal( vital: "SPO2", initial: "90",
                                                     target: ">95%",  performancePercen: "56%",  performanceString: "In Target Range"))
        
        initialReportList.append(InitialReportModal( vital: "Fasting Blood Sugar", initial: "4.55",
                                                     target: "<6.05",  performancePercen: "19%",  performanceString: "Outside Target"))
        
        initialReportList.append(InitialReportModal( vital: "HBA1C *", initial: "5.11",
                                                     target: "<6.00",  performancePercen: "",  performanceString: "With in target"))
        
        initialReportList.append(InitialReportModal( vital: "HDL Cholesterol *", initial: "1.75",
                                                     target: ">1.55",  performancePercen: "7%",  performanceString: "Outside Target"))
        
        initialReportList.append(InitialReportModal( vital: "LDL Cholesterol *", initial: "2.40",
                                                     target: "<2.60",  performancePercen: "4%",  performanceString: "Outside Target"))
        
        initialReportList.append(InitialReportModal( vital: "Total Cholesterol *", initial: "5.01",
                                                     target: "<5.20",  performancePercen: "4%",  performanceString: "Within Target"))
        
    }*/
}
//MARK:- UITableViewDataSource
extension MyHealthReportDetailVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let reportList = self.reportList{
            return reportList.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return reportNameCell(tableView,cellForRowAt:indexPath)
    }
    
    func reportNameCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! MyHealthReportDetailTC
        cell.setData(model: reportList![indexPath.row])
        return cell
    }
}
extension MyHealthReportDetailVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
