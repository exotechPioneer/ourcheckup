//
//  MyHealthReportVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class MyHealthReportVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    
    let CELL_ID = "cellid"
    let HEADER_ID = "headerid"
    
    var reports : [MyHealthReportMdl]?
    
    var reportType:String?
    var reportYear:String?
    var isInitialReport = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: "MY HEALTH REPORT")
        createData()
        setUpTableView()
    }
    
    func setUpTableView(){
        self.tableView.register("MyHealthReportTC".toNib(), forCellReuseIdentifier: CELL_ID)
        self.tableView.register("MyHealthReportTblHeader".toNib(), forHeaderFooterViewReuseIdentifier: HEADER_ID)
       
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    func createData(){
        let r1 = MyHealthReportMdl(reportType: "INITIAL REPORT", reports: nil, isSubViewHidden: false)
        let r21 = Report(name: "Quater 1", id: "1")
        let r22 = Report(name: "Quater 2", id: "2")
        let r23 = Report(name: "Quater 3", id: "3")
        let r24 = Report(name: "Quater 4", id: "4")
        
        let r2 = MyHealthReportMdl(reportType: "QUATERLY REPORT", reports: nil, isSubViewHidden: false)
        
        let r3 = MyHealthReportMdl(reportType: "SELECT YEAR", reports: [r21,r22,r23,r24], isSubViewHidden: false)
        
        //let r31 = Report(name: "Year 2017")
        let r42 = Report(name: "Year 2018", id: "2018")
        
        let r4 = MyHealthReportMdl(reportType: "YEARLY REPORT", reports: [r42], isSubViewHidden: false)
        
        let r5 = MyHealthReportMdl(reportType: "PROGRESS REPORT", reports: nil, isSubViewHidden: false)
        
        reports = [r1,r2,r3,r4,r5]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let id = segue.identifier{
            switch id{
            case SubscriberSegueIds.reportDetail.rawValue:
                let destVC = segue.destination as? MyHealthReportDetailVC
                destVC?.reportYear = self.reportYear
                destVC?.reportType = self.reportType
                destVC?.isInitialReport = self.isInitialReport
                break
            default:
                break
            }
        }
    }
}

//MARK:- UITableViewDataSource
extension MyHealthReportVC : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if let count = reports?.count{
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let report = reports?[section]{
            if  (report.reports != nil) && !report.isSubViewHidden{
                return report.reports!.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return reportNameCell(tableView,cellForRowAt:indexPath)
    }
    
    func reportNameCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! MyHealthReportTC
        if let report = reports?[indexPath.section]{
            cell.configure(indexPath: indexPath,report: report.reports![indexPath.row])
        }
        
        return cell
    }
}
//MARK:- UITableViewDelegate
extension MyHealthReportVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: HEADER_ID) as! MyHealthReportTblHeader
        view.configure(section: section, report: reports![section])
        view.delegate = self
        view.tag = section
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onHeaderTap)))
        return view
    }
    
    @objc func onHeaderTap(gesture:UITapGestureRecognizer){
        if let tag =  gesture.view?.tag{
            switch tag{
            case 4:
                let story = UIStoryboard.init(name: "Doctor", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "DrProgressReportVC")
                self.navigationController?.pushViewController(vc, animated: true)
                break
            case 0:
                self.isInitialReport = true
                self.performSegue(id: .initialReportDetail)
            case 2:
                self.showYearPicker()
                break
            default:
                break
            }
          
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.reportType = reports![indexPath.section].reports![indexPath.row].id
       // self.reportYear = reportYear
        if let reportYear = self.reportYear,!reportYear.isEmpty,indexPath.section == 2{
             self.performSegue(id: .reportDetail)
        }else{
            self.showYearPicker()
        }
       
    }
    
    func showYearPicker(){
        let vc = YearPickerVC()
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.present(vc, animated: true)
    }
    
}

extension MyHealthReportVC : MyHealthReportTblHeaderDelegate{
    func onDropArrowTapped(isHidden: Bool, section: Int) {
        self.reports?[section].isSubViewHidden = isHidden
        let index = IndexSet(integer: section)
        tableView.reloadSections(index, with: .automatic)
    }
}

extension MyHealthReportVC : YearPickerVCDelegate{
    func yearPicker(didCancel cancel: Bool) {
        
    }
    func yearPicker(didSelect year: String) {
        self.reportYear = year
        self.reports?[2].reportType = year
        self.tableView.reloadData()
    }
}
