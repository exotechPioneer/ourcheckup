//
//  MyHealthReportDetailTC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class MyHealthReportDetailTC: UITableViewCell {

    @IBOutlet weak var lblTestName:UILabel!
    @IBOutlet weak var lblInitialValue:UILabel!
    @IBOutlet weak var lblTargetValue:UILabel!
    @IBOutlet weak var lblTargetPercentageValue:UILabel!
    @IBOutlet weak var lblTargetStatus:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setData(model:QuarterlyReportMdl.Data){
        lblTestName.text = model.vital
        if model.isCvd{
            lblTestName.textColor = .red
            lblInitialValue.text = ""
            lblTargetValue.text = ""
            lblTargetPercentageValue.text = "\(model.cvd ?? 0) %"
            lblTargetStatus.text = ""
            let subViews = self.contentView.subviews
            if subViews.count > 0{
                subViews[0].backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
            }
        }else{
            lblInitialValue.text = "\(model.initial ?? 0)"
            lblTargetValue.text = "\(model.target ?? 0)"
            let (percentage,performance) = getPercentage(model: model)
            lblTargetPercentageValue.text = percentage
            lblTargetStatus.text = performance
        }
    }
    
    
    func getPercentage(model:QuarterlyReportMdl.Data)->(String,String){
        if let initialVal = model.initial{
            if let targetVal = model.target{
                if let achieveVal = model.achieved{
                    switch model.vital {
                    case "heart_rate","dia","sys","visceral_fat","muscle","steps":
                        if let normalResult = model.normalCount,let totalResult = model.totalCount{
                            let result =  (normalResult/totalResult)*100
                            return ("\(result)%",getPerformance(percentage: result))
                        }
                        break
                    case "bmi","bmr","moisture","body_weight":
                        let result = ((initialVal - achieveVal)/(initialVal-targetVal))*100
                        return  ("\(result)%",getPerformance(percentage: result))
                    case "blood_oxygen","HBA1C","HDL_Cholesterol","LDL_Cholesterol":
                        let result = ((targetVal - achieveVal)/targetVal)*100
                         return ("\(result)%",getPerformance(percentage: result))
                    default:
                        break
                    }
                }
            }
        }
        return ("0%",getPerformance(percentage: 0))
    }
    
    func getPerformance(percentage:Int)->String{
        switch percentage {
        case 0...30:
            return "Outside Target"
        case 30...50:
            return "In target range"
        case 50...100:
            return "Target reached"
        default:
          break
        }
        return ""
    }
}
