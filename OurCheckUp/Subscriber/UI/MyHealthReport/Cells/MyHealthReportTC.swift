//
//  MyHealthReportTC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class MyHealthReportTC: UITableViewCell {

    @IBOutlet weak var lblReportName: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(indexPath:IndexPath,report:Report){
        self.lblReportName.text = report.name
    }
    
}
