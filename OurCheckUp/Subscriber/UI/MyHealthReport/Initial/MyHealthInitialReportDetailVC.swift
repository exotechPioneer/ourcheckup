//
//  MyHealthReportVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class MyHealthInitialReportDetailVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    let CELL_ID = "cellid"
    
    var reportList : [QuarterlyReportMdl.Data]?
    
    var reportType :String?
    var reportYear : String?
    var isInitialReport = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: "MY HEALTH REPORT")
        //createTestList()
        setUpTableView()
        getInitialReport()
    }
    
    func setUpTableView(){
        self.tableView.register("MyHealthInitialReportDetailTC".toNib(), forCellReuseIdentifier: CELL_ID)
        self.tableView.tableFooterView = UIView()
        self.tableView.dataSource = self
    }
    
    func getInitialReport(){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        var params  = [String:Any]()
        params["user_id"] = userID
        self.showProgress()
        Api.share.getInitialReport(params: params) { (dataModal, status) in
            self.hideProgress()
            if status{
                if let dataModal = dataModal{
                    self.reportList = []
                    if dataModal.data.count > 0{
                        for element in dataModal.data{
                            if element.vital == "deep_sleep" ||
                                element.vital == "shallow_sleep" ||
                                element.vital == "sleep_hours" ||
                                element.vital == "sleep_minutes"{
                                //self.reportList?.remove(at: i)
                            }else if element.vital == "blood_oxygen_real_time"{
                                // self.reportList?.remove(at: i)
                            }else if element.vital == "heart_rate_real_time"{
                                //self.reportList?.remove(at: i)
                            }else{
                                self.reportList?.append(element)
                            }
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        self.getCVD()
                    }else{
                        DispatchQueue.main.async {
                            showError(error: "Report is not available for selected preferences.")
                        }
                    }
                    
                }
            }
        }
    }
    
    func getCVD(){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        var params  = [String:Any]()
        params["user_id"] = "\(userID)"
        self.showProgress()
        Api.share.getCVDRisk(params: params) { (cvd, status) in
            self.hideProgress()
            if status{
                if let data = cvd?.data{
                    if data.count > 0{
                        self.reportList?.append(QuarterlyReportMdl.Data(vital: "CVD risk percentage ", cvd: data[0].tenYearsCvd ?? 0))
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    
}
//MARK:- UITableViewDataSource
extension MyHealthInitialReportDetailVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let reportList = self.reportList{
            return reportList.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return reportNameCell(tableView,cellForRowAt:indexPath)
    }
    
    func reportNameCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! MyHealthInitialReportDetailTC
        cell.setData(model: reportList![indexPath.row])
        return cell
    }
}
extension MyHealthInitialReportDetailVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
