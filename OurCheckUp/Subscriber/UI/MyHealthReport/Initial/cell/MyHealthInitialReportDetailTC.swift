//
//  MyHealthReportDetailTC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class MyHealthInitialReportDetailTC: UITableViewCell {

    @IBOutlet weak var lblTestName:UILabel!
    @IBOutlet weak var lblInitialValue:UILabel!
    @IBOutlet weak var lblTargetValue:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setData(model:QuarterlyReportMdl.Data){
        lblTestName.text = model.vital
        if model.isCvd{
            lblTestName.textColor = .red
            lblInitialValue.text = ""
            lblTargetValue.text = ""
            let subViews = self.contentView.subviews
            if subViews.count > 0{
                subViews[0].backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
            }
        }else{
            lblInitialValue.text = "\(model.initial ?? 0)"
            lblTargetValue.text = "\(model.target ?? 0)"
        }
    }
   
}
