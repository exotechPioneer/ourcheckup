//
//  MyHealthReportTblHeader.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

protocol MyHealthReportTblHeaderDelegate{
    func onDropArrowTapped(isHidden:Bool,section:Int)
}

class MyHealthReportTblHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var lblReportType: UILabel!
    
    @IBOutlet weak var btnArrow: UIButton!
    
    var isReportsHidden = false
    
    var section : Int!
    
    var delegate: MyHealthReportTblHeaderDelegate?
    
    @IBAction func btnArrowAction(_ sender: UIButton) {
        if !isReportsHidden{
            btnArrow.setImage(#imageLiteral(resourceName: "dropdownarrow"), for: .normal)
        }else{
            btnArrow.setImage(#imageLiteral(resourceName: "uparrow"), for: .normal)
        }
        
        isReportsHidden = !isReportsHidden
        delegate?.onDropArrowTapped(isHidden: isReportsHidden, section: section)
    }
    
    func configure(section:Int,report:MyHealthReportMdl){
        if let r = report.reports{
            if r.count > 0{
                btnArrow.isHidden = false
            }else{
                  btnArrow.isHidden = true
            }
        }else{
            btnArrow.isHidden = true
        }
        
        self.lblReportType.text = report.reportType
        self.section = section
        self.isReportsHidden = report.isSubViewHidden
        if isReportsHidden{
            btnArrow.setImage(#imageLiteral(resourceName: "dropdownarrow"), for: .normal)
        }else{
              btnArrow.setImage(#imageLiteral(resourceName: "uparrow"), for: .normal)
        }
    }
}
