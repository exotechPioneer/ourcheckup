//
//  CustomXAxisValueFormatter.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 28/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation

class CustomXAxisValueFormatter : NSObject,IAxisValueFormatter{
   
    var values : [String]!
   
    init(values:[String]) {
        self.values = values
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value)
        if index < values.count{
            return values[index]
        }else{
            return ""
        }
        
    }
}
