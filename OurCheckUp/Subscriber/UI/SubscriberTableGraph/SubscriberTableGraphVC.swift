//
//  SubscriberTableGraphVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 02/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit
import SwiftChart

struct SubscriberTableGraphTableDataModal {
    var header:String
    var values : [OC_Value]
}

class SubscriberTableGraphVC: BaseVC {
    
    public enum DeviceDataReportType{
        case bodyWeight
        case heartRate
        case bloodPressure
        case sleepPattern
        case activity
        case spo2
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var btnList:UIButtonX!
    @IBOutlet weak var btnChart:UIButtonX!
    @IBOutlet weak var btnLeftArrow:UIButtonX!
    @IBOutlet weak var btnRightArrow:UIButtonX!
    @IBOutlet weak var headerLabelsView : UIViewX!
    @IBOutlet weak var lblheaderTime:UILabelX!
    @IBOutlet weak var lblheaderResult:UILabelX!
    @IBOutlet weak var lblheaderValue:UILabelX!
    @IBOutlet weak var lblDisplayToggle:UILabelX!
    @IBOutlet weak var lblReportName:UILabelX!
    @IBOutlet weak var lblDate:UILabelX!
    @IBOutlet weak var viewReportTypeBtns: D_W_M_Y_View!
    let CELL_ID = "cellid"
    var isListViewShow = true
    var tintColor: UIColor?
    private var reportId : String!
    var reportType: DeviceDataReportType!
    var reportTimeType : D_W_M_Y_View.SelectionType!
    var reportName:String!
    var deviceDataHelper :DeviceDataReportHelper!
    var dataArray : [BPGetDataMdl.Data]?
    var selectedDate : Date!
    var startDate : Date!
    var endDate : Date!
    var originalDataDictionary : [String:[OC_Value]]!
    var pageNumber = 1
    var isFetching = false
    var isAllPageLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        self.deviceDataHelper = DeviceDataReportHelper()
        setColorTheme()
        setUpNavLogo()
        
        self.reportTimeType = .daily
        viewReportTypeBtns.selectionType = reportTimeType
        viewReportTypeBtns.setSelection()
        self.lblReportName.text = reportName
        setCurrentDate()
        self.loadData(timeType: reportTimeType)
        self.viewReportTypeBtns.action = {[unowned self] (type) in
            self.reportTimeType = type
            self.setCurrentDate()
            self.resetPaging()
            self.loadData(timeType: type)
        }
        selectListView()
    }
    
    //MARK:- Button Actions
    @IBAction func btnChartAction(_ sender:UIButton){
        selectChartView()
    }
    @IBAction func btnListAction(_ sender:UIButton){
        selectListView()
    }
    @IBAction func btnLeftArrowAction(_ sender:UIButton){
        prevoiusDate()
    }
    @IBAction func btnRightArrowAction(_ sender:UIButton){
        forwardDate()
    }
    
    func selectListView(){
        self.btnList.setImage(#imageLiteral(resourceName: "radio_on_button"), for: .normal)
        self.btnChart.setImage(#imageLiteral(resourceName: "radio_off_button"), for: .normal)
        self.tableView.isHidden = false
        self.lineChartView.isHidden = true
        self.headerLabelsView.isHidden = false
    }
    func selectChartView(){
        self.btnList.setImage(#imageLiteral(resourceName: "radio_off_button"), for: .normal)
        self.btnChart.setImage(#imageLiteral(resourceName: "radio_on_button"), for: .normal)
        self.tableView.isHidden = true
        self.lineChartView.isHidden = false
        self.headerLabelsView.isHidden = true
    }
    
    //MARK:- setup top view theme
    func setColorTheme(){
        if let color = tintColor{
            btnList.tintColor = color
            btnChart.tintColor = color
            btnList.setTitleColor(color, for: .normal)
            btnChart.setTitleColor(color, for: .normal)
            lblDisplayToggle.textColor = color
            lblReportName.textColor = color
            
            headerLabelsView.backgroundColor = color
            btnLeftArrow.tintColor = color
            btnRightArrow.tintColor = color
            lblDate.textColor = color
            
            if reportType == .spo2 || reportType == .activity{
                viewReportTypeBtns.selectedColor = color
                viewReportTypeBtns.normalColor = .white
                viewReportTypeBtns.normalTextColor = color
                viewReportTypeBtns.selectedTextColor = .white
                lblheaderResult.textColor =  .white
                lblheaderTime.textColor = .white
                lblheaderValue.textColor = .white
            }else{
                viewReportTypeBtns.selectedColor = color
                viewReportTypeBtns.normalColor = .white
                viewReportTypeBtns.normalTextColor = color
                viewReportTypeBtns.selectedTextColor = .black
                
            }
        }
    }
    
    //MARK:- Setup chart
    func updateGraph(values:[BPGetDataMdl.Data]){
        
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.leftAxis.drawGridLinesEnabled = false
        lineChartView.rightAxis.enabled = false
        lineChartView.legend.form = .line
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.chartDescription?.text = ""
        
        let data = LineChartData()
        var axisLabelValues = [String]()
        
        var lineChartEntry  = [ChartDataEntry]()
        
        for val in values{
            switch reportTimeType! {
            case .daily:
                if let d = val.date?.date(withFormat: "yyyy-MM-dd hh:mm:ss"){
                    axisLabelValues.append(d.dateString(withFormat: "hh:mm"))
                }
                break
            case .weekly:
                if let d = val.day?.date(withFormat: "dd-MM-yyyy"){
                    axisLabelValues.append(d.dateString(withFormat: "EEE dd"))
                }
                break
            case .monthly:
                if let d = val.week{
                    axisLabelValues.append(d)
                }
                break
            case .yearly:
                if let d = val.month?.date(withFormat: "M-yyyy"){
                    axisLabelValues.append(d.dateString(withFormat: "MMM yy"))
                }
                break
                
            }
        }
        
        
        
        switch reportType! {
        case .activity:
            self.lblheaderValue.text = "Distance Covered"
            self.lblheaderTime.text = "Total Steps"
            self.lblheaderResult.text = "Calories Burnt"
            var lineChartEntry1  = [ChartDataEntry]()//Calories
            var lineChartEntry2  = [ChartDataEntry]()//Steps
            var lineChartEntry3  = [ChartDataEntry]()//Distance
            
            for i in 0..<values.count {
                if let calories = Float(values[i].calories_burt ?? "0"),
                    let steps = Float(values[i].steps)
                {
                    let distance = steps * 0.415
                    let entry3 = ChartDataEntry(x: Double(i), y: Double(distance)) // here we set the X and Y status in a data chart entry
                    lineChartEntry3.append(entry3)
                    
                    let entry2 = ChartDataEntry(x: Double(i), y: Double(steps)) // here we set the X and Y status in a data chart entry
                    lineChartEntry2.append(entry2)
                    
                    let entry1 = ChartDataEntry(x: Double(i), y: Double(calories)) // here we set the X and Y status in a data chart entry
                    lineChartEntry1.append(entry1)
                }
            }
            
            let line1 = LineChartDataSet(values: lineChartEntry1, label: "Calories") //Here we convert lineChartEntry to a LineChartDataSet
            line1.circleColors = line1.colors//Sets the colour to blue
            line1.drawIconsEnabled = false
            line1.lineWidth = 1
            line1.circleRadius = 3
            line1.fillColor = UIColor(rgba:"#FBF550")
            line1.drawFilledEnabled = true
            line1.fillAlpha = 1.0
            line1.colors = [line1.fillColor]
            line1.circleColors = line1.colors
            
            
            let line2 = LineChartDataSet(values: lineChartEntry2, label: "Steps") //Here we convert lineChartEntry to a LineChartDataSet
            line2.colors = [UIColor(rgba:"#345186")]
            line2.circleColors = line2.colors//Sets the colour to blue
            line2.drawIconsEnabled = false
            line2.lineWidth = 1
            line2.circleRadius = 3
            line2.fillColor = UIColor(rgba:"#345186")
            line2.drawFilledEnabled = true
            line2.fillAlpha = 1.0
            
            let line3 = LineChartDataSet(values: lineChartEntry3, label: "Distance") //Here we convert lineChartEntry to a LineChartDataSet
            line3.colors = [UIColor(rgba:"#DDAB3D")]
            line3.circleColors = line3.colors//Sets the colour to blue
            line3.drawIconsEnabled = false
            line3.lineWidth = 1
            line3.circleRadius = 3
            line3.fillColor = UIColor(rgba:"#DDAB3D")
            line3.drawFilledEnabled = true
            line3.fillAlpha = 1.0
            
            //This is the object that will be added to the chart
            data.addDataSet(line1)
            data.addDataSet(line2)
            data.addDataSet(line3)
            break
        case .bloodPressure:
            var lineChartEntry1  = [ChartDataEntry]()
            var lineChartEntry2  = [ChartDataEntry]()
            
            for i in 0..<values.count {
                if let value2 = values[i].sys,let value1 = values[i].dia{
                    let entry2 = ChartDataEntry(x: Double(i), y: Double(value2) ?? 0) // here we set the X and Y status in a data chart entry
                    lineChartEntry2.append(entry2)
                    
                    let entry1 = ChartDataEntry(x: Double(i), y: Double(value1) ?? 0) // here we set the X and Y status in a data chart entry
                    lineChartEntry1.append(entry1)
                    
                }
            }
            
            let line1 = LineChartDataSet(values: lineChartEntry1, label: "Dia") //Here we convert lineChartEntry to a LineChartDataSet
            line1.drawIconsEnabled = false
            line1.lineWidth = 1
            line1.circleRadius = 3
            line1.drawFilledEnabled = true
            line1.fillAlpha = 1.0
            line1.colors = [tintColor!]
            line1.circleColors = line1.colors
            line1.fillColor = tintColor!
            
            
            let line2 = LineChartDataSet(values: lineChartEntry2, label: "Sys") //Here we convert lineChartEntry to a LineChartDataSet
            line2.colors = [UIColor(rgba:"#F1833B")]
            line2.circleColors = line2.colors//Sets the colour to blue
            line2.drawIconsEnabled = false
            line2.lineWidth = 1
            line2.circleRadius = 3
            line2.fillColor = UIColor(rgba:"#F1833B")
            line2.drawFilledEnabled = true
            line2.fillAlpha = 1.0
            
            //This is the object that will be added to the chart
            data.addDataSet(line2)
            data.addDataSet(line1)
            
            break
        case .bodyWeight:
            self.lblheaderResult.text = ""
            for i in 0..<values.count {
                if let value = values[i].body_weight{
                    let entry = ChartDataEntry(x: Double(i), y: Double(value) ?? 0) // here we set the X and Y status in a data chart entry
                    lineChartEntry.append(entry)
                }
            }
            
            let line1 = LineChartDataSet(values: lineChartEntry, label: "") //Here we convert lineChartEntry to a LineChartDataSet
            line1.colors = [NSUIColor.blue] //Sets the colour to blue
            line1.drawIconsEnabled = false
            line1.lineWidth = 1
            line1.circleRadius = 3
            line1.drawFilledEnabled = true
            line1.fillAlpha = 1.0
            line1.colors = [tintColor!]
            line1.circleColors = line1.colors
            line1.fillColor = tintColor!//UIColor(rgba:"")
            //This is the object that will be added to the chart
            data.addDataSet(line1) //Adds the line to the dataSet
            break
        case .heartRate:
            for i in 0..<values.count {
                if let value = values[i].heart_rate{
                    let entry = ChartDataEntry(x: Double(i), y: Double(value) ?? 0) // here we set the X and Y status in a data chart entry
                    lineChartEntry.append(entry)
                }
            }
            
            let line1 = LineChartDataSet(values: lineChartEntry, label: "") //Here we convert lineChartEntry to a LineChartDataSet
            line1.colors = [NSUIColor.blue] //Sets the colour to blue
            line1.drawIconsEnabled = false
            line1.lineWidth = 1
            line1.circleRadius = 3
            line1.drawFilledEnabled = true
            line1.fillAlpha = 1.0
            line1.colors = [tintColor!]
            line1.circleColors = line1.colors
            line1.fillColor = tintColor!//UIColor(rgba:"")
            //This is the object that will be added to the chart
            data.addDataSet(line1) //Adds the line to the dataSet
            break
        case .sleepPattern:
            self.lblheaderValue.text = "Time"
            self.lblheaderTime.text = "Date"
            self.lblheaderResult.text = ""
            for i in 0..<values.count {
                let value = values[i].sleep_minutes ?? "0"
                let entry = ChartDataEntry(x: Double(i), y: Double(value) ?? 0) // here we set the X and Y status in a data chart entry
                lineChartEntry.append(entry)
                
            }
            
            let line1 = LineChartDataSet(values: lineChartEntry, label: "") //Here we convert lineChartEntry to a LineChartDataSet
            line1.colors = [NSUIColor.blue] //Sets the colour to blue
            line1.drawIconsEnabled = false
            line1.lineWidth = 1
            line1.circleRadius = 3
            line1.drawFilledEnabled = true
            line1.fillAlpha = 1.0
            line1.colors = [tintColor!]
            line1.circleColors = line1.colors
            line1.fillColor = tintColor!//UIColor(rgba:"")
            //This is the object that will be added to the chart
            data.addDataSet(line1) //Adds the line to the dataSet
            break
        case .spo2:
            self.lblheaderValue.text = "%"
            self.lblheaderTime.text = "Time"
            self.lblheaderResult.text = ""
            for i in 0..<values.count {
                if let value = values[i].spo2{
                    let entry = ChartDataEntry(x: Double(i), y: Double(value) ?? 0) // here we set the X and Y status in a data chart entry
                    
                    lineChartEntry.append(entry)
                }
            }
            
            let line1 = LineChartDataSet(values: lineChartEntry, label: "") //Here we convert lineChartEntry to a LineChartDataSet
            line1.colors = [NSUIColor.blue] //Sets the colour to blue
            line1.drawIconsEnabled = false
            line1.lineWidth = 1
            line1.circleRadius = 3
            line1.drawFilledEnabled = true
            line1.fillAlpha = 1.0
            line1.colors = [tintColor!]
            line1.circleColors = line1.colors
            line1.fillColor = tintColor!//UIColor(rgba:"")
            //This is the object that will be added to the chart
            data.addDataSet(line1) //Adds the line to the dataSet
            break
        }
        
        lineChartView.xAxis.labelFont = UIFont.systemFont(ofSize: 9)
        lineChartView.xAxis.labelRotationAngle = -70
        lineChartView.xAxis.valueFormatter = CustomXAxisValueFormatter(values: axisLabelValues)
        
        lineChartView.data = data
        
        if data.entryCount == 0 {
            lineChartView.data = nil
        }//finally - it adds the chart data to the chart and causes an update
        
    }
    
    //MARK:- Setup tableview
    func setUpTable(){
        self.tableView.register("SubscriberTableGraphCell".toNib(), forCellReuseIdentifier: CELL_ID)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
    }
    
    //MARK:- Load data from api
    func loadData(timeType:D_W_M_Y_View.SelectionType){
        
        let (type,deviceId,params) = self.deviceDataHelper.getReportId(type: self.reportType, timeType: timeType)
        
        let startDate = self.startDate.dateString(withFormat: "yyyy-MM-dd")
        let endDate = self.endDate.dateString(withFormat: "yyyy-MM-dd")
        
        self.isFetching = true
        self.showProgress()
        self.deviceDataHelper.getBPDataFromApi(device_id: deviceId, filterType: type, fields:params, startDate: startDate, endDate: endDate,pageNum: self.pageNumber) { (data, status) in
            self.hideProgress()
            if status{
                if let data = data?.data{
                    if data.count == 0{
                        self.isAllPageLoaded = true
                    }
                    if self.dataArray == nil{
                        self.dataArray = data
                    }else{
                        self.dataArray?.append(contentsOf: data)
                    }
                    self.tableView.reloadData()
                    self.updateGraph(values: self.dataArray!)
                    self.isFetching = false
                }
            }
        }
        
        
        //self.showProgress()
        /*self.deviceDataHelper.getReportData(reportId: self.reportId,timeType: timeType, completion: { (result) in
         // self.hideProgress()
         self.originalDataDictionary = result
         self.loadDataWithDate()
         })*/
    }
    
    func loadDataWithDate(){
        self.dataArray?.removeAll()
        updateGraph(values: [])
        self.tableView.reloadData()
        
        /*if originalDataDictionary != nil{
         
         var key = "'"
         switch reportTimeType! {
         case .daily:
         key = selectedDate.getDateStringWithFormate("MM/d/YYYY", timezone: TimeZone.current.abbreviation()!)
         case .weekly:
         key = "\(selectedDate.weekNumber())"
         
         case .monthly:
         key = selectedDate.getDateStringWithFormate("MM/YYYY", timezone: TimeZone.current.abbreviation()!)
         case .yearly:
         key = "\(selectedDate.year())"
         }
         
         if let values = originalDataDictionary[key]
         {
         self.dataArray = [SubscriberTableGraphTableDataModal(header: key,
         values:values )]
         DispatchQueue.main.async {
         self.tableView.reloadData()
         self.updateGraph(values: values)
         }
         
         }
         }*/
    }
    
    func setCurrentDate(){
        selectedDate = Date()
        setAccordingToReportTime()
    }
    
    func prevoiusDate(){
        switch reportTimeType! {
        case .daily:
            selectedDate = selectedDate.addDays(-1)
        case .weekly:
            selectedDate = selectedDate.addWeeks(-1)
        case .monthly:
            selectedDate = selectedDate.addMonths(-1)
        case .yearly:
            selectedDate = selectedDate.addYears(-1)
        }
        resetPaging()
        setAccordingToReportTime()
        loadData(timeType: reportTimeType)
    }
    
    func forwardDate(){
        switch reportTimeType! {
        case .daily:
            selectedDate = selectedDate.addDays(1)
        case .weekly:
            selectedDate = selectedDate.addWeeks(1)
        case .monthly:
            selectedDate = selectedDate.addMonths(1)
        case .yearly:
            selectedDate = selectedDate.addYears(1)
        }
        resetPaging()
        setAccordingToReportTime()
        loadData(timeType: reportTimeType)
    }
    
    func setAccordingToReportTime(){
        switch reportTimeType! {
        case .daily:
            self.startDate = selectedDate
            self.endDate = selectedDate
            self.lblDate.text = getFomattedDateString(date: selectedDate)
        case .weekly:
            let week = dayRangeOf(weekOfYear: selectedDate.weekNumber(), for: selectedDate)
            let startStr = getFomattedDateString(date: week.start)
            let endStr = getFomattedDateString(date: week.end)
            self.lblDate.text = startStr+" - "+endStr
            self.startDate = week.start
            self.endDate = week.end
        case .monthly:
            self.startDate = selectedDate.startOfMonth()
            self.endDate = selectedDate.endOfMonth()
            
            self.lblDate.text = selectedDate.getMonthName()+", \(selectedDate.year())"
        case .yearly:
            self.startDate = selectedDate.startOfYear()
            self.endDate = selectedDate.endOfYear()
            self.lblDate.text = "\(selectedDate.year())"
        }
        
        //loadDataWithDate()
        //loadData(timeType: reportTimeType)
    }
    
    func getFomattedDateString(date:Date)->String{
        return date.getDateStringWithFormate("dd/MM/YYYY", timezone: TimeZone.current.abbreviation()!)
    }
    
    func dayRangeOf(weekOfYear: Int, for date: Date) -> (start:Date,end:Date)
    {
        let calendar = Calendar.current
        let year = calendar.component(.yearForWeekOfYear, from: date)
        let startComponents = DateComponents(weekOfYear: weekOfYear, yearForWeekOfYear: year)
        let startDate = calendar.date(from: startComponents)!
        let endComponents = DateComponents(day:7, second: -1)
        let endDate = calendar.date(byAdding: endComponents, to: startDate)!
        return (startDate,endDate)
    }
    
    func resetPaging(){
        self.isAllPageLoaded = false
        self.pageNumber = 1
        self.dataArray = nil
        self.tableView.reloadData()
    }
}

extension SubscriberTableGraphVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        //if let array = self.dataArray{
        //return array.count
        //}
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = self.dataArray{
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! SubscriberTableGraphCell
        cell.setDataHelper(helper: deviceDataHelper)
        cell.setData(valueData: self.dataArray![indexPath.row], reportTime:reportTimeType,reportType:reportType)
        
        //data count >= 100 because api returns max 100 records per request
        // if records are < 100 then no need to call the api
        if indexPath.row > (dataArray!.count - 3) && !isFetching
            && !isAllPageLoaded && dataArray!.count >= 100{
            pageNumber += 1
            self.loadData(timeType: reportTimeType)
        }
        return cell
    }
}
