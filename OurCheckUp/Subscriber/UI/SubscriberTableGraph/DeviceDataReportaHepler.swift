//
//  DeviceDataReportaHepler.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 13/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation

class DeviceDataReportHelper{
    
    typealias ReportResult = ([String:[OC_Value]])->Void
 
    var reportTimeType: D_W_M_Y_View.SelectionType!
    
    var dailyReportOriginalData : DeviceDataReport?
    var weeklyReportOriginalData : DeviceDataReport?
    var monthlyReportOriginalData : DeviceDataReport?
    var yearlyReportOriginalData : DeviceDataReport?
    
    var dataDictionary : [String:[OC_Value]] = [:]
    var reportResult : ReportResult?
    
    func getReportData(reportId:String,
                       timeType:D_W_M_Y_View.SelectionType,
                       completion:ReportResult?){
        switch timeType{
        case .daily:
            getDailyReport(reportId: reportId)
        case .weekly:
            getWeeklyReport(reportId: reportId)
        case .monthly:
            getMonthlyReport(reportId: reportId)
        case .yearly:
            getYearlyReport(reportId: reportId)
        }
        self.reportResult = completion
    }
    
    func getDailyReport(reportId: String){
        self.reportTimeType = .daily
        
        if let model = dailyReportOriginalData{
            self.processData(modal: model)
        }else{
            Api.share.getDeviceReport(reportID: reportId) { (report) in
                self.dailyReportOriginalData = report
                if let rept = report{
                    self.processData(modal: rept)
                }
            }
        }
        
        
    }
    
  
    func getWeeklyReport(reportId: String){
        self.reportTimeType = .weekly
        if let model = weeklyReportOriginalData{
            self.processData(modal: model)
        }else{
            Api.share.getDeviceReport(reportID: reportId) { (report) in
                self.weeklyReportOriginalData = report
                if let rept = report{
                    self.processData(modal: rept)
                }
            }
        }
       
    }
    
    func getMonthlyReport(reportId: String){
        self.reportTimeType = .monthly
        if let model = monthlyReportOriginalData{
            self.processData(modal: model)
        }else{
            Api.share.getDeviceReport(reportID: reportId) { (report) in
                self.monthlyReportOriginalData = report
                if let rept = report{
                    self.processData(modal: rept)
                }
            }
        }
        
       
    }
    
    func getYearlyReport(reportId: String){
        self.reportTimeType = .yearly
        
        if let model = yearlyReportOriginalData{
            self.processData(modal: model)
        }else{
            Api.share.getDeviceReport(reportID: reportId) { (report) in
                self.yearlyReportOriginalData = report
                if let rept = report{
                    self.processData(modal: rept)
                }
            }
        }
    }
    
    func processData(modal : DeviceDataReport){
        if let elements = modal.content?.elements{
            if elements.count > 1{
                if let values = elements[0]?.values{
                    let values2 = elements[1]!.values!
                    var values3 = [OC_Value]()
                    if elements.count == 3{
                        values3 = elements[2]!.values! as! [OC_Value]
                    }
                    self.dataDictionary.removeAll()
                    for (index,value) in values.enumerated(){
                        var val = value
                        val?.value2 = values2[index]?.value
                        
                        if values3.count > 0{
                            val?.value3 = values3[index].value
                        }
                        
                        var dictKey = ""
                        switch reportTimeType! {
                        case .daily,.monthly,.yearly:
                            let dateTime = extractDailyDateTime(dateTimeString: (val?.tip!)!)
                            dictKey = dateTime.date
                        
                        case .weekly:
                            let weekYear = extractWeekDateTime(dateTimeString: (value?.tip!)!)
                            dictKey = weekYear.weekNum
                            
                        }
                        
                        
                        guard var oldValues = self.dataDictionary[dictKey]
                            else{
                                self.dataDictionary[dictKey] = [val!]
                                continue
                        }
                        oldValues.append(val!)
                        self.dataDictionary[dictKey] = oldValues
                    }
                }
            }else{
                if let values = elements[0]?.values{
                    self.dataDictionary.removeAll()
                    for val in values{
                        var dictKey = ""
                        switch reportTimeType! {
                        case .daily,.monthly,.yearly:
                            let dateTime = extractDailyDateTime(dateTimeString: (val?.tip!)!)
                            dictKey = dateTime.date
                            
                        case .weekly:
                            let weekYear = extractWeekDateTime(dateTimeString: (val?.tip!)!)
                            dictKey = weekYear.weekNum
                            
                        }
                        
                        guard var oldValues = self.dataDictionary[dictKey]
                            else{
                                self.dataDictionary[dictKey] = [val!]
                                continue
                        }
                        oldValues.append(val!)
                        self.dataDictionary[dictKey] = oldValues
                    }
                }
            }
        }
        
        self.reportResult?(self.dataDictionary)
    }
    
     func extractDailyDateTime(dateTimeString:String)->(date:String,time:String){
        //9/1/2018 06:22 (Sys)\n135
        let septVals = dateTimeString.components(separatedBy: " ")
        if septVals.count > 2{
            let dateStr = septVals[0]
            let time = septVals[1]
            return (dateStr,time)
        }else if septVals.count == 1{
            let dateStr = septVals[0]
            return (dateStr,"00:00")
        }else{
            return ("--","00:00")
        }
    }
    
    func extractWeekDateTime(dateTimeString:String)->(weekNum:String,year:String,time:String){
        //9/1/2018 06:22 (Sys)\n135
        let septVals = dateTimeString.components(separatedBy: " ")
        if septVals.count > 3{
            let weekNum = septVals[0]
            let year = septVals[2]
            let time = septVals[3]
            return (weekNum,year,time)
        }else if septVals.count == 1{
            let weekNum = septVals[0]
            return (weekNum,"0000","00:00")
        }else{
            return ("00","0000","00:00")
        }
    }
    
    func getReportId (type:SubscriberTableGraphVC.DeviceDataReportType,
                      timeType:D_W_M_Y_View.SelectionType)->(String,String,[Any]){
        
        var reportTimeType = ""
        switch timeType{
        case .daily:
            reportTimeType = "day"
        //return api.REPORTIDBPDAILY
        case .weekly:
            reportTimeType = "week"
        //return api.REPORTIDBPWEEKLY
        case .monthly:
            reportTimeType = "month"
        //return api.REPORTIDBPMONTHLY
        case .yearly:
            reportTimeType = "year"
            //return api.REPORTIDBPYEARLY
        }
        
        //let api = Api.share
        switch type {
        case .activity:
            /*
            switch timeType{
            case .daily:
                return api.reportIdSTDaily
            case .weekly:
                return api.reportIdSTWeekly
            case .monthly:
                return api.reportIdSTMonthly
            case .yearly:
                return api.reportIdSTYearly
            }*/
            return (reportTimeType,DevicesConnectionHelper.SMART_BAND_DEVICE_ID,["calories_burt","steps"])
            
        case .bloodPressure:
          return (reportTimeType,DevicesConnectionHelper.ICHOICE_DEVICE_ID,["sys","dia","pul"])
        case .bodyWeight:
           /* switch timeType{
            case .daily:
                return api.reportIdBodyWeightDaily
            case .weekly:
                return api.reportIdBodyWeightWeekly
            case .monthly:
                return api.reportIdBodyWeightMonthly
            case .yearly:
                return api.reportIdBodyWeightYearly
            }*/
             return (reportTimeType,DevicesConnectionHelper.SMART_BODY_ANAYLZER_DEVICE_ID,["body_weight"])
            
        case .heartRate:
            /*switch timeType{
            case .daily:
                return api.reportIdHRDaily
            case .weekly:
                return api.reportIdHRWeekly
            case .monthly:
                return api.reportIdHRMonthly
            case .yearly:
                return api.reportIdHRYearly
            }*/
             return (reportTimeType,DevicesConnectionHelper.SMART_BAND_DEVICE_ID,["heart_rate"])
            
        case .sleepPattern:
            /*switch timeType{
            case .daily:
                return api.reportIdSleepDaily
            case .weekly:
                return api.reportIdSleepWeekly
            case .monthly:
                return api.reportIdSleepMonthly
            case .yearly:
                return api.reportIdSleepYearly
            }*/
            return (reportTimeType,DevicesConnectionHelper.SMART_BAND_DEVICE_ID,["sleep_minutes","sleep_hours"])
            
        case .spo2:
            /*switch timeType{
            case .daily:
                return api.reportIdSPODaily
            case .weekly:
                return api.reportIdSPOWeekly
            case .monthly:
                return api.reportIdSPOMonthly
            case .yearly:
                return api.reportIdSPOYearly
            }*/
            
            return (reportTimeType,DevicesConnectionHelper.SMART_BAND_DEVICE_ID,["spo2"])
        }
       
    }
    
    /*func getDeviceDataFromApi(filterType:String,fields:[Any]){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        var params = [String:Any]()
        params["user_id"] = userID
        params["filter_type"] = filterType
        params["columns"] =  fields
        //{ "user_id" : 4,"filter_type" : "day","columns" : ["sys","dia","pull","date"]}
        Api.share.getDeviceReportData2(params: params) { (data, status) in
            if status{
                print(data ?? "nil model")
            }else{
                print("Error in getting device health data")
            }
        }
    }*/
    
    func getBPDataFromApi(device_id:String,filterType:String,fields:[Any],startDate:String,endDate:String,pageNum:Int,completion:@escaping (BPGetDataMdl?,Bool)->Void){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        var params = [String:Any]()
        params["user_id"] = userID
        params["device_id"] = device_id
        params["columns"] =  fields
        params["filter_type"] = filterType
        params["start_date"] =  startDate
        params["end_date"] =  endDate
        params["columns"] =  fields
        params["page"] = pageNum
        
        Api.share.getBPData(params:params) { (bpData, status) in
              completion(bpData,status)
        }
    }
}

