//
//  SubscriberTableGraphCell.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 13/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit
class SubscriberTableGraphCell: UITableViewCell {
    
    @IBOutlet weak var lblTime:UILabelX!
    @IBOutlet weak var lblResult:UILabelX!
    @IBOutlet weak var lblValue:UILabelX!
    var helper:DeviceDataReportHelper!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDataHelper(helper:DeviceDataReportHelper){
        self.helper = helper
    }
    
    func setData(valueData:BPGetDataMdl.Data,reportTime:D_W_M_Y_View.SelectionType,reportType:SubscriberTableGraphVC.DeviceDataReportType){
        
        var timeTxt = " "
        
        switch reportTime {
        case .daily:
            if let dateStr = valueData.date{
                timeTxt = dateStr.components(separatedBy: " ")[1]
            }
            break
        case .monthly:
            timeTxt = valueData.week ?? ""
            break
        case .yearly:
            timeTxt = valueData.month ?? ""
            break
        case .weekly:
            timeTxt = valueData.date ?? ""
            break
        }
        self.lblTime.text = timeTxt
       
        switch reportType {
        case .activity:
            self.lblTime.text = valueData.steps
            let distStr = String(format: "%.02f", Double(valueData.steps ?? "0")! * ((164 * 0.415)/100000))
            let calsStr = String(format: "%.02f", Double(valueData.calories_burt ?? "0")!)
            self.lblValue.text = distStr+" km"
            self.lblResult.text = "\(calsStr) kcal"
            break
        case .bloodPressure:
            let bp = Int(valueData.sys ?? "0")!
            self.lblValue.text = "\(valueData.sys ?? "0")/\(valueData.dia ?? "0") mmHg"
            if bp >= 120 && bp <= 140{
                self.lblResult.text = "Normal"
                self.contentView.backgroundColor = .white
            }else{
                self.lblResult.text = "Abnormal"
                self.contentView.backgroundColor = .red
            }
            break
        case .bodyWeight:
            let weightStr = String(format: "%.02f", Double(valueData.body_weight ?? "0")!)
            self.lblValue.text = weightStr+" kg"
            self.lblResult.text = ""
            break
        case .heartRate:
            let hr = Int(valueData.heart_rate ?? "0")!
            self.lblValue.text = "\(hr) bpm"
            if hr >= 60 && hr <= 90{
                self.lblResult.text = "Normal"
                self.contentView.backgroundColor = .white
            }else{
                self.lblResult.text = "Abnormal"
                self.contentView.backgroundColor = .red
            }
            break
        case .sleepPattern:
            let sleepM = Int(valueData.sleep_minutes ?? "0")!
            self.lblValue.text = "\(0) Hours\(0) Mins"
            if sleepM > 0{
                let (h,m,_) = secondsToHoursMinutesSeconds(seconds: sleepM/1000)
                self.lblValue.text = "\(h) Hours\(m) Mins"
            }
            self.lblResult.text = ""
            break
        case .spo2:
            self.lblValue.text = "\(valueData.spo2 ?? "0")"
            self.lblResult.text = ""
            break
        }
        
        /*switch reportTime {
         case .daily,.monthly,.yearly:
         timeTxt = helper.extractDailyDateTime(dateTimeString: valueData.tip!).time
         case .weekly:
         timeTxt = helper.extractWeekDateTime(dateTimeString: valueData.tip!).time
         }
         self.lblTime.text = timeTxt
         switch reportType {
         case .activity:
         self.lblTime.text = "\(Int(valueData.value2 ?? 0))"
         self.lblValue.text = "\(Float(valueData.value3 ?? 0)) km"
         self.lblResult.text = "\(Int(valueData.value ?? 0)) kcal"
         break
         case .bloodPressure:
         let bp = valueData.value ?? 0
         self.lblValue.text = "\(valueData.value ?? 0)/\(valueData.value2 ?? 0) mmHg"
         if bp >= 120 && bp <= 140{
         self.lblResult.text = "Normal"
         self.contentView.backgroundColor = .white
         }else{
         self.lblResult.text = "Abnormal"
         self.contentView.backgroundColor = .red
         }
         break
         case .bodyWeight:
         self.lblValue.text = "\(valueData.value ?? 0) kg"
         self.lblResult.text = ""
         break
         case .heartRate:
         let hr = valueData.value ?? 0
         
         self.lblValue.text = "\(hr) bpm"
         if hr >= 60 && hr <= 90{
         self.lblResult.text = "Normal"
         self.contentView.backgroundColor = .white
         }else{
         self.lblResult.text = "Abnormal"
         self.contentView.backgroundColor = .red
         }
         
         break
         case .sleepPattern:
         self.lblValue.text = "\(valueData.value ?? 0)"
         break
         case .spo2:
         self.lblValue.text = "\(valueData.value ?? 0)"
         break
         
         
         }*/
    }
    
   
}
