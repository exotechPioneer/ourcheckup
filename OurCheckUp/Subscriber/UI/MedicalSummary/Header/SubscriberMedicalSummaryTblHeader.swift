//
//  SubscriberMedicalSummaryTC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class SubscriberMedicalSummaryTblHeader: UITableViewHeaderFooterView {

    
    @IBOutlet weak var lblHeader: UILabel!
    
    func configure(header:String){
        lblHeader.text = header
    }
    
   
}
