//
//  SubscriberMedicalSummaryVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class SubscriberMedicalSummaryVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblCreateOn:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblMRN:UILabel!
    @IBOutlet weak var lblDOB:UILabel!
    @IBOutlet weak var lblIDnum:UILabel!
    @IBOutlet weak var lblGender:UILabel!
    @IBOutlet weak var lblAge:UILabel!
    
    var dataArray : [SubscriberMedicalSummaryMdl]?
    
    var CELL_ID = "CELLID"
    var HEADER_ID = "HeaderID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: "MEDICAL SUMMARY")
        setUpTable()
        getMedicalSummary()
    }
    
    func createData(dataAarry:[SubscriberMedicalSummaryMdl.Data]){
        
        var significantArray = [SubscriberMedicalSummaryMdl.Child]()
        var pastMedicalHistoryArray = [SubscriberMedicalSummaryMdl.Child]()
        var drugAllergiesArray = [SubscriberMedicalSummaryMdl.Child]()
        var currentMedicationArray = [SubscriberMedicalSummaryMdl.Child]()
        
        for element in dataAarry{
            if let currentMedication = element.currentMedication{
                 let arr = currentMedication.components(separatedBy: ",")
                for item in arr{
                    if !isValueContains(array: currentMedicationArray, value: item){
                      currentMedicationArray.append(SubscriberMedicalSummaryMdl.Child(name:item,time:""))
                    }
                }
            }
            
            if let drugAllergies = element.drugAllergies{
                let arr = drugAllergies.components(separatedBy: ",")
                for item in arr{
                    if !isValueContains(array: drugAllergiesArray, value: item){
                        drugAllergiesArray.append(SubscriberMedicalSummaryMdl.Child(name:item,time:""))
                    }
                }
            }
            
            if let pastSurgicalHistory = element.pastSurgicalHistory{
                let arr = pastSurgicalHistory.components(separatedBy: ",")
                for item in arr{
                    if !isValueContains(array: pastMedicalHistoryArray, value: item){
                        pastMedicalHistoryArray.append(SubscriberMedicalSummaryMdl.Child(name:item,time:""))
                    }
                }
            }
            
            if let significantMedicalHistory = element.significantMedicalHistory{
                let arr = significantMedicalHistory.components(separatedBy: ",")
                for item in arr{
                    if !isValueContains(array: significantArray, value: item){
                        significantArray.append(SubscriberMedicalSummaryMdl.Child(name:item,time:""))
                    }
                }
            }
        }
        
        let m1 = SubscriberMedicalSummaryMdl(header: "Significant Medical History", childs:significantArray)
       
        let m2 = SubscriberMedicalSummaryMdl(header: "Past Medical History", childs: pastMedicalHistoryArray)
        
        let m3 = SubscriberMedicalSummaryMdl(header: "Drug Allergies", childs: drugAllergiesArray)
        
        let m4 = SubscriberMedicalSummaryMdl(header: "Current Medication", childs: currentMedicationArray)
        
        self.dataArray = [m1,m2,m3,m4]
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func setUpTable(){
        self.tableView.register("SubscriberMedicalSummaryTC".toNib(), forCellReuseIdentifier: CELL_ID)
        self.tableView.register("SubscriberMedicalSummaryTblHeader".toNib(), forHeaderFooterViewReuseIdentifier: HEADER_ID)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
    }
    
    func getMedicalSummary(){
        let userID:Int = AppPreferences.share.get(forkey: .userID)!
        let params = ["user_id":userID]
        self.showProgress()
        Api.share.getMedicalSummary(params: params) { (summary, status) in
            self.hideProgress()
            if status{
                self.setSummaryData(summary: summary)
            }
        }
    }
    
    func setSummaryData(summary:SubscriberMedicalSummaryMdl?){
        if let summary = summary{
            if let data = summary.data{
                createData(dataAarry: data)
                if data.count > 0{
                    let index = data.count - 1
                    let name = data[index].name
                    let age = data[index].age
                    let mrn = data[index].mrn
                    let dob = data[index].dob
                    let gender = data[index].gender
                    let created_on = data[index].createdOn
                    let id = data[index].idNo
                    DispatchQueue.main.async {
                        self.lblName.text = name
                        self.lblName.sizeToFit()
                        if let age = age{
                            self.lblAge.text = "Age: \(age)"
                            self.lblAge.sizeToFit()
                        }else{
                            self.lblAge.text = "Age:"
                        }
                        if let mrn = mrn{
                            self.lblMRN.text = "\(mrn)"
                            self.lblMRN.sizeToFit()
                        }else{
                            self.lblMRN.text = ""
                        }
                        if let gender = gender{
                            self.lblGender.text = gender
                            self.lblGender.sizeToFit()
                        }else{
                            self.lblGender.text = ""
                        }
                        if let dob = dob{
                            self.lblDOB.text = dob.components(separatedBy: "T")[0]
                            self.lblDOB.sizeToFit()
                        }else{
                            self.lblDOB.text = ""
                        }
                        if let createOn = created_on{
                            self.lblCreateOn.text = "\(createOn)"
                            self.lblCreateOn.sizeToFit()
                        }else{
                            self.lblCreateOn.text = ""
                        }
                        if let idNo = id{
                            self.lblIDnum.text = "\(idNo)"
                            self.lblIDnum.sizeToFit()
                        }else{
                            self.lblIDnum.text = ""
                        }
                    }
                }else{
                    displayAlert("Error!", andMessage: "No medical summary available.") {
                        //self.dismiss(animated: false)
                        self.navigationController?.popViewController(animated: false)
                    }
                }
            }
        }
    }
    
    func isValueContains(array:[SubscriberMedicalSummaryMdl.Child],value:String)->Bool{
        for child in array{
            if child.name == value{
                return true
            }
        }
        return false
    }
    
    
}
//MARK:- UITableViewDataSource
extension SubscriberMedicalSummaryVC : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if let count = dataArray?.count{
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = dataArray?[section]{
            if  (data.childs != nil){
                return data.childs!.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return setCell(tableView,cellForRowAt:indexPath)
    }
    
    func setCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! SubscriberMedicalSummaryTC
        
        cell.configure(child: dataArray![indexPath.section].childs![indexPath.row])
        return cell
    }
}
//MARK:- UITableViewDelegate
extension SubscriberMedicalSummaryVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: HEADER_ID) as! SubscriberMedicalSummaryTblHeader
        view.configure(header: dataArray![section].header ?? "")
        return view
    }
}
