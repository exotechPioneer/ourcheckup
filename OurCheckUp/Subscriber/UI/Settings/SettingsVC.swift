//
//  SettingsVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 16/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    var testArray : [String]!
    var standUMOArr :[String]!
    var otherUMO: [String]!
    var conversionFactArr : [String]!
    
    @IBOutlet weak var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        testArray = createTestArray()
        standUMOArr = standardUMOArray()
        otherUMO = otherUMOArray()
        conversionFactArr = conversionFactorArray()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override var shouldAutorotate: Bool{
        return true
    }
    
    func createTestArray()->[String]{
        return ["Height",
            "Body Weight",
            "Heart Rate",
            "Blood Pressure",
            "Body Mass Index(BMI)",
            "Basal Metabolic Rate",
            "(BMR)",
            "Visceral Fat",
            "Body Water",
            "Muscles Mass",
            "Metabolic Age",
            "Average Steps taken",
            "Distance",
            "SPO2",
            "Sugar - Fasting Blood",
            "Sugar",
            "Sugar - HBA1C *",
            "HDL Cholestrol *",
            "LDL Cholestrol *",
            "Total Cholestrol *"]
    }
    func standardUMOArray()->[String]{
        return [
            "cm",
            "kg",
            "bpm",
            "mmHg",
            "kg/m2",
            "kcal",
            "%",
            "lt",
            "kg",
            "years",
            "Avg Steps/Day",
            "km",
            "%",
            "mmol/L",
            "mmol/L",
            "mmol/L",
            "mmol/L",
            "mmol/L",
            " ",
            " "]
    }
    
    func otherUMOArray()->[String]{
        return ["Ft, in",
            "lbs",
            "bpm",
            "kPa",
            "kg/m2",
            "kcal",
            "%",
            "fl oz",
            "lbs",
            "years",
            "Avg Steps/Day",
            "%",
            "miles",
            "mg/dL",
            "%",
            "mg/dL",
            "mg/dL",
            "mg/dL",
            " ",
            " "]
    }
    
    func conversionFactorArray()->[String]{
        return [
            "0.0328084 (multiply)",
            "2.20462 (multiply)",
            "N/A",
            "0.133322 (multiply)",
            "** 0.204816 (multiply)",
            "kcal",
            "(kcal)",
            "N/A",
            "33.814 (multiply)",
            "2.20462 (multiply)",
            "N/A",
            "N/A",
            "0.621371 (multiply)",
            "N/A",
            "0.0555",
            "N/A",
            "0.0259 (divide)",
            "0.0259 (divide)",
            "0.0259 (divide)",
            " ",
            " "
        ]
    }
}
extension SettingsVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell") as! ReportCell
        
        let test = testArray[indexPath.row]
        cell.lblTest.text = test
        cell.tfIdeal.text = standUMOArr[indexPath.row]
        cell.tfInitial.text = otherUMO[indexPath.row]
        cell.tfTarget.text = conversionFactArr[indexPath.row]
        
        
        return cell
    }
}
