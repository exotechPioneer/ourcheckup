//
//  SubscriberDashboardVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 01/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class SubscriberDashboardVC: BaseVC {
    
    @IBOutlet weak var viewMyHealthRrpt: UIViewX!
    @IBOutlet weak var viewMyMedicalSmry: UIViewX!
    @IBOutlet weak var viewMyHealthData: UIViewX!
    @IBOutlet weak var viewMyAppointment: UIViewX!
    @IBOutlet weak var viewMySubscriptionAccount: UIViewX!
    @IBOutlet weak var viewFeedback: UIViewX!
    @IBOutlet weak var viewPromotions: UIViewX!
    @IBOutlet weak var viewHealthInfoBit: UIViewX!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapActions()
        //addSideMenuButton()
        //setUpNavLogo()
        
        let profileUpdate = AppPreferences.share.get(forkey: .profileUpdated) ?? false
        if !profileUpdate{
            self.performSegue(id: .subscriberProfile)
        }
    }
    
    @IBAction func btnMenuAction(){
        SideDrawerVC.share?.drawerState = .opened
    }
    
    override func viewDidLayoutSubviews() {
        addSideMenuButton()
    }
    
    func addTapActions(){
        self.viewMyHealthRrpt.addTapAction { [unowned self] (view) in
            self.performSegue(id: .healthReport)
        }
        self.viewMyHealthData.addTapAction { [unowned self]  (view) in
            self.performSegue(id: .healthData)
        }
        self.viewMyMedicalSmry.addTapAction { [unowned self]  (view) in
            self.performSegue(id: .medicalSummary)
        }
    }
    
    func createTitleLogo(){
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 45))
        imageView.image = #imageLiteral(resourceName: "ourcheckup_logo")
        self.navigationController?.navigationItem.titleView = imageView
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProfileVC{
            vc.isHideBackBtn = true
        }
    }
}
