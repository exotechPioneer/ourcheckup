//
//  DevicesDataDashboard.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 02/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit
import FirebaseDatabase
class DevicesDataDashboard: BaseVC {
    
    @IBOutlet weak var viewBodyMeasurement: UIViewX!
    @IBOutlet weak var viewBloodPresure: UIViewX!
    @IBOutlet weak var viewSleepPattern: UIViewX!
    @IBOutlet weak var viewHeartRate: UIViewX!
    @IBOutlet weak var viewSPO2: UIViewX!
    @IBOutlet weak var viewActivity: UIViewX!
    @IBOutlet weak var btnConnect: UIButtonX!
    @IBOutlet weak var lblConnectedDeviceName: UILabelX!
    
    @IBOutlet weak var lblBodyWeightVal: UILabel!
    @IBOutlet weak var lblBloodPressureVal: UILabel!
    @IBOutlet weak var lblSleepPatternVal: UILabel!
    @IBOutlet weak var lblHeartRateVal: UILabel!
    @IBOutlet weak var lblSPO2Val: UILabel!
    @IBOutlet weak var lblCaloriesVal: UILabel!
    @IBOutlet weak var lblStepsVal: UILabel!
    @IBOutlet weak var lblSyncStatusLbl: UILabel!
    @IBOutlet weak var fitbitActivityIndicator: UIActivityIndicatorView!
    
    let TEXT_DISCONNECT_DEVICE = "Disconnect Device"
    
    var authenticationController: AuthenticationController?
    
    var firbaseDbRef: DatabaseReference!
    
    var isFirebaseUpdate = false
    
    var fitbitLastSyncDate:Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firbaseDbRef = Database.database().reference()
        addTapActions()
        setUpNavLogo()
        setAllDataValues()
       
        self.fitbitActivityIndicator.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onDeviceDataChanged), name: NSNotification.Name("DeviceDataChanged"), object: nil)
        
        loadLatestData()
       
    }
    @objc func onDeviceDataChanged(){
        isFirebaseUpdate = true
        setAllDataValues()
    }
    override func viewWillAppear(_ animated: Bool) {
        setConnectBtnTitle()
    }
    
    func setConnectBtnTitle(){
        if let device = DevicesConnectionHelper.share.peripheral{
            self.btnConnect.setTitle(TEXT_DISCONNECT_DEVICE, for: .normal)
            self.lblConnectedDeviceName.text = device.name
        }else{
            self.btnConnect.setTitle("Connect", for: .normal)
        }
    }
    
    func setAllDataValues(){
        let pref = AppPreferences.share
        let bodyWeight:Float = pref.get(forkey: .bodyWeight) ?? 0
        let bp : String =  pref.get(forkey: .bp) ?? "0/0"
        var sleepH = pref.get(forkey: .sleepHours) ?? 0
        var sleepM = pref.get(forkey: .sleepMins) ?? 0
        
        if sleepM > 0{
            let (h,m,_) = secondsToHoursMinutesSeconds(seconds: sleepM/1000)
            sleepM = m
            sleepH = h
        }
        
        let heartRate:Int = pref.get(forkey: .heartRate) ?? 0
        let spo2:Int = pref.get(forkey: .spo2) ?? 0
        let calories:Int  = pref.get(forkey: .calories) ?? 0
        let steps : Int = pref.get(forkey: .steps) ?? 0
        
        self.lblBodyWeightVal.text = "\(bodyWeight)"
        self.lblBloodPressureVal.text = bp
        self.lblSleepPatternVal.text = "\(sleepH):\(sleepM) hrs"
        self.lblHeartRateVal.text = "\(heartRate)"
        self.lblSPO2Val.text = "\(spo2) %"
        self.lblCaloriesVal.text = "\(calories) kcal"
        self.lblStepsVal.text = "\(steps) steps"
        self.lblSyncStatusLbl.text = ""
        
        if isFirebaseUpdate{
            isFirebaseUpdate = false
            let userID : Int = AppPreferences.share.get(forkey: .userID) ?? 0
            self.firbaseDbRef.child("OurCheckUp").child("\(userID)")
                .setValue(["calories": calories,
                           "heartRate" : heartRate,
                           "oxygen": spo2,
                           "sleepDeep": 0,
                           "sleepHours" : sleepH,
                           "sleepMinutes":sleepM,
                           "sleepShal":0,
                           "steps":steps,
                           "bodyWeight":bodyWeight,
                           "bp":bp])
            
        }
    }
    
    func addTapActions(){
        let tap :OnTap = {(view) in
            self.performSegue(id: .tableGraphDetail,sender:view)
        }
        self.viewBodyMeasurement.addTapAction(completion: tap)
        self.viewBloodPresure.addTapAction(completion: tap)
        self.viewSleepPattern.addTapAction(completion: tap)
        self.viewHeartRate.addTapAction(completion: tap)
        self.viewSPO2.addTapAction(completion: tap)
        self.viewActivity.addTapAction(completion: tap)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let id = segue.identifier{
            if id == SubscriberSegueIds.fitbitAuth.rawValue{
                authenticationController = segue.destination as? AuthenticationController
                authenticationController?.delegate = self
            }
        }
        
        if let view = sender as? UIViewX{
            let destVC = segue.destination as! SubscriberTableGraphVC
            switch view.tag{
            case 0://Bodyweight
                destVC.reportType = .bodyWeight
                destVC.reportName = "BODY MEASUREMENT"
            case 1://BP
                destVC.reportType = .bloodPressure
                destVC.reportName = "BLOOD PRESSURE"
            case 2://Sleep pattern
                destVC.reportType = .sleepPattern
                destVC.reportName = "SLEEP PATTERN"
            case 3://Heart Rate
                destVC.reportType = .heartRate
                destVC.reportName = "HEART RATE"
            case 4: //spo2
                destVC.reportType = .spo2
                destVC.reportName = "SPO2"
            case 5://activity
                destVC.reportType = .activity
                destVC.reportName = "ACTIVITY"
            default:
                break
            }
            destVC.tintColor = view.backgroundColor
        }
    }
    
    @IBAction func btnConnectAction(_ sender : UIButtonX){
        if let text = sender.titleLabel?.text {
            if text  == TEXT_DISCONNECT_DEVICE{
                DevicesConnectionHelper.share.disconnect()
            }else{
                self.performSegue(id: .devices)
            }
        }else{
            
        }
        setConnectBtnTitle()
    }
    
    @IBAction func btnFitbitAction(_ sender : UIButtonX){
        if let token:String = AppPreferences.share.get(forkey: .fitbitAuthToken),!token.isEmpty{
            fitbitLoginSuccess(token: token)
        }else{
            self.performSegue(id: .fitbitAuth)
        }
        
    }
    
    func getDataFromFirebase(){
        let userID : Int = AppPreferences.share.get(forkey: .userID) ?? 0
        
        firbaseDbRef.child("OurCheckUp").child("\(userID)").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let cals = value?["calories"] as? Int ?? 0
            let hrtRate =  value?["heartRate"] as? Int ?? 0
            let spo2 =  value?["oxygen"] as? Int ?? 0
            let sleepHours =  value?["sleepHours"] as? Int ?? 0
            let sleepMinutes =  value?["sleepMinutes"] as? Int ?? 0
            let steps =  value?["steps"] as? Int ?? 0
            let bodyWeight =  value?["bodyWeight"] as? Float ?? 0
            let bp = value?["bp"] as? String ?? "0/0"
            
            AppPreferences.share.commit(data: cals, forkey: .calories)
            AppPreferences.share.commit(data: hrtRate, forkey: .heartRate)
            AppPreferences.share.commit(data: spo2, forkey: .spo2)
            AppPreferences.share.commit(data: sleepHours, forkey: .sleepHours)
            AppPreferences.share.commit(data: sleepMinutes, forkey: .sleepMins)
            AppPreferences.share.commit(data: steps, forkey: .steps)
            AppPreferences.share.commit(data: bodyWeight, forkey: .bodyWeight)
            AppPreferences.share.commit(data: bp, forkey: .bp)
            self.setAllDataValues()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func loadLatestData(){
        /*let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        let dateStr = Date().dateString(withFormat: "yyyy-MM-dd")
        let params:[String:Any] = ["user_id" : userID,
                                   "date" : dateStr]
        self.showProgress()
        Api.share.getLatestDevicesData(params: params) { (latestData, status) in
            self.hideProgress()
            if status{
                if let result = latestData{
                    if let d = result.data{
                        if let sys = d.sys, let dia = d.dia{
                            AppPreferences.share.commit(data: sys+"/"+dia, forkey: .bp)
                        }
                        if let hrtRate = d.heartRate{
                            AppPreferences.share.commit(data: Int(hrtRate) ?? 0, forkey: .heartRate)
                        }
                        if let slpMin = d.sleepMinutes{
                            AppPreferences.share.commit(data: Int(slpMin) ?? 0, forkey: .sleepMins)
                        }
                        if let slpHrs = d.sleepHours{
                            AppPreferences.share.commit(data: Int(slpHrs) ?? 0, forkey: .sleepHours)
                        }
                        if let steps = d.steps{
                            AppPreferences.share.commit(data: Int(steps) ?? 0, forkey: .steps)
                        }
                        if let cals = d.caloriesBurt{
                            AppPreferences.share.commit(data: Int(cals) ?? 0, forkey: .calories)
                        }
                        if let bodyWeight = d.bodyWeight{
                            AppPreferences.share.commit(data: Float(bodyWeight) ?? 0, forkey: .bodyWeight)
                        }
                    }
                    DispatchQueue.main.async {
                        self.isFirebaseUpdate = true
                        self.setAllDataValues()
                    }
                }
            }else{
                
            }
        }*/
    self.getDataFromFirebase()
   
    }
    
    func getLastSyncedDataDate(){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        let params:[String:Any] = ["user_id" : userID,
                                   "device_id" : "SmartBand_03"]
        self.showProgress()
        Api.share.getLastSyncDateTime(params: params) { (date, status) in
            self.hideProgress()
            self.fitbitLastSyncDate = date
            if let lastSync = self.fitbitLastSyncDate{
                FitbitDataSync.share.lastSyncDate = lastSync
                FitbitDataSync.share.getData(date: lastSync)
            }else{
                FitbitDataSync.share.getData(date: Date()-5)
            }
            self.fitbitActivityIndicator.startAnimating()
            self.fitbitActivityIndicator.isHidden = false
        }
    }
}

extension DevicesDataDashboard : AuthenticationProtocol{
    func authorizationDidFinish(_ success: Bool) {
        print("Hello World with \(success)!")
        guard let authToken = authenticationController?.authenticationToken else {
            return
        }
        
        AppPreferences.share.commit(data: authToken, forkey: .fitbitAuthToken)
        fitbitLoginSuccess(token: authToken)
    }
    
    func fitbitLoginSuccess(token:String){
        FitbitAPI.sharedInstance.authorize(with: token)
        FitbitDataSync.share.delegate = self
        self.getLastSyncedDataDate()
       
    }
}

extension DevicesDataDashboard : FitBitDataSyncDelegate{
    func didSyncDateChange(dateStr: String) {
        DispatchQueue.main.async {
             self.lblSyncStatusLbl.text = "Fitbit Syncing for "+dateStr
        }
    }
    
    func didTodayDataUploaded() {
        setAllDataValues()
    }
    
    func syncCompleted() {
        DispatchQueue.main.async {
            self.lblSyncStatusLbl.text = "Fitbit Syncing completed"
            self.fitbitActivityIndicator.stopAnimating()
            self.fitbitActivityIndicator.isHidden = true
        }
    }
}
