//
//  DeviceListVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 09/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class DeviceListVC: BaseVC {

    var bleArray : [BleModel] = []
    @IBOutlet weak var tableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        DevicesConnectionHelper.share.delegate = self
        DevicesConnectionHelper.share.initialize()
    }

    override func viewWillAppear(_ animated: Bool) {
        DevicesConnectionHelper.share.startScan()
    }

}

extension DeviceListVC : UITableViewDelegate,UITableViewDataSource{
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "BleCell")
        let model: BleModel? = bleArray[indexPath.row]
        cell.textLabel?.text = model?.name
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
        cell.detailTextLabel?.text = model?.mac
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 15)
        return cell
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       self.showProgress()
        DevicesConnectionHelper.share.connectDevice(model: self.bleArray[indexPath.row])
    }

   
}
extension DeviceListVC : DevicesConnectionHelperDelegate{
    func deviceConnection(_ devices: [BleModel]) {
        self.bleArray = devices
        self.tableView.reloadData()
    }
    func deviceConnection(didConnect peripheral: CBPeripheral) {
        self.hideProgress()
         if let name = peripheral.name{
             displayAlert("Success", andMessage: "Connected to "+name)
         }
         self.navigationController?.popViewController(animated: false)
    }
    
    func deviceConnection(didDisconnected peripheral: CBPeripheral) {
        
    }
    
}


