//
//  DevicesConnectionHelper.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 14/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation

protocol DevicesConnectionHelperDelegate {
    func deviceConnection(_ devices : [BleModel])
    func deviceConnection(didConnect peripheral :CBPeripheral)
    func deviceConnection(didDisconnected peripheral : CBPeripheral)
}

class DevicesConnectionHelper: NSObject{
    
    enum DeviceName : String{
        case M2S = "M2S"
        case iChoice = "iChoice"
        case HealthScale = "Health Scale"
    }
    
    public static let ICHOICE_DEVICE_ID = "iChoiceDevice_BPM_02"
    public static let SMART_BODY_ANAYLZER_DEVICE_ID = "SmartBodyanalyzer_01"
    public static let SMART_BAND_DEVICE_ID = "SmartBand_03"
    
    public static let share = DevicesConnectionHelper()
    
    var peripheral : CBPeripheral?
    
    var delegate : DevicesConnectionHelperDelegate?
    
    var smartBandDataArr : [AllDataModel] = []
    
    var isSmartBandDataSyncing = false
    
    var smartBandDataIndex = 0
    
    func post(){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 14
        let dateStr = Date().getDateStringWithFormate("yyyy-MM-dd hh:mm:ss", timezone: TimeZone.current.abbreviation()!)
        let data = iChoiceDataPostModel.Data(date: dateStr, dia: 80, pul: 90, sys: 120, unitType: 0, useId: Int(userID))
        let model = iChoiceDataPostModel(data: data, deviceId: DevicesConnectionHelper.ICHOICE_DEVICE_ID)
        
        let dict = model.toDictionay()
        
        print("BP dict data: ",dict)
        
      let dict1 = ["data":[model.toDictionay()]]
            Api.share.postDeviceData(dict: dict1, compeletion: {  (success) in
                if success{
                    print("Bp Success")
                }else{
                    print("Bp Failed")
                }
            })
        
    }
    
    
    
    func initialize(){
        BleManager.shared().delegate = self
    }
    
    func startScan(){
        BleManager.shared().ble_startScanBleDevices(withDuration: 600)
    }
    
    func connectDevice(model:BleModel)  {
        
        switch DeviceName.init(rawValue: model.name)!{
        case .M2S:
            BleManager.shared().ble_connectBleDevice(model.peripheral)
            break
        case .iChoice:
            startiChoiceDiscovery()
            break
        case .HealthScale:
            let ble = BLECtl.instance()
            ble?.delegate = self
            ble?.sendDataToBlueString = "FE030100AA1901B0"
            ble?.setCentralManager()
            ble?.scan()
            break
            
        }
    }
    
    func disconnect(){
        if let p = self.peripheral{
              BleManager.shared()?.centralManager.cancelPeripheralConnection(p)
        }
    }
    
    func startiChoiceDiscovery() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onDeviceFound(_:)), name: NSNotification.Name("OnDeviceFound"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBPDataReceived(_:)), name: NSNotification.Name("OnIncomingBPData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBpDeviceConnected(_:)), name: NSNotification.Name("OnDeviceConnected_BP"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBpDeviceDisConnected(_:)), name: NSNotification.Name("OnDeviceDisConnected_BP"), object: nil)
        
        AppDelegate.shared().ichoiceManager.scan()
    }
    
    @objc func onDeviceFound(_ notification: Notification?) {
        print("DeviceAddViewController::OnDeviceFound()")
        
        let record = notification?.object as? DeviceRecord
        
        if let aType = record?.m_deviceType {
            print("Device type = \(aType)")
        }
        
        if record == nil {
            print("[E] empty record")
            return
        }
        
        if record?.m_deviceType == DEVICE_TYPE_NONE {
            print("[E] device with no type")
            return
        }
        
        if record?.name == DeviceName.iChoice.rawValue {
            AppDelegate.shared().addDevice(record)
            AppDelegate.shared().ichoiceManager.connectDevice(record)
        }
    }
    
    @objc func onBPDataReceived(_ notification : Notification){
        if let bp = notification.object as? BloodPressureMeasurement{
            let userID:Int = AppPreferences.share.get(forkey: .userID)!
            let dateStr = bp.date.getDateStringWithFormate("YYYY-MM-dd hh:mm:ss", timezone: TimeZone.current.abbreviation()!)
            let data = iChoiceDataPostModel.Data(date: dateStr, dia: bp.nDiaBP, pul: bp.nPulse, sys: bp.nSysBP, unitType: 0, useId: Int(userID))
            let model = iChoiceDataPostModel(data: data, deviceId: DevicesConnectionHelper.ICHOICE_DEVICE_ID)
            
            let dict = model.toDictionay()
            
            print("BP dict data: ",dict)
            
            let bp = "\(bp.nSysBP)/\(bp.nDiaBP)"
            AppPreferences.share.commit(data: bp, forkey: .bp)
            
            let dict1 = ["data":[model.toDictionay()]]
            
           // DispatchQueue.global(qos: .background).async {
                Api.share.postDeviceData(dict: dict1,compeletion: { (success) in
                    if success{
                        print("Bp Success")
                    }else{
                        print("Bp Failed")
                    }
                })
            //}
            sendDataUpdate()
        }
    }
    
    @objc func onBpDeviceConnected(_ notification:Notification){
        if let p = notification.object as? CBPeripheral{
            self.peripheral = p
            delegate?.deviceConnection(didConnect: peripheral!)
        }
    }
    
    @objc func onBpDeviceDisConnected(_ notification:Notification){
        if let p = self.peripheral{
            delegate?.deviceConnection(didDisconnected: p)
            self.peripheral = nil
        }
    }
    
    func sendDataUpdate(){
        NotificationCenter.default.post(name: Notification.Name("DeviceDataChanged"), object: nil)
    }
}
extension DevicesConnectionHelper : BleManagerDelegate{
    
    func bleBack_state_On() {
        startScan()
    }
    func bleBack_state_Off() {
        
    }
    func bleBack_disconnectDevice(with peripheral: CBPeripheral!, error: Error!) {
        self.peripheral = nil
        delegate?.deviceConnection(didDisconnected: peripheral)
        
        
    }
    func bleBack_scanDevices(withBleArray bleArray: NSMutableArray!) {
        
        delegate?.deviceConnection( bleArray as! [BleModel])
    }
    
    func bleBack_connectDeviceSuccess(with peripheral: CBPeripheral!) {
        self.peripheral = peripheral
        delegate?.deviceConnection(didConnect: peripheral)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            let dateModel = DateModel()
            /*if let oldSyncTime:Double = AppPreferences.share.get(forkey: .smartBandSync) {
                let date = Date(timeIntervalSince1970: oldSyncTime)
                dateModel.year = Int32(date.year())
                dateModel.month = Int32(date.month())
                dateModel.day = Int32(date.day())
                dateModel.hour = Int32(date.hours(from: date))
                dateModel.minute = Int32(date.minutes(from: date))
            }else{*/
                dateModel.year = 2018
                dateModel.month = 1
                dateModel.day = 1
                dateModel.hour = 16
                dateModel.minute = 20
            //}
            BleManager.shared().ble_sendAllData(with: dateModel)
        }
    }
    
    func bleBack_getAllData(_ allDataModel: AllDataModel!) {
        smartBandDataArr.append(allDataModel)
        checkSmartBandData()
    }
    
    func bleBack_getSleep(with sleepDataModel: SleepDataModel!) {
        print("Sleep Datta","Hour",sleepDataModel.hour)
        print("Sleep Datta","Min",sleepDataModel.minute)
        print("Sleep Datta","time",sleepDataModel.time)
        print("Sleep Datta","Day",sleepDataModel.day)
        print("Sleep Datta","Month",sleepDataModel.month)
        print("Sleep Datta","year",sleepDataModel.year)
        print("__==================================")
    }
    
    func checkSmartBandData(){
        if !isSmartBandDataSyncing{
            if smartBandDataArr.count > smartBandDataIndex{
                startSyncSmartBandData(allDataModel: smartBandDataArr[smartBandDataIndex])
            }else{
                smartBandDataIndex = 0
                smartBandDataArr.removeAll()
            }
        }
    }
    
    func startSyncSmartBandData(allDataModel : AllDataModel){
        //"yyyy-MM-dd HH:mm:ss"
        
        let year = allDataModel.year
        let month = allDataModel.month
        let day = allDataModel.day
        let hour = allDataModel.hour
        let min = allDataModel.min
        
        //let dateStr = "\(year)-\(month)-\(day) \(hour):\(min):00"//Date().getDateStringWithFormate("yyyy-MM-dd HH:mm:ss",timezone: TimeZone.current.abbreviation()!)
        
        let date = Date().createDate(day: Int(day), month: Int(month), year: Int(year),
                                     timeZone: TimeZone.current.abbreviation()!, hour: Int(hour), minute: Int(min))
        
        let dateStr = date.getDateStringWithFormate("yyyy-MM-dd HH:mm:ss",timezone: TimeZone.current.abbreviation()!)
        
        let userID:Int = AppPreferences.share.get(forkey: .userID)!
        
        let pref = AppPreferences.share
        
        pref.commit(data: Int(allDataModel.oxygen), forkey: .spo2)
        
        pref.commit(data: Int(allDataModel.cal), forkey: .calories)
        pref.commit(data: Int(allDataModel.step), forkey: .steps)
        pref.commit(data: Int(allDataModel.heart), forkey: .heartRate)
        pref.commit(data: allDataModel.shallowSleep, forkey: .sleepHours)
        pref.commit(data: allDataModel.deepSleep, forkey: .sleepMins)
        
        let data = SmartBandDataPostModel.Data(bloodOxygen: Int(allDataModel.oxygen), bloodOxygenRealTime: Int(allDataModel.oxygen), bloodPressureDown: Int(allDataModel.minBlood), bloodPressureUp: Int(allDataModel.maxBlood), buttonTest: "", caloriesBurt: Int(allDataModel.cal), chargingEleQty: 0, date: dateStr, deepSleep: Int(allDataModel.deepSleep), ecg: "", fall: "", heartRate: Int(allDataModel.heart), heartRateRealTime: Int(allDataModel.heart), heartRateSensor: "", hourlyMeasure: 0, rssi: 0, shallowSleep: Int(allDataModel.shallowSleep), sleepHours: 0, sleepMinutes: 0, spo2: Int(allDataModel.oxygen), steps: Int(allDataModel.step), triaxialsensor: "", unchargedEleQty: 0, unitType: 0, useId: Int(userID), vesrionCode: "")
        
        let model = SmartBandDataPostModel(data: data, deviceId: DevicesConnectionHelper.SMART_BAND_DEVICE_ID)
        
        isSmartBandDataSyncing = true
        
        let dict = ["data":[model.toDictionay()]]
        
       // DispatchQueue.global(qos: .background).async {
            Api.share.postDeviceData(dict: dict, compeletion: { (success) in
                if success{
                    print("Smart Band Success")
                    AppPreferences.share.commit(data: date.timeIntervalSince1970, forkey: .smartBandSync)
                }else{
                    print("Smart Band Failed")
                }
                self.isSmartBandDataSyncing = false
                self.smartBandDataIndex += 1
                self.checkSmartBandData()
            })
        //}
        sendDataUpdate()
    }
}

extension DevicesConnectionHelper : BLECtlDelegate{
    func bleCtlDidReceivedData(_ receiveDic: [AnyHashable : Any]!) {
        NSLog("receive dic %@",receiveDic);
        
        let BMR = receiveDic["BMR"] as? Float
        let HealthAge = receiveDic["HealthAge"] as? Float
        // let age = receiveDic["age"] as? Float
        let bodyWater = receiveDic["bodyWater"] as? Float
        // let bone = receiveDic["bone"] as? Float
        // let device = receiveDic["device"]
        let fat = receiveDic["fat"] as? Float
        // let height = receiveDic["height"]
        // let level = receiveDic["level"]
        let muscleMass = receiveDic["muscleMass"] as? Float
        // let sex = receiveDic["sex"]
        // let userTypt = receiveDic["userTypt"]
        let visceralFat = receiveDic["visceralFat"] as? Float
        var weight = receiveDic["weight"] as? Float
        if let w = weight{
            if w > 0{
                weight = w/10
            }
        }
        
        AppPreferences.share.commit(data: weight ?? 0, forkey: .bodyWeight)
        
        let userID:Int = AppPreferences.share.get(forkey: .userID)!
        let dateStr = Date().getDateStringWithFormate("yyyy-MM-dd hh:mm:ss", timezone: TimeZone.current.abbreviation()!)
        let data = SmartBodyAnalyzerDataPostModel.Data(bmi: 0, bmr: BMR, bodyWeight: weight, bodyage: HealthAge, fat: fat, moisture: bodyWater, muscle: muscleMass, skeleton: 0, unitType: 0, useId: Int(userID), visceralFat: visceralFat, date: dateStr)
        let model = SmartBodyAnalyzerDataPostModel(data: data, deviceId: DevicesConnectionHelper.SMART_BODY_ANAYLZER_DEVICE_ID)
        
        
        if let oldDateStr:String = AppPreferences.share.get(forkey: .bodyAnalyzerLastDataSync){
            if oldDateStr ==  dateStr{
                return
            }
        }
        
        AppPreferences.share.commit(data: dateStr, forkey: .bodyAnalyzerLastDataSync)
        let dict = ["data":[model.toDictionay()]]
      //  DispatchQueue.global(qos: .background).async {
            Api.share.postDeviceData(dict: dict,  compeletion: { (success) in
                if success{
                    print("Body Data Success")
                }else{
                    print("Body Data Failed")
                }
            })
        //}
        
        sendDataUpdate()
        
    }
    func bleCtlDidDisconnectPeripheral(_ aPeripheral: CBPeripheral!) {
        self.delegate?.deviceConnection(didDisconnected: aPeripheral)
        self.peripheral = nil
    }
    func bleCtlDidConnect(_ aPeripheral: CBPeripheral!, macData mac: Data!) {
        self.peripheral = aPeripheral
        self.delegate?.deviceConnection(didConnect: aPeripheral)
    }
}
