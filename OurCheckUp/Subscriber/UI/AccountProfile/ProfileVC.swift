//
//  ProfileVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 04/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit
import DropDown
class ProfileVC: BaseVC {

    @IBOutlet weak var tfFirstName:UITextField!
    @IBOutlet weak var tfLastName:UITextField!
    @IBOutlet weak var tfEmail:UITextField!
    @IBOutlet weak var tfNationality:UITextField!
    @IBOutlet weak var tfGender:UIButton!
    @IBOutlet weak var tfDOB:UITextField!
    @IBOutlet weak var tfAddress1:UITextField!
    @IBOutlet weak var tfAddress2:UITextField!
    @IBOutlet weak var tfPostalCode:UITextField!
    @IBOutlet weak var tfState:UITextField!
    @IBOutlet weak var tfCountry:UITextField!
    @IBOutlet weak var tfPhoneNumer:UITextField!
    @IBOutlet weak var tfMobileNo:UITextField!
    @IBOutlet weak var tfFaxNo:UITextField!
    @IBOutlet weak var tfHeight:UITextField!
    @IBOutlet weak var btnSave:UIButtonX!
    
    let dropDown = DropDown()
    
    var isHideBackBtn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Profile")
        
        getDetails()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.tfGender.setTitle(item, for: .normal)
        }
        
        if isHideBackBtn{
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
    }
    
    @IBAction func btnGenderAction(_ sender: UIButton){
        dropDown.anchorView = sender
        dropDown.dataSource = ["Male", "Female"]
        dropDown.show()
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton){
        validateAndSave()
    }
    
    func getDetails(){
        let userId:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        let email:String? = AppPreferences.share.get(forkey: .userEmail)
        let firstName:String? = AppPreferences.share.get(forkey: .userFirstName)
        let lastName: String? = AppPreferences.share.get(forkey: .userLastName)
        
        self.tfFirstName.text = firstName
        self.tfLastName.text = lastName
        self.tfEmail.text = email
        
        
        let param = ["user_id" : userId]
        self.showProgress()
        Api.share.getSubscriberDetails(params: param) { (model, status) in
            self.hideProgress()
            if status{
                if let data = model?.data{
                    if data.count > 0{
                        let element = data[0]
                        DispatchQueue.main.async {
                         
                            self.tfFirstName.text = element.firstName
                            self.tfLastName.text = element.lastName
                            self.tfEmail.text = element.emailUserId
                            self.tfNationality.text = element.nationalId
                            if let gender = element.gender {
                                self.tfGender.setTitle(gender, for: .normal)
                            }
                            if let dobstr = element.dob{
                                let str = dobstr.components(separatedBy: "T")[0]
                                self.tfDOB.text = str
                            }
                            self.tfAddress1.text = element.address 
                            self.tfAddress2.text = element.address2
                            self.tfPostalCode.text = element.postcode
                            self.tfState.text = element.state
                            self.tfCountry.text = element.country
                            self.tfMobileNo.text = element.mobileNo
                            self.tfPhoneNumer.text = element.phoneNo
                            self.tfFaxNo.text = element.faxNo
                            if let height = element.height as? String{
                                self.tfHeight.text = height
                            }else if let height = element.height as? Float{
                                self.tfHeight.text = "\(height)"
                            }
                        }
                    }else{
                        showError(error: "No details avaiable!")
                    }
                }
            }
        }
    }
    
    func validateAndSave(){
        let firstName = tfFirstName.text!
        let lastName = tfLastName.text!
        let email = tfEmail.text!
        let nationalId = self.tfNationality.text!
        let gender =    self.tfGender.titleLabel!.text!
        let dob = self.tfDOB.text!
        let address1 = self.tfAddress1.text!
        let address2 = tfAddress2.text!
        let postalCode = tfPostalCode.text!
        let state = tfState.text!
        let country = tfCountry.text!
        let mobile = tfMobileNo.text!
        let phoneNo = tfPhoneNumer.text!
        let faxNo = tfFaxNo.text!
        let height = self.tfHeight.text!
        
        if firstName.isEmpty{
            showError(error: "Please enter firstname")
            return
        }
        if lastName.isEmpty{
            showError(error: "Please enter lastname")
            return
        }
        if email.isEmpty{
            showError(error: "Please enter email")
            return
        }
        if !email.isValidEmail{
            showError(error: "Please enter valid email")
            return
        }
        if nationalId.isEmpty{
            showError(error: "Please enter national id")
            return
        }
        if gender.contains("Select"){
            showError(error: "Please select gender")
            return
        }
        if dob.isEmpty{
            showError(error: "Please enter Date of Birth")
            return
        }
        if address1.isEmpty
        {
            showError(error: "Please enter address 1")
            return
        }
        if address2.isEmpty
        {
            showError(error: "Please enter address 2")
            return
        }
        if postalCode.isEmpty
        {
            showError(error: "Please enter postal code")
            return
        }
        if state.isEmpty
        {
            showError(error: "Please enter state name")
            return
        }
        if country.isEmpty
        {
            showError(error: "Please enter country name")
            return
        }
        if mobile.isEmpty
        {
            showError(error: "Please enter mobile")
            return
        }
        if phoneNo.isEmpty
        {
            showError(error: "Please enter phone number")
            return
        }
        if faxNo.isEmpty
        {
            showError(error: "Please enter fax number")
            return
        }
        if height.isEmpty
        {
            showError(error: "Please enter height in centimeters only")
            return
        }
        
        let params: [String:Any] = ["user_id": getUserID(),
                                    "first_name": firstName,
                                    "last_name": lastName,
                                    "email_user_id": email,
                                    "national_id": nationalId,
                                    "dob": dob,
                                    "gender": gender,
                                    "address": address1,
                                    "address_2": address2,
                                    "postcode": postalCode,
                                    "state": state,
                                    "country": country,
                                    "phone_no": phoneNo,
                                    "mobile_no": mobile,
                                    "fax_no": faxNo ,
                                    "height" : height]
        
        self.showProgress()
        Api.share.updateSubscriberDetails(params: params) { (msg, status) in
            self.hideProgress()
            if status{
                AppPreferences.share.commit(data: true, forkey: .profileUpdated)
                self.navigationItem.setHidesBackButton(false, animated: true)
                displayAlert("Success", andMessage: msg ?? "")
            }else{
                showError(error: "Error occured")
            }
        }
    }
}
