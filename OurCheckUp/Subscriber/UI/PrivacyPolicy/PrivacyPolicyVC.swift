//
//  PrivacyPolicyVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 13/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class PrivacyPolicyVC: BaseVC {
    
    @IBOutlet weak var webView : UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
setTitle(title: "Privacy Policy")
        if let pdf = Bundle.main.url(forResource: "PrivacyPolicy", withExtension: "pdf", subdirectory: nil) {
            let req = URLRequest(url: pdf)
            webView.loadRequest(req)
        }
    }

    

}
