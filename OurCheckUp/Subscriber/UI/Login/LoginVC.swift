//
//  LoginVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 06/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class LoginVC: BaseVC {
    
    @IBOutlet weak var tfEmail : SkyFloatingLabelTextField!
    @IBOutlet weak var tfPassword : SkyFloatingLabelTextField!
    @IBOutlet weak var btnLogin:UIButton!
    @IBOutlet weak var viewScrollViewChild: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if let isLogin: Bool = AppPreferences.share.get(forkey: .isLogin)
            ,let category:String = AppPreferences.share.get(forkey: .userCategory){
            if isLogin{
                self.viewScrollViewChild.alpha = 0
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    if category == "Doctor"{
                          self.performSegue(id: .doctor)
                    }else{
                        self.performSegue(id: .dashboardSubsriber)
                    }
                   self.viewScrollViewChild.alpha = 1
                }
            }
        }
    }

    @IBAction func btnLoginAction(){
        if validate() {
            doLogin()
            //DevicesConnectionHelper.share.post()
        }
    }
    
    @IBAction func btnSignupAction(){
        self.performSegue(id: .signup)
    }
   
    @IBAction func btnDoctorAction(){
       // let st = UIStoryboard(name: "Doctor", bundle: nil)
       // let vc = st.instantiateViewController(withIdentifier: "DrSideDrawerVC")
       // self.navigationController?.pushViewController(vc, animated: true)
        self.performSegue(id: .doctor)
    }
    
    func validate()->Bool{
        if tfEmail.text!.isEmpty {
            displayAlert("Error", andMessage: "Please enter username")
            return false
        }else if tfPassword.text!.isEmpty{
            displayAlert("Error", andMessage: "Please enter Password")
            return false
        }/*else if !tfEmail.text!.isValidEmail{
            displayAlert("Error", andMessage: "Please enter valid email")
            return false
        }*/
        return true
    }
    
    func doLogin(){
        self.showProgress()
        Api.share.doLogin(username: tfEmail.text!, password: tfPassword.text!) { (success) in
            self.hideProgress()
            self.tfPassword.text = ""
            self.tfEmail.text = ""
            if success{
                AppPreferences.share.commit(data: self.tfEmail.text ?? "", forkey: .username)
                AppPreferences.share.commit(data: self.tfPassword.text ?? "", forkey: .password)
                //self.getUserDetail()
                AppPreferences.share.commit(data: true, forkey: .isLogin)
                if let category:String = AppPreferences.share.get(forkey: .userCategory){
                    if category == "Doctor"{
                        self.performSegue(id: .doctor)
                    }else{
                        self.performSegue(id: .dashboardSubsriber)
                    }
                }else{
                     self.performSegue(id: .dashboardSubsriber)
                }
            }else{
                
            }
        }
    }
    
    func getUserDetail()
    {
        Api.share.getUserDetails(username: tfEmail.text!) { (user) in
            self.hideProgress()
            if let user1 = user {
                AppPreferences.share.commit(data: user1.id ?? "", forkey: .userID)
                self.performSegue(id: .dashboardSubsriber)
            }else{
                
            }
        }
    }
}
