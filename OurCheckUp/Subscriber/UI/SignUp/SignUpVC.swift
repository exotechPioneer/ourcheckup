//
//  SignUpVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 23/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown
class SignUpVC: BaseVC {

    @IBOutlet weak var tfFirstname : SkyFloatingLabelTextField!
    @IBOutlet weak var tfLastname : SkyFloatingLabelTextField!
    @IBOutlet weak var tfEmail : SkyFloatingLabelTextField!
    @IBOutlet weak var tfPassword : SkyFloatingLabelTextField!
    @IBOutlet weak var btnLogin:UIButton!
    @IBOutlet weak var btnSignUp:UIButton!
    @IBOutlet weak var btnCategory:UIButton!
    let dropDown = DropDown()
    
    var category : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.category = item
            self.btnCategory.setTitle(item, for: .normal)
        }
    }
    
    @IBAction func btnLoginAction(){
      self.dismiss(animated: true)
    }
    @IBAction func btnSignUpAction(){
        if validate(){
            self.showProgress()
            Api.share.doSignUp(firstname: tfFirstname.text!, lastname: tfLastname.text!, email: tfEmail.text!, password: tfPassword.text!,category: category!) { (status) in
                self.hideProgress()
                if status{
                    displayAlert("Success", andMessage: "SignUp Successfully", completion: {
                         self.dismiss(animated: true)
                    })
                }
            }
        }
    }
    
    @IBAction func btnCategoryAction(sender: UIButton){
        dropDown.anchorView = sender
        dropDown.dataSource = ["Subscriber", "Doctor"]
        dropDown.show()
    }
    
  
    func validate()->Bool{
        if tfFirstname.text!.isEmpty {
            displayAlert("Error", andMessage: "Please enter Firstname")
            return false
        }
        else if tfLastname.text!.isEmpty {
            displayAlert("Error", andMessage: "Please enter Lastname")
            return false
        }
        else if tfEmail.text!.isEmpty {
            displayAlert("Error", andMessage: "Please enter email")
            return false
        }else if !(tfEmail.text!.isValidEmail){
             displayAlert("Error", andMessage: "Please enter valid email")
            return false
        }
        else if tfPassword.text!.isEmpty{
            displayAlert("Error", andMessage: "Please enter Password")
            return false
        }else if category == nil{
            displayAlert("Error", andMessage: "Please select Category")
            return false
        }
        /*else if !tfEmail.text!.isValidEmail{
         displayAlert("Error", andMessage: "Please enter valid email")
         return false
         }*/
        return true
    }
}
