//
//  FitbitDataSync.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 02/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation

protocol FitBitDataSyncDelegate {
    func didSyncDateChange(dateStr:String)
    func didTodayDataUploaded()
    func syncCompleted()
}

class FitbitDataSync{
    static let share = FitbitDataSync()
    var dataPostIndex = 0
    var delegate:FitBitDataSyncDelegate?
    let maxEntryArrayCount = 50
    var dataArray = [[String:Any]]()
    var steps:FitbitActivityStepsMdl?,
    sleep:FitbitSleepMdl?,heartRate:FitbitActivityHeartRateMdl?,
    calories:FitbitActivityCaloriesMdl?,distance:FitbitActivityDistanceMdl?
    var syncingDate : Date!
    var lastSyncDate:Date?
   
    var addDelayAtIndex = 500
    
    func getData(date:Date){
        dataPostIndex = 0
        syncingDate = date
        let dateStr = date.dateString(withFormat: "yyyy-MM-dd")
        FitbitAPI.loadData(path: dateStr, period: "/1d.json", completion:{
            (steps,sleep,heartRate,calories,distance) in
            if date.day() == Date().day() && date.month() == Date().month(){
                if let calsArr = calories?.activitiescalories{
                    if calsArr.count > 0{
                        let intCals = Int(calsArr[0].value ?? "0")!
                        AppPreferences.share.commit(data: intCals, forkey: .calories)
                    }
                }
                if let stepsArr = steps?.activitiessteps{
                    if stepsArr.count > 0{
                        let intSteps = Int(stepsArr[0].value ?? "0")!
                        AppPreferences.share.commit(data: intSteps, forkey: .steps)
                    }
                }
                /*if let heartArr = heartRate?.activitiesheart{
                    if heartArr.count > 0{
                        let heartInt = Int(heartArr[0].value.restingHeartRate)
                        AppPreferences.share.commit(data: heartInt, forkey: .heartRate)
                    }
                }*/
                
                if let sleepArr = sleep?.sleep{
                    if sleepArr.count > 0{
                        let duration = sleepArr[0].duration ?? 0
                        //let (h,m,_) = secondsToHoursMinutesSeconds(seconds: (duration ?? 0)/1000)
                        AppPreferences.share.commit(data: duration, forkey: .sleepMins)
                        AppPreferences.share.commit(data: 0, forkey: .sleepHours)
                    }
                }
                self.delegate?.didTodayDataUploaded()
            }
            self.startDataPosting(date: date, steps: steps, sleep: sleep, heartRate: heartRate, calories: calories, distance: distance)
        },errorResult: { error in
            showError(error: error)
            AppPreferences.share.commit(data: "", forkey: .fitbitAuthToken)
        })
    }
    
    func startDataPosting(date:Date,steps:FitbitActivityStepsMdl?,
                          sleep:FitbitSleepMdl?,
                          heartRate:FitbitActivityHeartRateMdl?,
                          calories:FitbitActivityCaloriesMdl?,
                          distance:FitbitActivityDistanceMdl?){
       
        self.steps = steps
        self.sleep = sleep
        self.heartRate = heartRate
        self.calories = calories
        self.distance = distance
        
        if let steps = steps,let sleep = sleep,let heartRate = heartRate,
            let calories = calories{
            
            if let activityInd = steps.activitiesstepsintraday{
                if let dataSet = activityInd.dataset{
                    if dataPostIndex >= dataSet.count{
                        var newDate = date.addDays(1)
                        let newDateStr = newDate.dateString(withFormat: "yyyy-MM-dd")
                        newDate = newDateStr.date(withFormat: "yyyy-MM-dd")!
                        
                        var todayDate = Date()
                        let todayDateStr = todayDate.dateString(withFormat: "yyyy-MM-dd")
                        todayDate = todayDateStr.date(withFormat: "yyyy-MM-dd")!
                        
                        if todayDate.addDays(1).isGreaterThanDate(newDate){
                            self.addDelayAtIndex = 500
                            getData(date: newDate)
                        }else{
                            delegate?.syncCompleted()
                        }
                        return
                    }
                    
                    let time = dataSet[dataPostIndex].time ?? "00:00:00"
                    let stps = dataSet[dataPostIndex].value ?? 0
                    var hr = 0
                    if heartRate.activitiesheartintraday!.dataset!.count > dataPostIndex{
                        hr = heartRate.activitiesheartintraday!.dataset![dataPostIndex].value ?? 0
                    }
                    var cals = 0
                    if calories.activitiescaloriesintraday!.dataset.count > dataPostIndex{
                        cals = calories.activitiescaloriesintraday!.dataset![dataPostIndex].value ?? 0
                    }
                    let dist = 0
                    /*if distance.activitiesdistanceintraday!.dataset.count > dataPostIndex{
                        dist = distance.activitiesdistanceintraday!.dataset![dataPostIndex].value ?? 0
                    }
                    */
                    
                    let sleepInt = sleep.sleep.count > 0 ?  Int(sleep.sleep[0].duration ?? 0) : 0
                    
                    if dataArray.count < maxEntryArrayCount && self.dataPostIndex < dataSet.count{
                        
                        let dateStr = date.dateString(withFormat: "yyyy-MM-dd")+" "+time
                        var isAlready = false
                        if let lastSyncDate = self.lastSyncDate{
                            if let entryDate = dateStr.date(withFormat: "yyyy-MM-dd HH:mm:ss"){
                                if entryDate.timeIntervalSince1970 > lastSyncDate.timeIntervalSince1970{
                                    self.addIntoArray(dateStr: dateStr, time: time, steps: stps, sleep:sleepInt, heartRate: hr, calories: cals,distance: Float(dist))
                                }else{
                                     isAlready = true
                                    print("Item skipped \(dataPostIndex)  "+dateStr)
                                }
                            }
                        }else{
                            self.addIntoArray(dateStr: dateStr, time: time, steps: stps, sleep:sleepInt, heartRate: hr, calories: cals,distance: Float(dist))
                        }
                        
                        self.dataPostIndex += 1
                        var delay:Double = 0
                        if dataPostIndex > addDelayAtIndex && isAlready{
                            delay = 2
                            addDelayAtIndex += 400
                            isAlready = false
                        }
                        let time1 = DispatchTime.now() + delay
                        DispatchQueue.main.asyncAfter(deadline: time1) {
                             self.startDataPosting(date: date, steps: steps, sleep: sleep, heartRate: heartRate, calories: calories, distance: distance)
                        }
                       
                    }else{
                        let dict = ["data":self.dataArray]
                        self.setToServer(data: dict)
                    }
                }
            }
        }
    }
    
   
    
    func addIntoArray(dateStr:String,time:String,steps:Int,sleep:Int,heartRate:Int,
                                calories:Int,distance:Float){
        //yyyy-MM-dd HH:mm:ss
         AppPreferences.share.commit(data: heartRate, forkey: .heartRate)
       
        let userID:Int = AppPreferences.share.get(forkey: .userID)!
        
        let data = SmartBandDataPostModel.Data(bloodOxygen: 0, bloodOxygenRealTime: 0, bloodPressureDown:0, bloodPressureUp: 0, buttonTest: "0", caloriesBurt: calories, chargingEleQty: 0, date: dateStr, deepSleep: sleep, ecg: "0", fall: "0", heartRate: heartRate, heartRateRealTime: heartRate, heartRateSensor: "0", hourlyMeasure: 0, rssi: 0, shallowSleep: sleep, sleepHours: 0, sleepMinutes: sleep, spo2: 0, steps: steps, triaxialsensor: "0", unchargedEleQty: 0, unitType: 0, useId: userID,vesrionCode: "1")
        
        let model = SmartBandDataPostModel(data: data, deviceId: "SmartBand_03")
        
        self.dataArray.append(model.toDictionay())
    }
    
    func setToServer(data:[String:Any]){
        let dateStr = syncingDate.dateString(withFormat: "yyyy-MM-dd hh:mm")
        self.delegate?.didSyncDateChange(dateStr: dateStr)
        Api.share.postDeviceData(dict: data, compeletion: { (success) in
            // self.hideProgress()
            if success{
                print("Smart Band Success")
                    //AppPreferences.share.commit(data:d, forkey: .smartBandSync)
                    self.dataArray.removeAll()
                    self.startDataPosting(date: self.syncingDate, steps: self.steps, sleep: self.sleep, heartRate: self.heartRate, calories: self.calories, distance: self.distance)
            }else{
                print("Smart Band Failed")
            }
        })
    }
    
    
}


