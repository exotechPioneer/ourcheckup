//
//  FitbitAPI.swift
//  SampleBit
//
//  Created by Ryan LaSante on 9/14/16.
//  Copyright © 2016 Fitbit. All rights reserved.
//

import UIKit

class FitbitAPI {
    
    static let sharedInstance: FitbitAPI = FitbitAPI()
    
    static let baseAPIURL = URL(string:"https://api.fitbit.com/1")
    
    func authorize(with token: String) {
        let sessionConfiguration = URLSessionConfiguration.default
        var headers = sessionConfiguration.httpAdditionalHeaders ?? [:]
        headers["Authorization"] = "Bearer \(token)"
        sessionConfiguration.httpAdditionalHeaders = headers
        session = URLSession(configuration: sessionConfiguration)
    }
    
    var session: URLSession?
    
    
    static func loadData(path:String,period:String,
                         completion:@escaping (_ stepsResult:FitbitActivityStepsMdl?,
        _ sleepResult : FitbitSleepMdl?,_ heartRateResult:FitbitActivityHeartRateMdl?,
        _ caloriesResult:FitbitActivityCaloriesMdl?,_ distanceResult:FitbitActivityDistanceMdl?)->Void,errorResult:@escaping (String)->Void){
        _ = fetchTodaysStepStat(path: path, peroid: period) { (steps, stepErr) in
            if let stepEr = stepErr{
                showError(error: stepEr.localizedDescription)
                errorResult(stepEr.localizedDescription)
                return
            }
            if steps == nil && stepErr == nil{
                errorResult("Login session expired. Please login again to fitbit.")
                return
            }
            _ = fetchTodaysSleep(path: path, peroid: period, callback: { (sleep, sleepError) in
                if let sleepError = sleepError{
                    showError(error: sleepError.localizedDescription)
                }
                _ = fetchTodaysHeartRate(path: path, peroid: period, callback: { (heartRate, heartRateError) in
                    if let heartRateError = heartRateError{
                        showError(error: heartRateError.localizedDescription)
                    }
                    _ = fetchTodaysCalories(path: path, peroid: period, callback: { (calories, calError) in
                        if let calError = calError{
                            showError(error: calError.localizedDescription)
                        }
                        completion(steps,sleep,heartRate,calories,nil)
                    })
                })
            })
        }
    }
    
    static func fetchTodaysStepStat(path:String,peroid:String,callback: @escaping (FitbitActivityStepsMdl?, Error?)->Void) -> URLSessionDataTask? {
        let datePath = path+peroid
        return fetchSteps(for: datePath) { (stepStats, error) in
            callback(stepStats, error)
        }
    }
    
    static func fetchTodaysCalories(path:String,peroid:String,callback: @escaping (FitbitActivityCaloriesMdl?, Error?)->Void) -> URLSessionDataTask? {
        let datePath = path+peroid
        return fetchCalories(for: datePath) { (stepStats, error) in
            callback(stepStats, error)
        }
    }
    
    static func fetchTodaysDistance(path:String,peroid:String,callback: @escaping (FitbitActivityDistanceMdl?, Error?)->Void) -> URLSessionDataTask? {
        let datePath = path+peroid
        return fetchDistance(for: datePath) { (stepStats, error) in
            callback(stepStats, error)
        }
    }
    
    static func fetchTodaysHeartRate(path:String,peroid:String,callback: @escaping (FitbitActivityHeartRateMdl?, Error?)->Void) -> URLSessionDataTask? {
        let datePath = path+peroid
        return fetchHeartRate(for: datePath) { (stepStats, error) in
            callback(stepStats, error)
        }
    }
    
    static func fetchTodaysWeight(path:String,peroid:String,callback: @escaping (FitbitWeightMdl?, Error?)->Void) -> URLSessionDataTask? {
        let datePath = path+peroid
        return fetchWeight(for: datePath) { (stepStats, error) in
            callback(stepStats?.first, error)
        }
    }
    
    static func fetchTodaysSleep(path:String,peroid:String,callback: @escaping (FitbitSleepMdl?, Error?)->Void) -> URLSessionDataTask? {
        let datePath = path+".json"
        return fetchSleep(for: datePath) { (stepStats, error) in
            callback(stepStats, error)
        }
    }
    
    static func fetchSteps(for dateRange: NSDateInterval, callback: @escaping (FitbitActivityStepsMdl?, Error?)->Void) -> URLSessionDataTask? {
        let startDate = dateRange.startDate.dateString()
        let endDate = dateRange.endDate.dateString()
        let datePath = "/\(startDate)/\(endDate).json"
        return fetchSteps(for: datePath, callback: callback)
    }
    
    static func fetchDistance(for datePath: String, callback: @escaping (FitbitActivityDistanceMdl?, Error?)->Void) -> URLSessionDataTask? {
        guard let session = FitbitAPI.sharedInstance.session,
            let distanceURL = URL(string: "https://api.fitbit.com/1/user/-/activities/distance/date/\(datePath)") else {
                return nil
        }
        print("Distance Url: "+distanceURL.absoluteURL.absoluteString)
        let dataTask = session.dataTask(with: distanceURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: AnyObject] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    return
            }
            
            let activity:FitbitActivityDistanceMdl? = FitbitActivityDistanceMdl(fromDictionary: dictionary)
            
            /*let str = String(data: data, encoding: .utf8)
             // print(str?.replacingOccurrences(of: "\\", with: "") ?? "nnnnn")
             // print(dictionary)
             guard let distance = dictionary["activities-distance"] as? [[String: AnyObject]] else { return }
             let stats = distance.compactMap({ StepStat(withDictionary: $0) })
             */
            DispatchQueue.main.async {
                callback(activity, nil)
            }
        }
        dataTask.resume()
        return dataTask
    }
    
    static func fetchCalories(for datePath: String, callback: @escaping (FitbitActivityCaloriesMdl?, Error?)->Void) -> URLSessionDataTask? {
        guard let session = FitbitAPI.sharedInstance.session,
            let caloriesURL = URL(string: "https://api.fitbit.com/1/user/-/activities/calories/date/\(datePath)") else {
                return nil
        }
         print("caloriesURL : "+caloriesURL.absoluteURL.absoluteString)
        let dataTask = session.dataTask(with: caloriesURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    return
            }
            /* let str = String(data: data, encoding: .utf8)
             // print(str?.replacingOccurrences(of: "\\", with: "") ?? "nnnnn")
             // print(dictionary)
             guard let calories = dictionary["activities-calories"] as? [[String: AnyObject]] else { return }
             let stats = calories.compactMap({ StepStat(withDictionary: $0) })*/
            let activity:FitbitActivityCaloriesMdl? = FitbitActivityCaloriesMdl(fromDictionary: dictionary)
            DispatchQueue.main.async {
                callback(activity, nil)
            }
        }
        dataTask.resume()
        return dataTask
    }
    
    static func fetchSteps(for datePath: String, callback: @escaping (FitbitActivityStepsMdl?, Error?)->Void) -> URLSessionDataTask? {
        guard let session = FitbitAPI.sharedInstance.session,
            let stepURL = URL(string: "https://api.fitbit.com/1/user/-/activities/steps/date/\(datePath)") else {
                return nil
        }
        print("stepURL : "+stepURL.absoluteURL.absoluteString)
        let dataTask = session.dataTask(with: stepURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: AnyObject] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    return
            }
            let activity:FitbitActivityStepsMdl? = FitbitActivityStepsMdl(fromDictionary: dictionary)
            /*let str = String(data: data, encoding: .utf8)
             //print(str?.replacingOccurrences(of: "\\", with: "") ?? "nnnnn")
             //print(dictionary)
             guard let steps = dictionary["activities-steps"] as? [[String: AnyObject]] else { return }
             let stats = steps.compactMap({ StepStat(withDictionary: $0) })*/
            DispatchQueue.main.async {
                callback(activity, nil)
            }
        }
        dataTask.resume()
        return dataTask
    }
    
    
    static func fetchHeartRate(for datePath: String, callback: @escaping (FitbitActivityHeartRateMdl?, Error?)->Void) -> URLSessionDataTask? {
        guard let session = FitbitAPI.sharedInstance.session,
            let heartRateURL = URL(string: "https://api.fitbit.com/1/user/-/activities/heart/date/\(datePath)") else {
                return nil
        }
        print("heartRateURL : "+heartRateURL.absoluteURL.absoluteString)
        let dataTask = session.dataTask(with: heartRateURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    return
            }
            let activity:FitbitActivityHeartRateMdl? = FitbitActivityHeartRateMdl(fromDictionary: dictionary)
            /*let str = String(data: data, encoding: .utf8)
             //print(str?.replacingOccurrences(of: "\\", with: "") ?? "nnnnn")
             //print(dictionary)
             guard let heartRate = dictionary["activities-heart"] as? [[String: AnyObject]] else { return }
             let stats = heartRate.compactMap({ FitbitHeartRateMdl(withDictionary: $0) })*/
            DispatchQueue.main.async {
                callback(activity, nil)
            }
        }
        dataTask.resume()
        return dataTask
    }
    
    
    static func fetchWeight(for datePath: String, callback: @escaping ([FitbitWeightMdl]?, Error?)->Void) -> URLSessionDataTask? {
        guard let session = FitbitAPI.sharedInstance.session,
            let weightURL = URL(string: "https://api.fitbit.com/1/user/-/body/log/weight/date/\(datePath)") else {
                return nil
        }
        let dataTask = session.dataTask(with: weightURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: AnyObject] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    return
            }
            //let str = String(data: data, encoding: .utf8)
            //print(str?.replacingOccurrences(of: "\\", with: "") ?? "nnnnn")
            //print(dictionary)
            guard let weight = dictionary["weight"] as? [[String: AnyObject]] else { return }
            let stats = weight.compactMap({ FitbitWeightMdl(withDictionary: $0) })
            DispatchQueue.main.async {
                callback(stats, nil)
            }
        }
        dataTask.resume()
        return dataTask
    }
    
    static func fetchActivity(for datePath: String, callback: @escaping ([StepStat]?, Error?)->Void) -> URLSessionDataTask? {
        guard let session = FitbitAPI.sharedInstance.session,
            let activityURL = URL(string: "https://api.fitbit.com/1/user/-/activities/list.json?offset=2&limit=5&sort=desc&beforeDate=2019-01-22") else {
                return nil
        }
        let dataTask = session.dataTask(with: activityURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: AnyObject] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    return
            }
            // let str = String(data: data, encoding: .utf8)
            // print(str?.replacingOccurrences(of: "\\", with: "") ?? "nnnnn")
            //print(dictionary)
            guard let activity = dictionary["activities"] as? [[String: AnyObject]] else { return }
            let stats = activity.compactMap({ StepStat(withDictionary: $0) })
            DispatchQueue.main.async {
                callback(stats, nil)
            }
        }
        dataTask.resume()
        return dataTask
    }
    
    
    static func fetchSleep(for datePath: String, callback: @escaping (FitbitSleepMdl?, Error?)->Void) -> URLSessionDataTask? {
        guard let session = FitbitAPI.sharedInstance.session,
            let sleepURL = URL(string: "https://api.fitbit.com/1.2/user/-/sleep/date/\(datePath)") else {
                return nil
        }
        
        print("sleepURL : "+sleepURL.absoluteURL.absoluteString)
        
        let dataTask = session.dataTask(with: sleepURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    callback(nil, error)
                }
                return
            }
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: AnyObject] else {
                    DispatchQueue.main.async {
                        callback(nil, error)
                    }
                    return
            }
            let activity:FitbitSleepMdl? = FitbitSleepMdl(fromDictionary: dictionary)
            /*let str = String(data: data, encoding: .utf8)
            // print(str?.replacingOccurrences(of: "\\", with: "") ?? "nnnnn")
            //print(dictionary)
            guard let sleep = dictionary["sleep"] as? [[String: AnyObject]] else { return }
            let stats = sleep.compactMap({ FitbitSleepMdl(withDictionary: $0) })*/
            DispatchQueue.main.async {
                callback(activity, nil)
            }
        }
        dataTask.resume()
        return dataTask
    }
}
