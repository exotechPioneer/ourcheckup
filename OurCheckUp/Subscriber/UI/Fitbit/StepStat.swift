//
//  StepStat.swift
//  SampleBit
//
//  Created by Ryan LaSante on 9/14/16.
//  Copyright © 2016 Fitbit. All rights reserved.
//

import UIKit

struct StepStat {

    let day: DateComponents
	let steps: Float
	
	init?(withJSON json: String) {
		guard let jsonData = json.data(using: .utf8) else {
			return nil
		}
		self.init(withJSON: jsonData)
	}
	
	init?(withJSON jsonData: Data) {
		guard let data = (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? [String: Any] else {
			return nil
		}
		self.init(withDictionary: data)
	}
	
	init?(withDictionary data: [String: Any]) {
		guard let dateTime = data["dateTime"] as? String,
			let dateComponents = dateTime.dateComponents() else {
				return nil
		}
		guard let stepCount = (data["value"] as? NSNumber)?.floatValue ?? Float((data["value"] as? String) ?? "") else {
			return nil
		}
		day = dateComponents
		steps = stepCount
	}

}
