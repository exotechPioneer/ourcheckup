//
//  FitbitDistanceMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 25/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation

struct FitbitActivityDistanceMdl {
    
    var activitiesdistance : [FitbitActivitiesDistance]!
    var activitiesdistanceintraday : FitbitActivitiesIntraday!
    

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        activitiesdistance = [FitbitActivitiesDistance]()
        if let activitiesdistanceArray = dictionary["activities-distance"] as? [[String:Any]]{
            for dic in activitiesdistanceArray{
                let value = FitbitActivitiesDistance(fromDictionary: dic)
                self.activitiesdistance.append(value)
            }
        }
        if let activitiesdistanceintradayData = dictionary["activities-distance-intraday"] as? [String:Any]{
            activitiesdistanceintraday = FitbitActivitiesIntraday(fromDictionary: activitiesdistanceintradayData)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if activitiesdistance != nil{
            var dictionaryElements = [[String:Any]]()
            for activitiesdistanceElement in activitiesdistance {
                dictionaryElements.append(activitiesdistanceElement.toDictionary())
            }
            dictionary["activities-distance"] = dictionaryElements
        }
        if activitiesdistanceintraday != nil{
            dictionary["activities-distance-intraday"] = activitiesdistanceintraday?.toDictionary()
        }
        return dictionary
    }
    

    
    struct FitbitActivitiesDistance {
        var dateTime : String!
        var value : String!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            dateTime = dictionary["dateTime"] as? String
            value = dictionary["value"] as? String
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if dateTime != nil{
                dictionary["dateTime"] = dateTime
            }
            if value != nil{
                dictionary["value"] = value
            }
            return dictionary
        }
    }
    
   
}
