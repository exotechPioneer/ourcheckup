//
//  FitbitStepsRateMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 22/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct FitbitActivityStepsMdl  {
    var activitiessteps : [FitbitActivitiesSteps]!
    var activitiesstepsintraday : FitbitActivitiesIntraday!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        activitiessteps = [FitbitActivitiesSteps]()
        if let activitiesstepsArray = dictionary["activities-steps"] as? [[String:Any]]{
            for dic in activitiesstepsArray{
                let value = FitbitActivitiesSteps(fromDictionary: dic)
                activitiessteps.append(value)
            }
        }
        if let activitiesstepsintradayData = dictionary["activities-steps-intraday"] as? [String:Any]{
            activitiesstepsintraday = FitbitActivitiesIntraday(fromDictionary: activitiesstepsintradayData)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if activitiessteps != nil{
            var dictionaryElements = [[String:Any]]()
            for activitiesstepsElement in activitiessteps {
                dictionaryElements.append(activitiesstepsElement.toDictionary())
            }
            dictionary["activities-steps"] = dictionaryElements
        }
        if activitiesstepsintraday != nil{
            dictionary["activities-steps-intraday"] = activitiesstepsintraday.toDictionary()
        }
        return dictionary
    }
    struct FitbitActivitiesSteps{
        
        var dateTime : String!
        var value : String!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            dateTime = dictionary["dateTime"] as? String
            value = dictionary["value"] as? String
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if dateTime != nil{
                dictionary["dateTime"] = dateTime
            }
            if value != nil{
                dictionary["value"] = value
            }
            return dictionary
        }
        
    }
}
