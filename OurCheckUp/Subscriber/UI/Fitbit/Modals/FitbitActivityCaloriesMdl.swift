//
//  FitbitCaloriesMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 25/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation

struct FitbitActivityCaloriesMdl  {
    
    var activitiescalories : [FitbitActivitiesCalories]!
    var activitiescaloriesintraday : FitbitActivitiesIntraday!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        activitiescalories = [FitbitActivitiesCalories]()
        if let activitiescaloriesArray = dictionary["activities-calories"] as? [[String:Any]]{
            for dic in activitiescaloriesArray{
                let value = FitbitActivitiesCalories(fromDictionary: dic)
                activitiescalories.append(value)
            }
        }
        if let activitiescaloriesintradayData = dictionary["activities-calories-intraday"] as? [String:Any]{
            activitiescaloriesintraday = FitbitActivitiesIntraday(fromDictionary: activitiescaloriesintradayData)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if activitiescalories != nil{
            var dictionaryElements = [[String:Any]]()
            for activitiescaloriesElement in activitiescalories {
                dictionaryElements.append(activitiescaloriesElement.toDictionary())
            }
            dictionary["activities-calories"] = dictionaryElements
        }
        if activitiescaloriesintraday != nil{
            dictionary["activities-calories-intraday"] = activitiescaloriesintraday.toDictionary()
        }
        return dictionary
    }
    struct FitbitActivitiesCalories{
        
        var dateTime : String!
        var value : String!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            dateTime = dictionary["dateTime"] as? String
            value = dictionary["value"] as? String
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if dateTime != nil{
                dictionary["dateTime"] = dateTime
            }
            if value != nil{
                dictionary["value"] = value
            }
            return dictionary
        }
        
    }
}
