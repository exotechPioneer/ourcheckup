//
//  FitbitHeartRateMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 22/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct FitbitWeightMdl {
    
    let bmi : Float
    let date: DateComponents
    let time : String
    let weight : Float
    
    init?(withJSON json: String) {
        guard let jsonData = json.data(using: .utf8) else {
            return nil
        }
        self.init(withJSON: jsonData)
    }
    
    init?(withJSON jsonData: Data) {
        guard let data = (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? [String: Any] else {
            return nil
        }
        self.init(withDictionary: data)
    }
    
    init?(withDictionary data: [String: Any]) {
        guard let date = data["date"] as? String,
            let dateComponents = date.dateComponents() else {
                return nil
        }
        guard let bmi = (data["bmi"] as? Float) ?? Float((data["duration"] as? String) ?? "") else {
            return nil
        }
        guard let time = data["time"] as? String else {
                return nil
        }
        guard let weight = data["weight"] as? Float
        else {
                return nil
        }
        
        self.bmi = bmi
        self.date = dateComponents
        self.time = time
        self.weight = weight
    }
}
