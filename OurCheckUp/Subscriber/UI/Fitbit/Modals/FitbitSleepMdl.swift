//
//  FitbitHeartRateMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 22/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct FitbitSleepMdl {
    
    var sleep : [Sleep]!
    //var summary : FSummary!

    
    init(fromDictionary dictionary: [String:Any]){
        sleep = [Sleep]()
        if let sleepArray = dictionary["sleep"] as? [[String:Any]]{
            for dic in sleepArray{
                let value = Sleep(fromDictionary: dic)
                sleep.append(value)
            }
        }
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if sleep != nil{
            var dictionaryElements = [[String:Any]]()
            for sleepElement in sleep {
                dictionaryElements.append(sleepElement.toDictionary())
            }
            dictionary["sleep"] = dictionaryElements
        }
        
        return dictionary
    }

    
    struct Sleep {
        
    var dateOfSleep : String!
    var duration : Int!
    var efficiency : Int!
    var endTime : String!
    var infoCode : Int!
    var isMainSleep : Bool!
    //var levels : FLevel!
    var logId : Int!
    var minutesAfterWakeup : Int!
    var minutesAsleep : Int!
    var minutesAwake : Int!
    var minutesToFallAsleep : Int!
    var startTime : String!
    var timeInBed : Int!
    var type : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dateOfSleep = dictionary["dateOfSleep"] as? String
        duration = dictionary["duration"] as? Int
        efficiency = dictionary["efficiency"] as? Int
        endTime = dictionary["endTime"] as? String
        infoCode = dictionary["infoCode"] as? Int
        isMainSleep = dictionary["isMainSleep"] as? Bool
        //if let levelsData = dictionary["levels"] as? [String:Any]{
          //  levels = FLevel(fromDictionary: levelsData)
        //}
        logId = dictionary["logId"] as? Int
        minutesAfterWakeup = dictionary["minutesAfterWakeup"] as? Int
        minutesAsleep = dictionary["minutesAsleep"] as? Int
        minutesAwake = dictionary["minutesAwake"] as? Int
        minutesToFallAsleep = dictionary["minutesToFallAsleep"] as? Int
        startTime = dictionary["startTime"] as? String
        timeInBed = dictionary["timeInBed"] as? Int
        type = dictionary["type"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dateOfSleep != nil{
            dictionary["dateOfSleep"] = dateOfSleep
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if efficiency != nil{
            dictionary["efficiency"] = efficiency
        }
        if endTime != nil{
            dictionary["endTime"] = endTime
        }
        if infoCode != nil{
            dictionary["infoCode"] = infoCode
        }
        if isMainSleep != nil{
            dictionary["isMainSleep"] = isMainSleep
        }
       // if levels != nil{
         //   dictionary["levels"] = levels.toDictionary()
        //}
        if logId != nil{
            dictionary["logId"] = logId
        }
        if minutesAfterWakeup != nil{
            dictionary["minutesAfterWakeup"] = minutesAfterWakeup
        }
        if minutesAsleep != nil{
            dictionary["minutesAsleep"] = minutesAsleep
        }
        if minutesAwake != nil{
            dictionary["minutesAwake"] = minutesAwake
        }
        if minutesToFallAsleep != nil{
            dictionary["minutesToFallAsleep"] = minutesToFallAsleep
        }
        if startTime != nil{
            dictionary["startTime"] = startTime
        }
        if timeInBed != nil{
            dictionary["timeInBed"] = timeInBed
        }
        if type != nil{
            dictionary["type"] = type
        }
        return dictionary
    }
    
    }
  
}
