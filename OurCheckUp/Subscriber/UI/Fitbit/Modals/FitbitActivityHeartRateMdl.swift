//
//  FitbitHeartRateMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 22/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct FitbitActivityHeartRateMdl {
        var activitiesheart : [FitbitActivitiesHeart]!
        var activitiesheartintraday : FitbitActivitiesIntraday!
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            activitiesheart = [FitbitActivitiesHeart]()
            if let activitiesheartArray = dictionary["activities-heart"] as? [[String:Any]]{
                for dic in activitiesheartArray{
                    let value = FitbitActivitiesHeart(fromDictionary: dic)
                    activitiesheart.append(value)
                }
            }
            if let activitiesheartintradayData = dictionary["activities-heart-intraday"] as? [String:Any]{
                activitiesheartintraday = FitbitActivitiesIntraday(fromDictionary: activitiesheartintradayData)
            }
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            /*if activitiesheart != nil{
                var dictionaryElements = [[String:Any]]()
                for activitiesheartElement in activitiesheart {
                    dictionaryElements.append(activitiesheartElement.toDictionary())
                }
                dictionary["activities-heart"] = dictionaryElements
            }*/
            if activitiesheartintraday != nil{
                dictionary["activities-heart-intraday"] = activitiesheartintraday.toDictionary()
            }
            return dictionary
        }
    
    struct FitbitActivitiesHeart {
        
        var dateTime : String!
        var value : Value!
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            dateTime = dictionary["dateTime"] as? String
            value = Value(fromDictionary: dictionary["value"] as?  [String:Any])
        }
        
        struct Value {
            var restingHeartRate : Int!
            init(fromDictionary dictionary: [String:Any]?){
                restingHeartRate = dictionary?["restingHeartRate"] as? Int ?? 0
            }
        }
    }
    
    
}
struct FitbitActivitiesIntraday  {
    var dataset : [FitbitDataset]!
    var datasetInterval : Int!
    var datasetType : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dataset = [FitbitDataset]()
        if let datasetArray = dictionary["dataset"] as? [[String:Any]]{
            for dic in datasetArray{
                let value = FitbitDataset(fromDictionary: dic)
                dataset.append(value)
            }
        }
        datasetInterval = dictionary["datasetInterval"] as? Int
        datasetType = dictionary["datasetType"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dataset != nil{
            var dictionaryElements = [[String:Any]]()
            for datasetElement in dataset {
                dictionaryElements.append(datasetElement.toDictionary())
            }
            dictionary["dataset"] = dictionaryElements
        }
        if datasetInterval != nil{
            dictionary["datasetInterval"] = datasetInterval
        }
        if datasetType != nil{
            dictionary["datasetType"] = datasetType
        }
        return dictionary
    }
    
    
    
    struct FitbitDataset : Codable {
        
        var time : String!
        var value : Int!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            time = dictionary["time"] as? String
            value = (dictionary["value"] as? Int ?? Int(dictionary["value"] as? Double ?? Double(Int(dictionary["value"] as! Float))))
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if time != nil{
                dictionary["time"] = time
            }
            if value != nil{
                dictionary["value"] = value
            }
            return dictionary
        }
        
        
    }
}
