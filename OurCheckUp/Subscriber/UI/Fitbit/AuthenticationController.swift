//
//  AuthenticationController.swift
//  SampleBit
//
//  Created by Stephen Barnes on 9/14/16.
//  Copyright © 2016 Fitbit. All rights reserved.
//

import SafariServices
import Foundation
import WebKit
protocol AuthenticationProtocol {
	func authorizationDidFinish(_ success :Bool)
}

class AuthenticationController: UIViewController, SFSafariViewControllerDelegate {
	
    let clientID = "22D9QN"
	let clientSecret = "81abd4e9dd1309bf53bd9e220bd14172"
	let baseURL = URL(string: "https://www.fitbit.com/oauth2/authorize")
    static let redirectURI = "https://fourwitlogics.com/"
	let defaultScope = "sleep+settings+nutrition+activity+social+heartrate+profile+weight+location"

	var authorizationVC: SFSafariViewController?
	var delegate: AuthenticationProtocol?
	var authenticationToken: String?
    
    //@IBOutlet weak var wkWebView:WKWebView!
    @IBOutlet weak var webView:UIWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var isAuthDone = false

	/*init(delegate: AuthenticationProtocol?) {
		self.delegate = delegate
		
		NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: NotificationConstants.launchNotification), object: nil, queue: nil, using: { [weak self] (notification: Notification) in
			// Parse and extract token
			let success: Bool
			if let token = AuthenticationController.extractToken(notification, key: "#access_token") {
				self?.authenticationToken = token
				NSLog("You have successfully authorized")
				success = true
			} else {
				print("There was an error extracting the access token from the authentication response.")
				success = false
			}
            self?.authorizationVC?.dismiss(animated: true, completion: {
				self?.delegate?.authorizationDidFinish(success)
			})
		})
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}
 */

    override func viewDidLoad() {
        isAuthDone = false
        login(fromParentViewController: self)
    }
    
	// MARK: Public API

	public func login(fromParentViewController viewController: UIViewController) {
		guard let url = URL(string: "https://www.fitbit.com/oauth2/authorize?response_type=token&client_id="+clientID+"&redirect_uri="+AuthenticationController.redirectURI+"&scope="+defaultScope+"&expires_in=604800") else {
			NSLog("Unable to create authentication URL")
			return
		}

		/*let authorizationViewController = SFSafariViewController(url: url)
		authorizationViewController.delegate = self
		authorizationVC = authorizationViewController
		viewController.present(authorizationViewController, animated: true, completion: nil)*/
        
        let myRequest = URLRequest(url: url)
        //self.wkWebView.uiDelegate = self
        //self.wkWebView.navigationDelegate = self
        //self.wkWebView.load(myRequest)
        self.webView.delegate = self
        self.webView.loadRequest(myRequest)
	}

	public static func logout() {
		//
	}

	private static func extractToken(_ notification: Notification, key: String) -> String? {
		guard let url = notification.userInfo?[UIApplicationLaunchOptionsKey.url] as? URL else {
			NSLog("notification did not contain launch options key with URL")
			return nil
		}
		// Extract the access token from the URL
		let strippedURL = url.absoluteString.replacingOccurrences(of: AuthenticationController.redirectURI, with: "")
		return self.parametersFromQueryString(strippedURL)[key]
	}

     static func extractToken(_ string: String, key: String) -> String? {
        
        // Extract the access token from the URL
        let strippedURL = string.replacingOccurrences(of: AuthenticationController.redirectURI, with: "")
        return self.parametersFromQueryString(strippedURL)[key]
    }
    
    
	// TODO: this method is horrible and could be an extension and use some functional programming
	 static func parametersFromQueryString(_ queryString: String?) -> [String: String] {
		var parameters = [String: String]()
		if (queryString != nil) {
			let parameterScanner: Scanner = Scanner(string: queryString!)
			var name:NSString? = nil
			var value:NSString? = nil
			while (parameterScanner.isAtEnd != true) {
				name = nil;
				parameterScanner.scanUpTo("=", into: &name)
				parameterScanner.scanString("=", into:nil)
				value = nil
				parameterScanner.scanUpTo("&", into:&value)
				parameterScanner.scanString("&", into:nil)
				if (name != nil && value != nil) {
					parameters[name!.removingPercentEncoding!]
						= value!.removingPercentEncoding!
				}
			}
		}
		return parameters
	}

	// MARK: SFSafariViewControllerDelegate
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
		delegate?.authorizationDidFinish(false)
	}
    func safariViewController(_ controller: SFSafariViewController, initialLoadDidRedirectTo URL: URL) {
        print("MMMMMMMMMMM")
       
    }
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        print("NNNNNNNNNNN")
    }
}

extension AuthenticationController : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("Start loading")
        self.indicator.startAnimating()
        self.indicator.isHidden = false
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Finish loading")
        self.indicator.stopAnimating()
        self.indicator.isHidden = true
        self.processAccessToken(url: webView.request?.url)
    }
    
    func processAccessToken(url:URL?){
        if let URL = url{
            if !isAuthDone{
                if URL.absoluteString.contains(AuthenticationController.redirectURI){
                    let success: Bool
                    if let token = AuthenticationController.extractToken(URL.absoluteString, key: "#access_token") {
                        self.authenticationToken = token
                        NSLog("You have successfully authorized")
                        success = true
                    } else {
                        print("There was an error extracting the access token from the authentication response.")
                        success = false
                    }
                    self.authorizationVC?.dismiss(animated: true, completion: {
                        self.delegate?.authorizationDidFinish(success)
                    })
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.authorizationDidFinish(success)
                    isAuthDone = true
                }
            }
        }
    }
}
extension AuthenticationController : WKUIDelegate,WKNavigationDelegate{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start loading")
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        self.processAccessToken(url: webView.url)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finish loading")
        self.indicator.stopAnimating()
        self.indicator.isHidden = true
       self.processAccessToken(url: webView.url)
    }
}
