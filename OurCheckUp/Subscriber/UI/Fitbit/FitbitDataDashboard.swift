//
//  DevicesDataDashboard.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 02/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class FitbitDataDashboard: BaseVC {
    
    @IBOutlet weak var viewBodyMeasurement: UIViewX!
    @IBOutlet weak var viewBloodPresure: UIViewX!
    @IBOutlet weak var viewSleepPattern: UIViewX!
    @IBOutlet weak var viewHeartRate: UIViewX!
    @IBOutlet weak var viewSPO2: UIViewX!
    @IBOutlet weak var viewActivity: UIViewX!
    @IBOutlet weak var btnConnect: UIButtonX!
    @IBOutlet weak var lblConnectedDeviceName: UILabelX!
    
    @IBOutlet weak var lblBodyWeightVal: UILabel!
    @IBOutlet weak var lblBloodPressureVal: UILabel!
    @IBOutlet weak var lblSleepPatternVal: UILabel!
    @IBOutlet weak var lblHeartRateVal: UILabel!
    @IBOutlet weak var lblSPO2Val: UILabel!
    @IBOutlet weak var lblCaloriesVal: UILabel!
    @IBOutlet weak var lblStepsVal: UILabel!
    
    let TEXT_DISCONNECT_DEVICE = "Disconnect Device"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTapActions()
        setUpNavLogo()
        setAllDataValues()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onDeviceDataChanged), name: NSNotification.Name("DeviceDataChanged"), object: nil)
    }
    @objc func onDeviceDataChanged(){
        setAllDataValues()
    }
    override func viewWillAppear(_ animated: Bool) {
        if let device = DevicesConnectionHelper.share.peripheral{
            self.btnConnect.setTitle(TEXT_DISCONNECT_DEVICE, for: .normal)
            self.lblConnectedDeviceName.text = device.name
        }else{
            self.btnConnect.setTitle("Connect", for: .normal)
        }
    }
    
    func setAllDataValues(){
        let pref = AppPreferences.share
        let bodyWeight:Float = pref.get(forkey: .bodyWeight) ?? 0
        let bp : String =  pref.get(forkey: .bp) ?? "0/0"
        let sleepH = pref.get(forkey: .sleepHours) ?? 0
        let sleepM = pref.get(forkey: .sleepMins) ?? 0
        let heartRate:Int = pref.get(forkey: .heartRate) ?? 0
        let spo2:Int = pref.get(forkey: .spo2) ?? 0
        let calories:Int  = pref.get(forkey: .calories) ?? 0
        let steps : Int = pref.get(forkey: .steps) ?? 0
        
        self.lblBodyWeightVal.text = "\(bodyWeight)"
        self.lblBloodPressureVal.text = bp
        self.lblSleepPatternVal.text = "\(sleepH):\(sleepM) hrs"
        self.lblHeartRateVal.text = "\(heartRate)"
        self.lblSPO2Val.text = "\(spo2) %"
        self.lblCaloriesVal.text = "\(calories) kcal"
        self.lblStepsVal.text = "\(steps) steps"
    }
    
    func addTapActions(){
        let tap :OnTap = {(view) in
            self.performSegue(id: .tableGraphDetail,sender:view)
        }
        self.viewBodyMeasurement.addTapAction(completion: tap)
        self.viewBloodPresure.addTapAction(completion: tap)
        self.viewSleepPattern.addTapAction(completion: tap)
        self.viewHeartRate.addTapAction(completion: tap)
        self.viewSPO2.addTapAction(completion: tap)
        self.viewActivity.addTapAction(completion: tap)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let view = sender as? UIViewX{
            let destVC = segue.destination as! SubscriberTableGraphVC
            switch view.tag{
            case 0://Bodyweight
                destVC.reportType = .bodyWeight
                destVC.reportName = "BODY MEASUREMENT"
            case 1://BP
                 destVC.reportType = .bloodPressure
                destVC.reportName = "BLOOD PRESSURE"
            case 2://Sleep pattern
                 destVC.reportType = .sleepPattern
                destVC.reportName = "SLEEP PATTERN"
            case 3://Heart Rate
                 destVC.reportType = .heartRate
                destVC.reportName = "HEART RATE"
            case 4: //spo2
                 destVC.reportType = .spo2
                destVC.reportName = "SPO2"
            case 5://activity
                 destVC.reportType = .activity
                destVC.reportName = "ACTIVITY"
            default:
                break
            }
            destVC.tintColor = view.backgroundColor
        }
    }
    
    
}
