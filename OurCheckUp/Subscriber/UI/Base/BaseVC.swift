//
//  BaseVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 01/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit
import MBProgressHUD
class BaseVC: UIViewController {

    private var progress: MBProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
    }
    
    func updateUserInterface() {
        guard let status = Network.reachability?.status else { return }
        switch status {
        case .unreachable:
            view.backgroundColor = .red
        case .wifi:
            view.backgroundColor = .green
        case .wwan:
            view.backgroundColor = .yellow
        }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
    }
    
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    func performSegue(id:SubscriberSegueIds,sender:Any? = nil){
        self.performSegue(withIdentifier: id.rawValue, sender: sender)
    }
    
    func setTitle(title:String){
        self.title = title
    }
    
    func addSideMenuButton(){
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        btn.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        btn.addTarget(self, action: #selector(didMenuBtnTapped(_:)), for: .touchUpInside)
        let barBtn = UIBarButtonItem(customView: btn)
        navigationController?.navigationItem.leftBarButtonItem = barBtn
    }
    
    @objc func didMenuBtnTapped(_ sender : UIButton){
        
    }
    
    func setUpNavLogo() {
        let logoImage = #imageLiteral(resourceName: "ourcheckup_logo")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x:0,y: 0,width: 180,height: 70)
        let myTitleView = CustomTitleView(frame: logoImageView.frame)
        logoImageView.contentMode = .scaleAspectFit
        myTitleView.addSubview(logoImageView)
        self.navigationItem.titleView = myTitleView
        //let imageItem = UIBarButtonItem.init(customView: logoImageView)
        //let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        //negativeSpacer.width = -45
       // navigationItem.leftBarButtonItems = [negativeSpacer,imageItem]
    }
    
    
    
  /*  func setUpNavLogoRight() {
        let logoImage = #imageLiteral(resourceName: "ourcheckup_logo")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x:40,y: 0,width: 150,height: 50)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -45
        navigationItem.rightBarButtonItems = [negativeSpacer,imageItem]
    }
    */
    
    func showProgress(){
        DispatchQueue.main.async {
            self.progress = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.progress?.mode = MBProgressHUDMode.indeterminate
        }
    }
    
    func hideProgress(){
        DispatchQueue.main.async {
            self.progress?.hide(animated: true)
            self.progress = nil
        }
    }
    
    func showSubcriberIdAlert(completion:@escaping (Int)->Void){
        let alert = UIAlertController(title: "", message: "Please enter subscriber id", preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "Submit", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            if let text = textField.text{
                if text.isEmpty{
                    showError(error: "Please enter subscriber id.")
                }else{
                    completion(Int(text)!)
                }
            }else{
                showError(error: "Please enter subscriber id.")
            }
        }
        alert.addTextField { (textField) in
            textField.placeholder = "Subscriber id"
            textField.keyboardType = .numberPad
        }
        alert.addAction(action)
        self.present(alert, animated:true, completion: nil)
    }
    
    func getAgeFromBirthDate(birthDate:Date)->Int{
        let now = Date()
        let birthday: Date = birthDate
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year!
        return age
    }
    
    func getUserID()->Int{
        return AppPreferences.share.get(forkey: .userID) ?? 0
    }
}


class CustomTitleView: UIView {
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
}
