//
//  SubscriberSegueIds.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 02/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation

enum SubscriberSegueIds : String{
    case signup = "signup"
    case healthData = "healthdata"
    case healthReport = "healthreport"
    case medicalSummary = "medicalsummary"
    case tableGraphDetail = "detail"
    case dashboardSubsriber = "subsdash"
    case privacypolicy = "privacypolicy"
    case devices = "devices"
    case doctor = "doctor"
    case fitbitAuth = "fitbitAuth"
    case reportDetail = "reportDetail"
    case initialReportDetail = "initialReportDetail"
    case subscriberProfile = "subscriberProfile"
    //Doctor Segue
    case initialReport = "initialreport"
    case drQuaterlyReport = "drQuaterlyReport"
    case drProgressReport = "drProgressReport"
    case drMedicalSummary = "drMedicalSummary"
    case drPayment = "drPayment"
    case drPrivacyPolicies = "drPrivacyPolicies"
    case drEmpanelment = "drEmpanelment"
    case doctorProfile = "doctorProfile"
    
}
