//
//  YearPickerVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 12/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit

protocol YearPickerVCDelegate {
    func yearPicker(didSelect year : String)
    func yearPicker(didCancel cancel : Bool)
}

class YearPickerVC: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var lblSelectYear:UILabel!
    @IBOutlet weak var btnCancel:UIButton!
    @IBOutlet weak var btnSelect:UIButton!
    
    var yearArray : [String]?
    
    var delegate : YearPickerVCDelegate?
    
    var selectedYear:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        yearArray = [String]()
        let currentYear = Date().year()
        for i in 0..<10{
            let year = currentYear - i
            yearArray?.append("\(year)")
        }
        
        btnSelect.isEnabled = false
        btnSelect.alpha = 0.7
    }
    
    @IBAction func btnSelectAction(sender:UIButton){
        if let year = selectedYear{
            delegate?.yearPicker(didSelect: year)
            btnCancelAction(sender: sender)
        }
    }
    
    @IBAction func btnCancelAction(sender:UIButton){
        delegate?.yearPicker(didCancel: true)
        self.dismiss(animated: true)
    }
}

extension YearPickerVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arr = yearArray{
            return arr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.textLabel?.text = yearArray?[indexPath.row] ?? ""
        
        return cell
    }
    
    
}

extension YearPickerVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        btnSelect.isEnabled = true
        btnSelect.alpha = 1.0
        
        selectedYear = yearArray?[indexPath.row]
    }
}
