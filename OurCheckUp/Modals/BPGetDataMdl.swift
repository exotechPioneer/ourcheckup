//
//  DeviceHealthDataMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 26/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct BPGetDataMdl{
    var data : [Data]!
    var status : Bool!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [Data]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = Data(fromDictionary: dic)
                data.append(value)
            }
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    struct Data{
        var dia : String!
        var pul : String!
        var sys : String!
        var maxDia : String!
        var maxPul : String!
        var maxSys : String!
        var minDia : String!
        var minPul : String!
        var minSys : String!
        var spo2 : String!
        var heart_rate : String!
        var max_heart_rate : String!
        var min_heart_rate : String!
        var steps : String!
        var max_steps : String!
        var min_steps : String!
        var calories_burt : String!
        var max_calories_burt : String!
        var min_calories_burt : String!
        var sleep_minutes : String!
        var max_sleep_minutes : String!
        var min_sleep_minutes : String!
        var sleep_hours : String!
        var max_sleep_hours : String!
        var min_sleep_hours : String!
        var body_weight : String!
        var date : String?
        var day : String?
        var week : String?
        var month : String?
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            dia = dictionary["dia"] as? String
            pul = dictionary["pul"] as? String
            sys = dictionary["sys"] as? String
            maxDia = dictionary["max_dia"] as? String
            maxPul = dictionary["max_pul"] as? String
            maxSys = dictionary["max_sys"] as? String
            minDia = dictionary["min_dia"] as? String
            minPul = dictionary["min_pul"] as? String
            minSys = dictionary["min_sys"] as? String
            
            spo2 = dictionary["spo2"] as? String
            max_heart_rate = dictionary["max_heart_rate"] as? String
            min_heart_rate = dictionary["min_heart_rate"] as? String
            heart_rate = dictionary["heart_rate"] as? String
            steps = dictionary["steps"] as? String
            max_steps = dictionary["max_steps"] as? String
            min_steps = dictionary["min_steps"] as? String
            calories_burt = dictionary["calories_burt"] as? String
            max_calories_burt = dictionary["max_calories_burt"] as? String
            min_calories_burt = dictionary["min_calories_burt"] as? String
            sleep_minutes = dictionary["sleep_minutes"] as? String
            sleep_hours = dictionary["sleep_hours"] as? String
            body_weight = dictionary["body_weight"] as? String
            day = dictionary["day"] as? String
            date = dictionary["date"] as? String
            week = dictionary["week"] as? String
            month = dictionary["month"] as? String
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if dia != nil{
                dictionary["dia"] = dia
            }
            if pul != nil{
                dictionary["pul"] = pul
            }
            if sys != nil{
                dictionary["sys"] = sys
            }
            if day != nil{
                dictionary["day"] = day
            }
            if week != nil{
                dictionary["week"] = week
            }
            if month != nil{
                dictionary["month"] = month
            }
            return dictionary
        }
    }
}
