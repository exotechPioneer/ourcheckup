//
//  SmartDataPostModel.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 13/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation


struct SmartBodyAnalyzerDataPostModel  {
    
    let data : Data?
    let deviceId : String?
    
    
    struct Data  {
        
        let bmi : Int?
        let bmr : Float?
        let bodyWeight : Float?
        let bodyage : Float?
        let fat : Float?
        let moisture : Float?
        let muscle : Float?
        let skeleton : Int?
        let unitType : Int?
        let useId : Int?
        let visceralFat : Float?
        let date : String?
        
        
    }
    func toDictionay()->[String:Any]{
        var mainDic = [String:Any]()
        mainDic["device_id"] = deviceId!
        if let  d = self.data{
            mainDic["bmi"] = d.bmi!
            mainDic["bmr"] = d.bmr!
            mainDic["body_weight"] = d.bodyWeight!
            mainDic["bodyage"] = d.bodyage!
            mainDic["fat"] = d.fat!
            mainDic["moisture"] = d.moisture!
            mainDic["muscle"] = d.muscle!
            mainDic["skeleton"] = d.skeleton!
            mainDic["unitType"] = d.unitType!
            mainDic["visceral_fat"] = d.visceralFat!
            mainDic["user_id"] = d.useId!
            mainDic["date"] = d.date!
        }
        
        return mainDic
    }
}
