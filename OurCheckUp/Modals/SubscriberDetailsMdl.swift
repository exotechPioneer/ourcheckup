//
//  SubscriberDetailsMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 17/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct SubscriberDetailsMdl{
    
    var data : [Data]!
    var status : Bool!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [Data]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = Data(fromDictionary: dic)
                data.append(value)
            }
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    struct Data{
        
        var address : String!
        var address2 : String!
        var country : String!
        var dob : String!
        var emailUserId : String!
        var faxNo : String!
        var firstName : String!
        var gender : String!
        var lastName : String!
        var mobileNo : String!
        var nationalId : String!
        var phoneNo : String!
        var postcode : String!
        var state : String!
        var userId : Int!
        var height : AnyObject!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            address = dictionary["address"] as? String
            address2 = dictionary["address_2"] as? String
            country = dictionary["country"] as? String
            dob = dictionary["dob"] as? String
            emailUserId = dictionary["email_user_id"] as? String
            faxNo = dictionary["fax_no"] as? String
            firstName = dictionary["first_name"] as? String
            gender = dictionary["gender"] as? String
            lastName = dictionary["last_name"] as? String
            mobileNo = dictionary["mobile_no"] as? String
            nationalId = dictionary["national_id"] as? String
            phoneNo = dictionary["phone_no"] as? String
            postcode = dictionary["postcode"] as? String
            state = dictionary["state"] as? String
            userId = dictionary["user_id"] as? Int
            height = dictionary["user_id"] as? AnyObject
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if address != nil{
                dictionary["address"] = address
            }
            if address2 != nil{
                dictionary["address_2"] = address2
            }
            if country != nil{
                dictionary["country"] = country
            }
            if dob != nil{
                dictionary["dob"] = dob
            }
            if emailUserId != nil{
                dictionary["email_user_id"] = emailUserId
            }
            if faxNo != nil{
                dictionary["fax_no"] = faxNo
            }
            if firstName != nil{
                dictionary["first_name"] = firstName
            }
            if gender != nil{
                dictionary["gender"] = gender
            }
            if lastName != nil{
                dictionary["last_name"] = lastName
            }
            if mobileNo != nil{
                dictionary["mobile_no"] = mobileNo
            }
            if nationalId != nil{
                dictionary["national_id"] = nationalId
            }
            if phoneNo != nil{
                dictionary["phone_no"] = phoneNo
            }
            if postcode != nil{
                dictionary["postcode"] = postcode
            }
            if state != nil{
                dictionary["state"] = state
            }
            if userId != nil{
                dictionary["user_id"] = userId
            }
            return dictionary
        }
        
    }
}
