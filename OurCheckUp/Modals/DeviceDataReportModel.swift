//
//  DeviceDataReportModel.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 13/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation

struct DeviceDataReport : Codable {
    
    let cleverTitle : String?
    let content : OC_Content?
    
    
    enum CodingKeys: String, CodingKey {
        case cleverTitle = "cleverTitle"
        case content
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cleverTitle = try values.decodeIfPresent(String.self, forKey: .cleverTitle)
        content = try values.decodeIfPresent(OC_Content.self, forKey: .content)
    }
}

struct OC_Content : Codable {
    
    let chartId : String?
    let elements : [OC_Element?]?
    let isDecimalSeparatorComma : Int?
    let isThousandSeparatorDisabled : Int?
    //let options : Option?
    //let tooltip : Tooltip?
    //let xAxis : XAxi?
    //let yAxis : YAxi?
    
    
    enum CodingKeys: String, CodingKey {
        case chartId = "chartId"
        case elements = "elements"
        case isDecimalSeparatorComma = "is_decimal_separator_comma"
        case isThousandSeparatorDisabled = "is_thousand_separator_disabled"
        //case options
        //case tooltip
        //case xAxis
        //case yAxis
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        chartId = try values.decodeIfPresent(String.self, forKey: .chartId)
        elements = try values.decodeIfPresent([OC_Element].self, forKey: .elements)
        isDecimalSeparatorComma = try values.decodeIfPresent(Int.self, forKey: .isDecimalSeparatorComma)
        isThousandSeparatorDisabled = try values.decodeIfPresent(Int.self, forKey: .isThousandSeparatorDisabled)
        //options = try Option(from: decoder)
        //tooltip = try Tooltip(from: decoder)
        //xAxis = try XAxi(from: decoder)
        //yAxis = try YAxi(from: decoder)
    }
    
    
}


struct OC_Element : Codable {
    
    let colour : String?
    let values : [OC_Value?]?
    
    
    enum CodingKeys: String, CodingKey {
        case colour = "colour"
        case values = "values"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        colour = try values.decodeIfPresent(String.self, forKey: .colour)
        self.values = try values.decodeIfPresent([OC_Value].self, forKey: .values)
    }
    
    
}

struct OC_Value : Codable {
    
    let context : String?
    let label : String?
    let tip : String?
    let value : Double?
    var value2 : Double?
    var value3 : Double?
    
    
    enum CodingKeys: String, CodingKey {
        case context = "context"
        case label = "label"
        case tip = "tip"
        case value = "value"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        context = try values.decodeIfPresent(String.self, forKey: .context)
        label = try values.decodeIfPresent(String.self, forKey: .label)
        tip = try values.decodeIfPresent(String.self, forKey: .tip)
        value = try values.decodeIfPresent(Double.self, forKey: .value)
        value2 = 0
        value3 = 0
    }
    
    
}

