//
//  MyHealthReportMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation

struct MyHealthReportMdl{
    
    var reportType:String
    var reports:[Report]?
    var isSubViewHidden : Bool
    
}

struct Report {
    var name:String
    var id:String
}
