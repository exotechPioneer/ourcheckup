//
//  CVDRiskMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 04/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
import Foundation

struct CVDRiskMdl{
    
    var data : [Data]!
    var status : Bool!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [Data]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = Data(fromDictionary: dic)
                data.append(value)
            }
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    struct Data{
        
        var lifeTimeCvd : Int!
        var lifeTimeCvdOptimal : Int!
        var tenYearsCvd : Int!
        var tenYearsCvdOptimal : Int!
        var userId : Int!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            lifeTimeCvd = dictionary["life_time_cvd"] as? Int
            lifeTimeCvdOptimal = dictionary["life_time_cvd_optimal"] as? Int
            tenYearsCvd = dictionary["ten_years_cvd"] as? Int
            tenYearsCvdOptimal = dictionary["ten_years_cvd_optimal"] as? Int
            userId = dictionary["user_id"] as? Int
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if lifeTimeCvd != nil{
                dictionary["life_time_cvd"] = lifeTimeCvd
            }
            if lifeTimeCvdOptimal != nil{
                dictionary["life_time_cvd_optimal"] = lifeTimeCvdOptimal
            }
            if tenYearsCvd != nil{
                dictionary["ten_years_cvd"] = tenYearsCvd
            }
            if tenYearsCvdOptimal != nil{
                dictionary["ten_years_cvd_optimal"] = tenYearsCvdOptimal
            }
            if userId != nil{
                dictionary["user_id"] = userId
            }
            return dictionary
        }
        
    }
}
