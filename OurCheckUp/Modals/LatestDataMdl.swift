//
//  LatestDataMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 31/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation

struct LatestDataMdl{
    
    var data : Data!
    var status : Bool!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let dataData = dictionary["data"] as? [String:Any]{
            data = Data(fromDictionary: dataData)
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            dictionary["data"] = data.toDictionary()
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    struct Data{
        
        var bodyWeight : String!
        var caloriesBurt : String!
        var dia : String!
        var heartRate : String!
        var sleepHours : String!
        var sleepMinutes : String!
        var steps : String!
        var sys : String!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            bodyWeight = dictionary["body_weight"] as? String
            caloriesBurt = dictionary["calories_burt"] as? String
            dia = dictionary["dia"] as? String
            heartRate = dictionary["heart_rate"] as? String
            sleepHours = dictionary["sleep_hours"] as? String
            sleepMinutes = dictionary["sleep_minutes"] as? String
            steps = dictionary["steps"] as? String
            sys = dictionary["sys"] as? String
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if bodyWeight != nil{
                dictionary["body_weight"] = bodyWeight
            }
            if caloriesBurt != nil{
                dictionary["calories_burt"] = caloriesBurt
            }
            if dia != nil{
                dictionary["dia"] = dia
            }
            if heartRate != nil{
                dictionary["heart_rate"] = heartRate
            }
            if sleepHours != nil{
                dictionary["sleep_hours"] = sleepHours
            }
            if sleepMinutes != nil{
                dictionary["sleep_minutes"] = sleepMinutes
            }
            if steps != nil{
                dictionary["steps"] = steps
            }
            if sys != nil{
                dictionary["sys"] = sys
            }
            return dictionary
        }
        
    }
}
