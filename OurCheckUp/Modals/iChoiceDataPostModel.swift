//
//  iChoiceDataPostModel.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 13/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation
struct iChoiceDataPostModel  {
    
    let data : Data?
    let deviceId : String?
    
    struct Data  {
        let date : String?
        let dia : Int?
        let pul : Int?
        let sys : Int?
        let unitType : Int?
        let useId : Int?
    }
    
    func toDictionay()->[String:Any]{
        var mainDic = [String:Any]()
        mainDic["device_id"] = deviceId!
        if let d = self.data{
            mainDic["date"] = d.date!
            mainDic["dia"] = d.dia!
            mainDic["pul"] = d.pul!
            mainDic["sys"] = d.sys!
            mainDic["unitType"] = d.unitType!
            mainDic["user_id"] = d.useId!
            
        }
        return mainDic
    }
    
}
