//
//  SmartBandDataPostModel.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 13/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation


struct SmartBandDataPostModel  {
    
    let data : Data?
    let deviceId : String?
    
    struct Data  {
        
        let bloodOxygen : Int?
        let bloodOxygenRealTime : Int?
        let bloodPressureDown : Int?
        let bloodPressureUp : Int?
        let buttonTest : String?
        let caloriesBurt : Int?
        let chargingEleQty : Int?
        let date : String?
        let deepSleep : Int?
        let ecg : String?
        let fall : String?
        let heartRate : Int?
        let heartRateRealTime : Int?
        let heartRateSensor : String?
        let hourlyMeasure : Int?
        let rssi : Int?
        let shallowSleep : Int?
        let sleepHours : Int?
        let sleepMinutes : Int?
        let spo2 : Int?
        let steps : Int?
        let triaxialsensor : String?
        let unchargedEleQty : Int?
        let unitType : Int?
        let useId : Int?
        let vesrionCode : String?
        
        
        
        
    }
    
    func toDictionay()->[String:Any]{
        var mainDic = [String:Any]()
        mainDic["device_id"] = deviceId!
       
        if let d = self.data{
            mainDic["date"] = d.date
            mainDic["blood_oxygen"] = d.bloodOxygen
            mainDic["blood_oxygen_real_time"] = d.bloodOxygenRealTime
            mainDic["blood_pressure_down"] = d.bloodPressureDown
            mainDic["blood_pressure_up"] = d.bloodPressureUp
            mainDic["button_test"] = d.buttonTest
            mainDic["calories_burt"] = d.caloriesBurt
            mainDic["charging_ele_qty"] = d.caloriesBurt
            mainDic["deep_sleep"] = d.deepSleep
            mainDic["ecg"] = d.ecg
            mainDic["fall"] = d.fall
            mainDic["heart_rate"] = d.heartRate
            mainDic["heart_rate_real_time"] = d.heartRateRealTime
            mainDic["heart_rate_sensor"] = d.heartRateSensor
            mainDic["hourly_measure"] = d.hourlyMeasure
            mainDic["rssi"] = d.rssi
            mainDic["shallow_sleep"] = d.shallowSleep
            mainDic["sleep_hours"] = d.sleepHours
            mainDic["sleep_minutes"] = d.sleepMinutes
            mainDic["spo2"] = d.spo2
            mainDic["steps"] = d.steps
            mainDic["triaxialsensor"] = d.triaxialsensor
            mainDic["uncharged_ele_qty"] = d.unchargedEleQty
            mainDic["vesrion_code"] = d.vesrionCode
            mainDic["unitType"] = d.unitType
            mainDic["user_id"] = d.useId
            
        }
       
        return mainDic
    }
}
