//
//  SubscriberMedicalSummaryMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 28/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation

struct SubscriberMedicalSummaryMdl{
    
    var header:String?
    var childs : [Child]?
    var isYes : Bool?
    var apiParamName:String?
    
    init(header:String,childs:[Child]) {
        self.header = header
        self.childs = childs
    }
    
    struct Child {
        var name : String
        var time: String
        var isChecked:Bool = false
        var level : Int = 0
        var needTextInput = false
        var inputText: String?
        var isSubChilds = false
        var parentNodeID : Int = 0
        var apiParamName : String?
        var nodeId:Int = 0
        
        init(name:String,time:String) {
            self.name = name
            self.time = time
        }
        init(name:String,time:String,level:Int) {
            self.level = level
            self.name = name
            self.time = time
        }
    }
    
    var data : [Data]!
    var status : Bool!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [Data]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = Data(fromDictionary: dic)
                data.append(value)
            }
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    struct Data{
        
        var age : Int!
        var createdOn : Int!
        var currentMedication : String!
        var dob : String!
        var drugAllergies : String!
        var gender : String!
        var idNo : Int!
        var mrn : Int!
        var name : String!
        var pastSurgicalHistory : String!
        var significantMedicalHistory : String!
        var userId : Int!
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            age = dictionary["age"] as? Int
            createdOn = dictionary["created_on"] as? Int
            currentMedication = dictionary["current_medication"] as? String
            dob = dictionary["dob"] as? String
            drugAllergies = dictionary["drug_allergies"] as? String
            gender = dictionary["gender"] as? String
            idNo = dictionary["id_no"] as? Int
            mrn = dictionary["mrn"] as? Int
            name = dictionary["name"] as? String
            pastSurgicalHistory = dictionary["past_surgical_history"] as? String
            significantMedicalHistory = dictionary["significant_medical_history"] as? String
            userId = dictionary["user_id"] as? Int
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if age != nil{
                dictionary["age"] = age
            }
            if createdOn != nil{
                dictionary["created_on"] = createdOn
            }
            if currentMedication != nil{
                dictionary["current_medication"] = currentMedication
            }
            if dob != nil{
                dictionary["dob"] = dob
            }
            if drugAllergies != nil{
                dictionary["drug_allergies"] = drugAllergies
            }
            if gender != nil{
                dictionary["gender"] = gender
            }
            if idNo != nil{
                dictionary["id_no"] = idNo
            }
            if mrn != nil{
                dictionary["mrn"] = mrn
            }
            if name != nil{
                dictionary["name"] = name
            }
            if pastSurgicalHistory != nil{
                dictionary["past_surgical_history"] = pastSurgicalHistory
            }
            if significantMedicalHistory != nil{
                dictionary["significant_medical_history"] = significantMedicalHistory
            }
            if userId != nil{
                dictionary["user_id"] = userId
            }
            return dictionary
        }
    }
}
