//
//  User.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 12/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import Foundation
struct User : Codable {
    
    let active : Bool?
    let domainId : String?
    let email : String?
    let firstLogin : String?
    let firstName : String?
    let groups : [String]?
    let id : String?
    let info : String?
    let lastLogin : String?
    let lastName : String?
    let locale : String?
    let name : String?
    let parameters : [String]?
    let phone : String?
    let roles : [Role]?
    let surname : String?
    let timeZone : String?
    let username : String?
    
    
    enum CodingKeys: String, CodingKey {
        case active = "active"
        case domainId = "domain_id"
        case email = "email"
        case firstLogin = "firstLogin"
        case firstName = "firstName"
        case groups = "groups"
        case id = "id"
        case info = "info"
        case lastLogin = "lastLogin"
        case lastName = "lastName"
        case locale = "locale"
        case name = "name"
        case parameters = "parameters"
        case phone = "phone"
        case roles = "roles"
        case surname = "surname"
        case timeZone = "timeZone"
        case username = "username"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        active = try values.decodeIfPresent(Bool.self, forKey: .active)
        domainId = try values.decodeIfPresent(String.self, forKey: .domainId)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        firstLogin = try values.decodeIfPresent(String.self, forKey: .firstLogin)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        groups = try values.decodeIfPresent([String].self, forKey: .groups)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        info = try values.decodeIfPresent(String.self, forKey: .info)
        lastLogin = try values.decodeIfPresent(String.self, forKey: .lastLogin)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        locale = try values.decodeIfPresent(String.self, forKey: .locale)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        parameters = try values.decodeIfPresent([String].self, forKey: .parameters)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        roles = try values.decodeIfPresent([Role].self, forKey: .roles)
        surname = try values.decodeIfPresent(String.self, forKey: .surname)
        timeZone = try values.decodeIfPresent(String.self, forKey: .timeZone)
        username = try values.decodeIfPresent(String.self, forKey: .username)
    }
    
    
}


struct Role : Codable {
    
    let role : String?
    
    
    enum CodingKeys: String, CodingKey {
        case role = "role"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        role = try values.decodeIfPresent(String.self, forKey: .role)
    }
    
    
}
