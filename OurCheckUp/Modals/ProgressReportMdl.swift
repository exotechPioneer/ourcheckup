//
//  ProgressReportMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 06/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct ProgressReportMdl{
    
    var data : [Data]!
    var status : Bool!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [Data]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = Data(fromDictionary: dic)
                data.append(value)
            }
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    
    struct Data{
        
        var age : Int!
        var dob : String!
        var entryBy : String!
        var entryDate : String!
        var freeNotes : String!
        var gender : String!
        var idNo : Int!
        var mrn : Int!
        var name : String!
        var riskCoronaryArteryDiseaseScore : Int!
        var userId : Int!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            age = dictionary["age"] as? Int
            dob = dictionary["dob"] as? String
            entryBy = dictionary["entry_by"] as? String
            entryDate = dictionary["entry_date"] as? String
            freeNotes = dictionary["free_notes"] as? String
            gender = dictionary["gender"] as? String
            idNo = dictionary["id_no"] as? Int
            mrn = dictionary["mrn"] as? Int
            name = dictionary["name"] as? String
            riskCoronaryArteryDiseaseScore = dictionary["risk_coronary_artery_disease_score"] as? Int
            userId = dictionary["user_id"] as? Int
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if age != nil{
                dictionary["age"] = age
            }
            if dob != nil{
                dictionary["dob"] = dob
            }
            if entryBy != nil{
                dictionary["entry_by"] = entryBy
            }
            if entryDate != nil{
                dictionary["entry_date"] = entryDate
            }
            if freeNotes != nil{
                dictionary["free_notes"] = freeNotes
            }
            if gender != nil{
                dictionary["gender"] = gender
            }
            if idNo != nil{
                dictionary["id_no"] = idNo
            }
            if mrn != nil{
                dictionary["mrn"] = mrn
            }
            if name != nil{
                dictionary["name"] = name
            }
            if riskCoronaryArteryDiseaseScore != nil{
                dictionary["risk_coronary_artery_disease_score"] = riskCoronaryArteryDiseaseScore
            }
            if userId != nil{
                dictionary["user_id"] = userId
            }
            return dictionary
        }
        
    }
}
