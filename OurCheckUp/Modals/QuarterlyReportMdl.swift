//
//  QuarterlyReportMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 04/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
import Foundation

struct QuarterlyReportMdl{
    
    var data : [Data]!
    var status : Bool!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [Data]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = Data(fromDictionary: dic)
                data.append(value)
            }
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    struct Data{
        
        var achieved : Int!
        var initial : Int!
        var performance : String!
        var quarter : Int!
        var round : String!
        var target : Int!
        var userId : String!
        var vital : String!
        var year : Int!
        var normalCount:Int!
        var totalCount:Int!
        var cvd : Int!
        var isCvd = false
        var values : String!
        var date : String!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        
        init(vital:String,cvd:Int) {
            self.vital = vital
            self.cvd = cvd
            self.isCvd = true
        }
        
        init(fromDictionary dictionary: [String:Any]){
            achieved = dictionary["achieved"] as? Int
            initial = dictionary["initial"] as? Int
            performance = dictionary["performance"] as? String
            quarter = dictionary["quarter"] as? Int
            round = dictionary["round"] as? String
            target = dictionary["target"] as? Int
            userId = dictionary["user_id"] as? String
            vital = dictionary["vital"] as? String
            year = dictionary["year"] as? Int
            normalCount = dictionary["normal_count"] as? Int
            totalCount = dictionary["total_count"] as? Int
            values = dictionary["values"] as? String
            date = dictionary["date"] as? String
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if achieved != nil{
                dictionary["achieved"] = achieved
            }
            if initial != nil{
                dictionary["initial"] = initial
            }
            if performance != nil{
                dictionary["performance"] = performance
            }
            if quarter != nil{
                dictionary["quarter"] = quarter
            }
            if round != nil{
                dictionary["round"] = round
            }
            if target != nil{
                dictionary["target"] = target
            }
            if userId != nil{
                dictionary["user_id"] = userId
            }
            if vital != nil{
                dictionary["vital"] = vital
            }
            if year != nil{
                dictionary["year"] = year
            }
            if normalCount != nil{
                dictionary["normal_count"] = normalCount
            }
            if totalCount != nil{
                dictionary["total_count"] = normalCount
            }
            
            return dictionary
        }
        
    }
}
