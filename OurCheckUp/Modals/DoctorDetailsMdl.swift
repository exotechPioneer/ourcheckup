//
//  DoctorDetailsMdl.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 18/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct DoctorDetailsMdl{
    
    var data : [Data]!
    var status : Bool!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [Data]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = Data(fromDictionary: dic)
                data.append(value)
            }
        }
        status = dictionary["status"] as? Bool
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    
    struct Data{
        
        var address2 : String!
        var addressOfClinic1 : String!
        var clinicType : String!
        var country : String!
        var dob : String!
        var firstName : String!
        var gender : String!
        var hospital : String!
        var hospitalLicenseNo : String!
        var lastName : String!
        var mobileNo : String!
        var mrn : String!
        var nationalId : String!
        var phoneNo : String!
        var postcode : String!
        var specialization : String!
        var state : String!
        var userId : Int!
        var yearOfRegistration : String!
        
        
        /**
         * Instantiate the instance using the passed dictionary values to set the properties values
         */
        init(fromDictionary dictionary: [String:Any]){
            address2 = dictionary["address_2"] as? String
            addressOfClinic1 = dictionary["address_of_clinic_1"] as? String
            clinicType = dictionary["clinic_type"] as? String
            country = dictionary["country"] as? String
            dob = dictionary["dob"] as? String
            firstName = dictionary["first_name"] as? String
            gender = dictionary["gender"] as? String
            hospital = dictionary["hospital"] as? String
            hospitalLicenseNo = dictionary["hospital_license_no"] as? String
            lastName = dictionary["last_name"] as? String
            mobileNo = dictionary["mobile_no"] as? String
            mrn = dictionary["mrn"] as? String
            nationalId = dictionary["national_id"] as? String
            phoneNo = dictionary["phone_no"] as? String
            postcode = dictionary["postcode"] as? String
            specialization = dictionary["specialization"] as? String
            state = dictionary["state"] as? String
            userId = dictionary["user_id"] as? Int
            yearOfRegistration = dictionary["year_of_registration"] as? String
        }
        
        /**
         * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
         */
        func toDictionary() -> [String:Any]
        {
            var dictionary = [String:Any]()
            if address2 != nil{
                dictionary["address_2"] = address2
            }
            if addressOfClinic1 != nil{
                dictionary["address_of_clinic_1"] = addressOfClinic1
            }
            if clinicType != nil{
                dictionary["clinic_type"] = clinicType
            }
            if country != nil{
                dictionary["country"] = country
            }
            if dob != nil{
                dictionary["dob"] = dob
            }
            if firstName != nil{
                dictionary["first_name"] = firstName
            }
            if gender != nil{
                dictionary["gender"] = gender
            }
            if hospital != nil{
                dictionary["hospital"] = hospital
            }
            if hospitalLicenseNo != nil{
                dictionary["hospital_license_no"] = hospitalLicenseNo
            }
            if lastName != nil{
                dictionary["last_name"] = lastName
            }
            if mobileNo != nil{
                dictionary["mobile_no"] = mobileNo
            }
            if mrn != nil{
                dictionary["mrn"] = mrn
            }
            if nationalId != nil{
                dictionary["national_id"] = nationalId
            }
            if phoneNo != nil{
                dictionary["phone_no"] = phoneNo
            }
            if postcode != nil{
                dictionary["postcode"] = postcode
            }
            if specialization != nil{
                dictionary["specialization"] = specialization
            }
            if state != nil{
                dictionary["state"] = state
            }
            if userId != nil{
                dictionary["user_id"] = userId
            }
            if yearOfRegistration != nil{
                dictionary["year_of_registration"] = yearOfRegistration
            }
            return dictionary
        }
        
    }
}
