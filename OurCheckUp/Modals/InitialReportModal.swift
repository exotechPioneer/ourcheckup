//
//  InitialReportModal.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 29/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import Foundation
struct InitialReportModal{
    var vital : String
    var initial : String
    var target : String
    var performancePercen : String
    var performanceString: String
}
