//
//  DrHealthReportVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class DrHealthReportVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    
    let CELL_ID = "cellid"
    let HEADER_ID = "headerid"
    
    var reports : [MyHealthReportMdl]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: "HEALTH REPORT")
        createData()
        setUpTableView()
    }
    
    func setUpTableView(){
        self.tableView.register("MyHealthReportTC".toNib(), forCellReuseIdentifier: CELL_ID)
        self.tableView.register("MyHealthReportTblHeader".toNib(), forHeaderFooterViewReuseIdentifier: HEADER_ID)
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    func createData(){
        let r1 = MyHealthReportMdl(reportType: "INITIAL REPORT", reports: nil, isSubViewHidden: false)
        let r21 = Report(name: "Quater 1", id: "1")
        let r22 = Report(name: "Quater 2", id: "2")
        let r23 = Report(name: "Quater 3", id: "3")
        let r24 = Report(name: "Quater 4", id: "4")
        
        let r2 = MyHealthReportMdl(reportType: "QUATERLY REPORT", reports: [r21,r22,r23,r24], isSubViewHidden: false)
        
        let r31 = Report(name: "Year 2017", id: "2017")
        let r32 = Report(name: "Year 2018", id: "2018")
        
        let r3 = MyHealthReportMdl(reportType: "YEARLY REPORT", reports: [r31,r32], isSubViewHidden: false)
        
        let r4 = MyHealthReportMdl(reportType: "PROGRESS REPORT", reports: nil, isSubViewHidden: false)
        reports = [r1,r2,r3,r4]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is DrProgressReportVC{
            (segue.destination as! DrProgressReportVC).isDoctor = true
        }
    }
}

//MARK:- UITableViewDataSource
extension DrHealthReportVC : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if let count = reports?.count{
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let report = reports?[section]{
            if  (report.reports != nil) && !report.isSubViewHidden{
                return report.reports!.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return reportNameCell(tableView,cellForRowAt:indexPath)
    }
    
    func reportNameCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! MyHealthReportTC
        if let report = reports?[indexPath.section]{
            cell.configure(indexPath: indexPath,report: report.reports![indexPath.row])
        }
        
        return cell
    }
}
//MARK:- UITableViewDelegate
extension DrHealthReportVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: HEADER_ID) as! MyHealthReportTblHeader
        view.configure(section: section, report: reports![section])
        view.delegate = self
        view.tag = section
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onHeaderTap)))
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(id: .drQuaterlyReport)
    }
    
    @objc func onHeaderTap(gesture:UITapGestureRecognizer){
        if let section = gesture.view?.tag{
            if section == 0{
               self.performSegue(id: .initialReport)
            }else if section == 3{
                self.performSegue(id: .drProgressReport)
            }
        }
    }
}

extension DrHealthReportVC : MyHealthReportTblHeaderDelegate{
    func onDropArrowTapped(isHidden: Bool, section: Int) {
        self.reports?[section].isSubViewHidden = isHidden
        let index = IndexSet(integer: section)
        tableView.reloadSections(index, with: .automatic)
    }
    
}

