//
//  AddFreeNotesVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 14/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit
protocol AddFreeNotesVCDelegate {
    func addFreeNotes(didCancel cancel : Bool)
    func addFreeNotes(didSubmit notes : String)
    
}
class AddFreeNotesVC: BaseVC {

    @IBOutlet weak var btnCancel:UIButton!
    @IBOutlet weak var btnSelect:UIButton!
    @IBOutlet weak var textView:UITextView!
    @IBOutlet weak var lblTitle:UILabel!
    
    var delegate: AddFreeNotesVCDelegate?
    var text:String?
    var titleTop: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if let t = titleTop{
            self.lblTitle.text = t
        }
        textView.text = text
        textView.layer.cornerRadius = 8
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.layer.borderWidth = 1
    }

    
    @IBAction func btnCancelAction(_ sender : UIButton){
        self.dismiss(animated: true)
        delegate?.addFreeNotes(didCancel: true)
    }
    @IBAction func btnSubmitAction(_ sender : UIButton){
        if let text = textView.text{
            if text.isEmpty{
                textView.shake(repeatCount: 1)
            }else{
                delegate?.addFreeNotes(didSubmit: text)
                self.dismiss(animated: true)
            }
        }else{
            textView.shake(repeatCount: 1)
        }
    }

}
