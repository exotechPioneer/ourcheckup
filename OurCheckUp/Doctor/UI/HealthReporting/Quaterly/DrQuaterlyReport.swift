//
//  DrQuaterlyReport.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 14/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit

class DrQuaterlyReport: BaseVC {

    @IBOutlet weak var tableView : UITableView!
    
    var testArray : [InitialReportModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        createTestList()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        setTitle(title: "QUATERLY REPORT")
    }
    
    func createTestList(){
        testArray.append(InitialReportModel(testName: "Height", testUnit: "(ft)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Body Weight", testUnit: "(kg)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Heart Rate", testUnit: "(bpm)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Blood Pressure", testUnit: "(kg)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Systolic", testUnit: "(mmHg)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Diastolic", testUnit: "(mmHg)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Body Mass Index (BMI)", testUnit: "(kg)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Basal Metabolic Rate", testUnit: "(kg/m2)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "(BMR)", testUnit: "()",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Visceral Fat", testUnit: "()",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Body Water", testUnit: "(%)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Muscles Mass", testUnit: "(years)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Metabolic Age", testUnit: "(Avg Steps/Day)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Average Steps taken", testUnit: "(%)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "SPO2", testUnit: "(mmol/L)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Sugar - Fasting Blood", testUnit: "(mmol/L)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Sugar", testUnit: "(mmol/L)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Sugar - HBA1C *", testUnit: "(mmol/L)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "LDL Cholesterol *", testUnit: "(mmol/L)",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
        testArray.append(InitialReportModel(testName: "Total Cholesterol *", testUnit: "",
                                            ideal: 0, initial: 0, Target: 0, isHeading: false))
    }
}
extension DrQuaterlyReport : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell") as! DrQuaterlyRptTblCell
        let test = testArray[indexPath.row]
        cell.lblTest.text = test.testName
        //cell.lblUnit.text = test.testUnit
        return cell
    }
}
