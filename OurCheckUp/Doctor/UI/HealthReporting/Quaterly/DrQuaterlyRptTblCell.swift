//
//  DrQuaterlyRptTblCell.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 14/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit

class DrQuaterlyRptTblCell: UITableViewCell {

    @IBOutlet weak var lblTest:UILabel!
    @IBOutlet weak var tfIdeal:UITextField!
    @IBOutlet weak var tfTargetInitial:UITextField!
    @IBOutlet weak var tfCurrent:UITextField!
    @IBOutlet weak var tfInitial:UITextField!
    @IBOutlet weak var tfTargetNew:UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
