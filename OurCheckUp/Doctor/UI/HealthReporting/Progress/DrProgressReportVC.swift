//
//  DrProgressReportVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 14/01/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit

class DrProgressReportVC: BaseVC {
    
    var isDoctor = false
    @IBOutlet weak var lblEntrydate:UILabel!
    @IBOutlet weak var lblEntryBy:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var tfMRN:UITextField!
    @IBOutlet weak var lblDOB:UILabel!
    @IBOutlet weak var lblIDnum:UILabel!
    @IBOutlet weak var lblGender:UILabel!
    @IBOutlet weak var lblAge:UILabel!
    @IBOutlet weak var tfRiskOfCVD:UITextField!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var btnAddFreeNotes : UIButtonX!
    @IBOutlet weak var btnSubmit : UIButtonX!
    
    var freeNotesArray : [String]?
    
    var subscriberId : Int!
    
    var subsName : String?
    var subsDob : String?
    var subsCreateOn : String?
    var subsIdNo : Int?
    var subsGender: String?
    var subsAge: Int?
    var subsMrn: Int?
    var subsCVD : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "PROGRESS REPORT")
        
        if !isDoctor{
            self.tfMRN.isEnabled = false
            self.tfMRN.isUserInteractionEnabled = false
            self.getProgressReport()
            self.btnAddFreeNotes.isHidden = true
            self.btnSubmit.isHidden = true
        }else{
            self.showSubcriberIdAlert {(id) in
                self.subscriberId = id
                self.getSubscriberDetails()
            }
        }
    }
    
    @IBAction func btnAddFreeNotesAction(_ sender:UIButtonX){
        let freeNotesVC = AddFreeNotesVC()
        freeNotesVC.modalTransitionStyle = .coverVertical
        freeNotesVC.modalPresentationStyle = .overCurrentContext
        freeNotesVC.delegate = self
        self.navigationController?.present(freeNotesVC, animated: true)
    }
    
    @IBAction func btnSubmitAction(_ sender:UIButtonX){
        if freeNotesArray == nil || freeNotesArray!.count == 0{
            showError(error: "Please fill free notes.")
        }else{
            createProgressReport()
        }
    }
    
    
    func getProgressReport(){
        let userID:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        let param = ["user_id":userID]
        self.showProgress()
        Api.share.getProgressReport(params: param) { (model, status) in
            self.hideProgress()
            if status{
                if let model = model{
                    if model.data.count > 0{
                        let item = model.data[0]
                        DispatchQueue.main.async {
                            self.lblAge.text = "Age: \(item.age ?? 0)"
                            self.lblName.text = item.name
                            self.lblIDnum.text = "\(item.idNo ?? 0)"
                            self.lblGender.text = item.gender
                            if let dob = item.dob{
                                self.lblDOB.text = dob.components(separatedBy: "T")[0]
                                self.lblDOB.sizeToFit()
                            }else{
                                self.lblDOB.text = ""
                            }
                            if let entryBy = item.entryBy{
                                self.lblEntryBy.text = "Entry By: "+entryBy.components(separatedBy: "T")[0]
                                self.lblEntryBy.sizeToFit()
                            }
                            if let entryDate = item.entryDate{
                                self.lblEntrydate.text = entryDate.components(separatedBy: "T")[0]
                            }
                            self.tfMRN.text = "\(item.mrn ?? 0)"
                            if let cvd = item.riskCoronaryArteryDiseaseScore{
                                self.tfRiskOfCVD.text = "\(cvd)%"
                            }else{
                                self.tfRiskOfCVD.text = ""
                            }
                            
                            if let freeNotes = item.freeNotes{
                                self.freeNotesArray = [freeNotes]
                                self.tableView.reloadData()
                            }
                        }
                    }else{
                        showError(error: "Progress report is not avaialble")
                    }
                    
                }
            }
        }
    }
    
    func createProgressReport(){
        if let mrnTxt = self.tfMRN.text,let cvd = tfRiskOfCVD.text{
            if mrnTxt.isEmpty{
                showError(error: "Please enter MRN")
                return
            }else if cvd.isEmpty{
                showError(error: "Please enter CVD")
                return
            }
            self.subsCVD = Int(cvd)
            self.subsMrn = Int(mrnTxt)
        }
        var params = [String:Any]()
        params["user_id"] = subscriberId!
        params["name"] = self.subsName!
        params["age"] = self.subsAge!
        params["mrn"] = self.subsMrn!
        params["dob"] = self.subsDob!
        params["id_no"] = self.subsIdNo!
        params["entry_date"] = Date().dateString(withFormat: "yyyy-MM-dd")
        params["entry_by"] = Date().dateString(withFormat: "yyyy-MM-dd")
        params["risk_coronary_artery_disease_score"] = self.subsCVD!
        if freeNotesArray != nil, freeNotesArray!.count > 0{
            params["free_notes"] = freeNotesArray![0]
        }
        self.showProgress()
        Api.share.createProgressReport(params: params) { (msg, status) in
            self.hideProgress()
            if status{
                displayAlert("Success", andMessage: msg ?? "")
            }else{
                displayAlert("Error", andMessage: "Unable to submit report. Report already exist.")
            }
        }
    }
    
    func getSubscriberDetails(){
        let params:[String:Any] = ["user_id":subscriberId]
        self.showProgress()
        Api.share.getSubscriberDetails(params: params) { (model, status) in
            self.hideProgress()
            if status{
                if let data = model?.data{
                    if data.count > 0{
                        let element = data[0]
                        DispatchQueue.main.async {
                            var fullName = ""
                            if let fN = element.firstName{
                                fullName = fN
                            }
                            if let lN = element.lastName{
                                fullName = fullName+" "+lN
                            }
                            self.lblName.text = fullName
                            self.lblGender.text = element.gender as? String
                            
                            self.lblIDnum.text = "\(element.userId ?? 0)"
                            self.lblEntryBy.text = "Enter By: "+Date().dateString(withFormat: "yyyy-MM-dd")
                            self.lblEntrydate.text = Date().dateString(withFormat: "yyyy-MM-dd")
                            self.subsName = fullName
                            self.subsGender = element.gender as? String ?? ""
                            self.subsIdNo = element.userId
                            if let dob = element.dob
                            {
                                let dateStr = dob.components(separatedBy: "T")[0]
                                self.lblDOB.text = dateStr
                                let formater = DateFormatter()
                                formater.dateFormat = "yyyy-MM-dd"
                                if let birthDate = formater.date(from: dateStr){
                                    self.subsAge = self.getAgeFromBirthDate(birthDate: birthDate)
                                    self.lblAge.text = "Age:\(self.subsAge ?? 0)"
                                }
                                self.subsDob = dob
                            }else{
                                self.subsDob = ""
                            }
                        }
                    }else{
                        displayAlert("Error!", andMessage: "Subscriber details are not avaialble.", completion: {
                            self.navigationController?.popViewController(animated: false)
                        })
                    }
                }
            }
        }
    }
}


extension DrProgressReportVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let notes = freeNotesArray{
            return notes.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = freeNotesArray?[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
        return cell
    }
}

extension DrProgressReportVC : AddFreeNotesVCDelegate{
    func addFreeNotes(didCancel cancel: Bool) {
        
    }
    
    func addFreeNotes(didSubmit notes: String) {
        self.freeNotesArray = [notes]
        self.tableView.reloadData()
    }
    
    
}
