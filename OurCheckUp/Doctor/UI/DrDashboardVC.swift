//
//  SubscriberDashboardVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 01/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class DrDashboardVC: BaseVC {
    
    @IBOutlet weak var viewMyHealthRrpt: UIViewX!
    @IBOutlet weak var viewMyMedicalSmry: UIViewX!
    @IBOutlet weak var viewMyAppointment: UIViewX!
    @IBOutlet weak var viewMyAccount: UIViewX!
    @IBOutlet weak var viewPaymentHistory: UIViewX!
    @IBOutlet weak var viewAnnouncement: UIViewX!
    @IBOutlet weak var viewFeedback: UIViewX!
    
    var subscriberId:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapActions()
        createTitleLogo()
        addSideMenuButton()
        setUpNavLogo()
        
        let profileUpdated = AppPreferences.share.get(forkey: .profileUpdated) ?? false
        if !profileUpdated{
            self.performSegue(id: .doctorProfile)
        }
    }
    
    @IBAction func btnMenuAction(){
        DrSideDrawerVC.share?.drawerState = .opened
    }
    
    func addTapActions(){
        self.viewMyHealthRrpt.addTapAction { [unowned self] (view) in
            self.performSegue(id: .healthReport)
        }
        self.viewMyMedicalSmry.addTapAction { [unowned self]  (view) in
            self.showSubcriberIdAlert(completion: { (id) in
                self.subscriberId = id
                self.performSegue(id: .drMedicalSummary)
            })
        }
        
        self.viewMyAppointment.addTapAction { [unowned self]  (view) in
           // self.performSegue(id: .medicalSummary)
        }
        self.viewPaymentHistory.addTapAction{ [unowned self]  (view) in
            self.performSegue(id: .drPayment)
        }
        self.viewMyAccount.addTapAction{ [unowned self]  (view) in
            self.performSegue(id: .drEmpanelment)
        }
    }
    
    func createTitleLogo(){
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 45))
        imageView.image = #imageLiteral(resourceName: "ourcheckup_logo")
        self.navigationController?.navigationItem.titleView = imageView
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is DrMedicalSummaryVC{
            (segue.destination as! DrMedicalSummaryVC).subcriberId = self.subscriberId
        }
        
        if let vc = segue.destination as? DrProfileVC{
            vc.isHideBackBtn = true
        }
    }
}
