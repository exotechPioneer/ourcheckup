//
//  SideMenuOptionsVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 06/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class DrSideMenuOptionsVC: BaseVC {
    
    enum Option : String {
        case Home = "Home"
        case Notification = "Notification"
        case Account_Profile = "Account/Profile"
        case Version = "Version"
        case TermsofService = "Terms of Service"
        case PrivacyPolicy = "Privacy Policy"
        case Setting = "Setting"
        case Help_Support = "Help & Support"
        case Logout = "Logout"
    }
    
    @IBOutlet weak var tableView : UITableView!
    
    var menuOptions:[Option] = [.Home,.Account_Profile,.Notification,.Version,.TermsofService,.PrivacyPolicy,.Setting,.Help_Support,.Logout]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let parent = self.parent as? DrSideDrawerVC{
            //if parent.isDoctorMod{
            menuOptions = [.Home,.Account_Profile,.Notification,.Version,.TermsofService,.PrivacyPolicy,.Setting,.Help_Support,.Logout]
            //}
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        
    }
    
}
//MARK:- UITableViewDataSource
extension DrSideMenuOptionsVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return reportNameCell(tableView,cellForRowAt:indexPath)
    }
    
    func reportNameCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = menuOptions[indexPath.row].rawValue
        return cell!
    }
}
//MARK:- UITableViewDelegate
extension DrSideMenuOptionsVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch menuOptions[indexPath.row] {
        case .Home:
            if let controllers = self.navigationController?.viewControllers{
                for c in controllers{
                    if c is DrDashboardVC{
                        navigationController?.popToViewController(c, animated: true)
                        break
                    }
                }
            }
            break
        case .Account_Profile:
            self.performSegue(id: .doctorProfile)
            break
        case .Notification:
            break
        case .Version:
            displayAlert(Option.Version.rawValue, andMessage: appVersion())
            break
        case .PrivacyPolicy:
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "PrivacyPolicyVC")
            self.navigationController?.pushViewController(vc, animated: true)
            //self.performSegue(id: .privacypolicy)
            break
        case .Setting:
            let st = UIStoryboard(name: "Doctor", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "SettingsVC")
            self.navigationController?.pushViewController(vc, animated: true)
        case .Logout:
            AppPreferences.share.commit(data: false, forkey: .isLogin)
            AppPreferences.share.clear()
            self.dismiss(animated: true)
            break
        default:
            break
        }
        DrSideDrawerVC.share?.closeDrawer()
    }
    
}
