//
//  SideDrawerVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 01/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class DrSideDrawerVC: KYDrawerController {

    var isDoctorMod : Bool = false
    
    public static var share : DrSideDrawerVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DrSideDrawerVC.share = self
        
        drawerDirection = .right
       
        //addSideMenuButton()
        //setUpNavLogo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        addSideMenuButton()
    }

    func addSideMenuButton(){
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btn.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        btn.addTarget(self, action: #selector(didMenuBtnTapped(_:)), for: .touchUpInside)
        let barBtn = UIBarButtonItem(customView: btn)
        self.navigationItem.rightBarButtonItem = barBtn
    }
    func setUpNavLogo() {
        let logoImage = #imageLiteral(resourceName: "ourcheckup_logo")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x:-40,y: 0,width: 150,height: 50)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -45
        navigationItem.leftBarButtonItems = [negativeSpacer,imageItem]
    }
    
    @objc func didMenuBtnTapped(_ sender : UIButton){
        if drawerState == .opened{
            drawerState = .closed
        }else{
            drawerState = .opened
        }
        
    }
   
    func closeDrawer(){
        drawerState = .closed
    }
}
