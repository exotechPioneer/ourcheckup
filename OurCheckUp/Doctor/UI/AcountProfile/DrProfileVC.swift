//
//  ProfileVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 04/02/19.
//  Copyright © 2019 Avtar Singh. All rights reserved.
//

import UIKit
import DropDown
class DrProfileVC: BaseVC {

    @IBOutlet weak var tfFirstName:UITextField!
    @IBOutlet weak var tfLastName:UITextField!
    @IBOutlet weak var tfMRN:UITextField!
    @IBOutlet weak var tfNationality:UITextField!
    @IBOutlet weak var tfGender:UIButton!
    @IBOutlet weak var tfDOB:UITextField!
    @IBOutlet weak var tfAddress1:UITextField!
    @IBOutlet weak var tfAddress2:UITextField!
    @IBOutlet weak var tfPostalCode:UITextField!
    @IBOutlet weak var tfState:UITextField!
    @IBOutlet weak var tfCountry:UITextField!
    @IBOutlet weak var tfPhoneNumer:UITextField!
    @IBOutlet weak var tfMobileNo:UITextField!
    @IBOutlet weak var tfYearOfReg:UITextField!
    @IBOutlet weak var tfClinicType:UITextField!
    @IBOutlet weak var tfSpecialization:UITextField!
    @IBOutlet weak var tfHospital:UITextField!
    @IBOutlet weak var tfHospitalLicenceNo:UITextField!
    
    @IBOutlet weak var btnSave:UIButtonX!
    
    let dropDown = DropDown()
    
    var isHideBackBtn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Profile")
        
        getDetails()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.tfGender.setTitle(item, for: .normal)
        }
        
        if isHideBackBtn{
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
    }
    
    @IBAction func btnGenderAction(_ sender: UIButton){
        dropDown.anchorView = sender
        dropDown.dataSource = ["Male", "Female"]
        dropDown.show()
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton){
        validateAndSave()
    }
    
    func getDetails(){
        let userId:Int = AppPreferences.share.get(forkey: .userID) ?? 0
        let firstName:String? = AppPreferences.share.get(forkey: .userFirstName)
        let lastName: String? = AppPreferences.share.get(forkey: .userLastName)
        
        self.tfFirstName.text = firstName
        self.tfLastName.text = lastName
       
        
        let param = ["user_id" : userId]
        self.showProgress()
        Api.share.getDoctorDetails(params: param) { (model, status) in
            self.hideProgress()
            if status{
                if let data = model?.data{
                    if data.count > 0{
                        let element = data[0]
                        DispatchQueue.main.async {
                         
                            self.tfFirstName.text = element.firstName
                            self.tfLastName.text = element.lastName
                            self.tfNationality.text = element.nationalId
                            if let gender = element.gender {
                                self.tfGender.setTitle(gender, for: .normal)
                            }
                            if let dobstr = element.dob{
                                let str = dobstr.components(separatedBy: "T")[0]
                                self.tfDOB.text = str
                            }
                            self.tfAddress1.text = element.addressOfClinic1
                            self.tfAddress2.text = element.address2
                            self.tfPostalCode.text = element.postcode
                            self.tfState.text = element.state
                            self.tfCountry.text = element.country
                            self.tfMobileNo.text = element.mobileNo
                            self.tfPhoneNumer.text = element.phoneNo
                            self.tfYearOfReg.text = element.yearOfRegistration
                            self.tfClinicType.text = element.clinicType
                            self.tfSpecialization.text = element.specialization
                            self.tfHospital.text = element.hospital
                            self.tfHospitalLicenceNo.text = element.hospitalLicenseNo
                            self.tfMRN.text = element.mrn
                        }
                    }else{
                        showError(error: "No details avaiable!")
                    }
                }
            }
        }
    }
    
    func validateAndSave(){
        let firstName = tfFirstName.text!
        let lastName = tfLastName.text!
        let mrn = tfMRN.text!
        let nationalId = self.tfNationality.text!
        let gender =    self.tfGender.titleLabel!.text!
        let dob = self.tfDOB.text!
        let address1 = self.tfAddress1.text!
        let address2 = tfAddress2.text!
        let postalCode = tfPostalCode.text!
        let state = tfState.text!
        let country = tfCountry.text!
        let mobile = tfMobileNo.text!
        let phoneNo = tfPhoneNumer.text!
        let yearOfReg = tfYearOfReg.text!
        let clinicType = self.tfClinicType.text!
        let specialization = self.tfSpecialization.text!
        let hospital = self.tfHospital.text!
        let hostpitalLiecence = self.tfHospitalLicenceNo.text!
        if firstName.isEmpty{
            showError(error: "Please enter firstname")
            return
        }
        if lastName.isEmpty{
            showError(error: "Please enter lastname")
            return
        }
        if mrn.isEmpty{
            showError(error: "Please enter MRN")
            return
        }
        if nationalId.isEmpty{
            showError(error: "Please enter national id")
            return
        }
        if gender.contains("Select"){
            showError(error: "Please select gender")
            return
        }
        if dob.isEmpty{
            showError(error: "Please enter Date of Birth")
            return
        }
        if address1.isEmpty
        {
            showError(error: "Please enter address 1")
            return
        }
        if address2.isEmpty
        {
            showError(error: "Please enter address 2")
            return
        }
        if postalCode.isEmpty
        {
            showError(error: "Please enter postal code")
            return
        }
        if state.isEmpty
        {
            showError(error: "Please enter state name")
            return
        }
        if country.isEmpty
        {
            showError(error: "Please enter country name")
            return
        }
        if mobile.isEmpty
        {
            showError(error: "Please enter mobile")
            return
        }
        if phoneNo.isEmpty
        {
            showError(error: "Please enter phone number")
            return
        }
        if yearOfReg.isEmpty
        {
            showError(error: "Please enter year of registration")
            return
        }
        if clinicType.isEmpty
        {
            showError(error: "Please enter clinic type")
            return
        }
        if specialization.isEmpty
        {
            showError(error: "Please enter specialization")
            return
        }
        if hospital.isEmpty
        {
            showError(error: "Please enter hospital name")
            return
        }
        if hostpitalLiecence.isEmpty
        {
            showError(error: "Please enter hospital license number")
            return
        }
        
        let params: [String:Any] = ["first_name": firstName,
                                    "last_name": lastName,
                                    "national_id": nationalId,
                                    "dob": dob,
                                    "gender": gender,
                                    "mrn": mrn,
                                    "year_of_registration": yearOfReg,
                                    "clinic_type": clinicType,
                                    "specialization": specialization,
                                    "hospital": hospital,
                                    "hospital_license_no": hostpitalLiecence,
                                    "address_of_clinic_1": address1,
                                    "address_2": address2,
                                    "postcode": postalCode,
                                    "state": state,
                                    "country": country,
                                    "phone_no": phoneNo,
                                    "mobile_no": mobile,
                                    "user_id": getUserID()]
        
        self.showProgress()
        Api.share.updateDoctorDetails(params: params) { (msg, status) in
            self.hideProgress()
            if status{
                AppPreferences.share.commit(data: true, forkey: .profileUpdated)
                self.navigationItem.setHidesBackButton(false, animated: true)
                displayAlert("Success", andMessage: msg ?? "")
            }else{
                showError(error: "Error occured")
            }
        }
    }
}
