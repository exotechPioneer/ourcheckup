//
//  SubscriberMedicalSummaryTC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

protocol DrMedicalSummaryTCDelegate {
    func drMedicalSummaryTC(didSinceYearTap indexPath:IndexPath)
}

class DrMedicalSummaryTC: UITableViewCell {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblSubText: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnRadio:UIButton!
    @IBOutlet weak var leftConstraint:NSLayoutConstraint!
    var delegate : DrMedicalSummaryTCDelegate?
    var indexPath : IndexPath!
    var child : SubscriberMedicalSummaryMdl.Child!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(child:SubscriberMedicalSummaryMdl.Child,indexPath:IndexPath){
        self.child = child
        self.lblDesc.text = child.name
        self.lblTime.text = child.time
        self.indexPath = indexPath
        
        self.lblTime.isUserInteractionEnabled = true
        self.lblTime.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(lblTimeTapAction)))
        
        if child.level > 0{
            self.leftConstraint.constant = CGFloat(30 * child.level)
        }else{
            self.leftConstraint.constant = 8.0
        }
        
        if child.isSubChilds{
            btnRadio.isHidden = true
        }else{
            btnRadio.isHidden = false
        }
        
        if child.needTextInput && child.inputText != nil
            && !child.inputText!.isEmpty{
            self.lblSubText.text = child.inputText
        }else{
             self.lblSubText.text = ""
        }
        
        if child.isChecked{
            self.btnRadio.setImage(UIImage(named: "radio_on_button"), for: .normal)
        }else{
            self.btnRadio.setImage(UIImage(named: "radio_off_button"),for: .normal)
        }
    }
    
    @objc func lblTimeTapAction(){
        if child.isChecked{
            delegate?.drMedicalSummaryTC(didSinceYearTap: indexPath)

        }
    }
}
