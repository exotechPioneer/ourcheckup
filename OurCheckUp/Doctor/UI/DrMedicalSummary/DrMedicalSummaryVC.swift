//
//  DrMedicalSummaryVC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 26/12/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

class DrMedicalSummaryVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDob:UILabel!
    @IBOutlet weak var lblCreateOn:UILabel!
    @IBOutlet weak var lblIDNo:UILabel!
    @IBOutlet weak var lblGender:UILabel!
    @IBOutlet weak var lblAge:UILabel!
    @IBOutlet weak var tfMRN:UITextField!
    
    var subsName : String?
    var subsDob : String?
    var subsCreateOn : String?
    var subsIdNo : Int?
    var subsGender: String?
    var subsAge: Int?
    var subsMrn: Int?
    
    var dataArray : [SubscriberMedicalSummaryMdl]?
    
    var CELL_ID = "CELLID"
    var HEADER_ID = "HeaderID"
    var subcriberId : Int!
    
    var indexForTextInput : IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: "MEDICAL SUMMARY")
        createData()
        setUpTable()
        getSubscriberDetails()
    }
    
    @IBAction func btnSubmitAction(_ sender:UIButtonX){
        createApiRequestParams()
    }
    
    func createData(){
        let c10 = SubscriberMedicalSummaryMdl.Child(name: "Nil", time: "Since Year")
        let c11 = SubscriberMedicalSummaryMdl.Child(name: "Hypertension", time: "Since Year")
       
        let c12 = SubscriberMedicalSummaryMdl.Child(name: "Diabetes", time: "Since Year")
        let c13 = SubscriberMedicalSummaryMdl.Child(name: "High Cholesterol", time: "Since Year")
        let c14 = SubscriberMedicalSummaryMdl.Child(name: "Smoking", time: "Since Year")
        let c15 = SubscriberMedicalSummaryMdl.Child(name: "Stroke", time: "Since Year")
        let c16 = SubscriberMedicalSummaryMdl.Child(name: "Chronic Kidney Disease", time: "Since Year")
        let c17 = SubscriberMedicalSummaryMdl.Child(name: "Family History of CAD", time: "Since Year")
        var m1 = SubscriberMedicalSummaryMdl(header: "Significant Medical History", childs: [c10,c11,c12,c13,c14,c15,c16,c17])
        m1.apiParamName = "significant_medical_history"
        
        let c21 = SubscriberMedicalSummaryMdl.Child(name: "Angioplasty", time: "Since Year")
        var c22 = SubscriberMedicalSummaryMdl.Child(name: "Heart Surgery", time: "Since Year")
        c22.isSubChilds = true
        c22.apiParamName = "heart_surgery"
        c22.nodeId = 1
        var c221 = SubscriberMedicalSummaryMdl.Child(name: "Bypass Surgery", time: "Since Year",level:1)
        c221.parentNodeID = c22.nodeId
        var c222 = SubscriberMedicalSummaryMdl.Child(name: "Valve Surgery", time: "Since Year",level:1)
        c222.parentNodeID = c22.nodeId
        var c223 = SubscriberMedicalSummaryMdl.Child(name: "Aortic Surgery", time: "Since Year",level:1)
        c223.parentNodeID = c22.nodeId
        var c224 = SubscriberMedicalSummaryMdl.Child(name: "Congenital Heart Surgery", time: "Since Year",level:1)
        c224.parentNodeID = c22.nodeId
        var c225 = SubscriberMedicalSummaryMdl.Child(name: "Pacemaker Implementation", time: "Since Year",level:1)
        c225.parentNodeID = c22.nodeId
        let c23 = SubscriberMedicalSummaryMdl.Child(name: "AICD Implantation", time: "Since Year")
        var c24 = SubscriberMedicalSummaryMdl.Child(name: "Others", time: "Since Year")
        c24.needTextInput = true
        
        var m2 = SubscriberMedicalSummaryMdl(header: "Past Medical History", childs: [c21,c22,c221,c222,c223,c224,c225,c23,c24])
        m2.apiParamName = "past_surgical_history"
        
        let c31 = SubscriberMedicalSummaryMdl.Child(name: "Amoxicillin", time: "Since Year")
        let c32 = SubscriberMedicalSummaryMdl.Child(name: "Pencillin", time: "Since Year")
        var m3 = SubscriberMedicalSummaryMdl(header: "Drug Allergies", childs: [c31,c32])
        m3.apiParamName = "drug_allergies"
        
        let c41 = SubscriberMedicalSummaryMdl.Child(name: "Plevix - 75 mg", time: "Since Year")
        let c42 = SubscriberMedicalSummaryMdl.Child(name: "Crestor - 5 mg", time: "Since Year")
        var m4 = SubscriberMedicalSummaryMdl(header: "Current Medication", childs: [c41,c42])
        m4.apiParamName = "current_medication"
        
        dataArray = [m1,m2,m3,m4]
    }
    
    func setUpTable(){
        self.tableView.register("DrMedicalSummaryTC".toNib(), forCellReuseIdentifier: CELL_ID)
        self.tableView.register("DrMedicalSummaryTblHeader".toNib(), forHeaderFooterViewReuseIdentifier: HEADER_ID)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
    }
    
    func createApiRequestParams(){
        var isReadyToSubmit = true
        if let mrnTxt = self.tfMRN.text{
            if mrnTxt.isEmpty{
                showError(error: "Please enter MRN")
                return
            }
            self.subsMrn = Int(mrnTxt)
        }
        
        var params = [String:Any]()
        var subChildApiKeyName = ""
        var nodeId = 0
       
        for model in dataArray!{
            var array = [String]()
            for c in model.childs!{
                if c.isSubChilds{
                    nodeId = c.nodeId
                    subChildApiKeyName = c.apiParamName ?? ""
                    params[subChildApiKeyName] = []
                } else if c.isChecked && c.nodeId == 0
                    && c.parentNodeID == 0 && !c.needTextInput{
                    if c.time == "Since Year"{
                        isReadyToSubmit = false
                        showSelectSinceYearError(valueName: c.name)
                        break
                    }
                    array.append(c.name+":"+c.time)
                }
                else if nodeId > 0 && c.parentNodeID > 0
                    && nodeId == c.parentNodeID && c.isChecked{
                    if c.time == "Since Year"{
                        isReadyToSubmit = false
                        showSelectSinceYearError(valueName: c.name)
                        break
                    }
                    if var value = params[subChildApiKeyName] as? [String]{
                        value.append(c.name+":"+c.time)
                        params[subChildApiKeyName] = value
                    }else{
                        params[subChildApiKeyName] = [c.name+":"+c.time]
                    }
                }else if c.needTextInput && c.inputText != nil
                    && !c.inputText!.isEmpty && c.isChecked{
                    params["others"] = c.inputText!
                }
            }
            params[model.apiParamName!] = array
        }
        
        params["created_on"] = Date().dateString(withFormat: "yyyy-MM-dd")
        params["user_id"] = subcriberId
        params["name"] = self.lblName.text ?? ""
        params["age"] = self.subsAge!
        params["mrn"] = self.subsMrn!
        params["dob"] = self.subsDob!
        params["id_no"] = self.subsIdNo
        params["gender"] = self.subsGender!
        params["since_year"] = "Since 2018"
        
        if isReadyToSubmit{
            self.showProgress()
            Api.share.createMedicalSummary(params: params) { (msg, status) in
                self.hideProgress()
                if status{
                    displayAlert("Message", andMessage: msg ?? "empty reponse")
                }
            }
        }
    }
    
    func showSelectSinceYearError(valueName:String){
        showError(error: "Please select since year for "+valueName)
    }
    
    func getSubscriberDetails(){
        let params:[String:Any] = ["user_id":subcriberId]
        self.showProgress()
        Api.share.getSubscriberDetails(params: params) { (model, status) in
            self.hideProgress()
            if status{
                if let data = model?.data{
                    if data.count > 0{
                        let element = data[0]
                        DispatchQueue.main.async {
                            var fullName = ""
                            if let fN = element.firstName{
                                fullName = fN
                            }
                            if let lN = element.lastName{
                                fullName = fullName+" "+lN
                            }
                            self.lblName.text = fullName
                            self.lblGender.text = element.gender
                            
                            self.lblIDNo.text = "\(element.userId ?? 0)"
                            self.subsName = fullName
                            self.subsGender = element.gender ?? ""
                            self.subsIdNo = element.userId
                            
                            if let dob = element.dob
                            {
                                let dateStr = dob.components(separatedBy: "T")[0]
                                self.lblDob.text = dateStr
                                let formater = DateFormatter()
                                formater.dateFormat = "yyyy-MM-dd"
                                if let birthDate = formater.date(from: dateStr){
                                    self.subsAge = self.getAgeFromBirthDate(birthDate: birthDate)
                                    self.lblAge.text = "Age: \(self.subsAge ?? 0)"
                                }
                                self.subsDob = dob
                            }
                        }
                    }else{
                        displayAlert("Error!", andMessage: "Subscriber details are not avaialble.", completion: {
                            self.navigationController?.popViewController(animated: false)
                        })
                    }
                }
            }
        }
    }
    
  
}
//MARK:- UITableViewDataSource
extension DrMedicalSummaryVC : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if let count = dataArray?.count{
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = dataArray?[section]{
            if  (data.childs != nil){
                return data.childs!.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return setCell(tableView,cellForRowAt:indexPath)
    }
    
    func setCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! DrMedicalSummaryTC
        cell.configure(child: dataArray![indexPath.section].childs![indexPath.row],indexPath: indexPath)
        cell.delegate = self
        return cell
    }
}
//MARK:- UITableViewDelegate
extension DrMedicalSummaryVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: HEADER_ID) as! DrMedicalSummaryTblHeader
        view.configure(model: dataArray![section], indexPath: IndexPath(row: 0, section: section))
        view.delegate = self
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let isYes = self.dataArray?[indexPath.section].isYes,isYes{
            let child = self.dataArray![indexPath.section].childs![indexPath.row]
            let isChecked = child.isChecked
            if child.needTextInput{
                self.indexForTextInput = indexPath
                let vc = AddFreeNotesVC()
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                vc.text = child.inputText
                vc.titleTop = "Others"
                vc.delegate = self
                self.present(vc, animated: true)
            }
            else if !child.isSubChilds{
                self.dataArray![indexPath.section].childs![indexPath.row].isChecked = !isChecked
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
}

extension DrMedicalSummaryVC : DrMedicalSummaryTblHeaderDelegate{
    func drMedicalSummaryTblHeader(didUpdate model: SubscriberMedicalSummaryMdl, indexPath: IndexPath) {
        self.dataArray?[indexPath.section] = model
        self.tableView.reloadData()
    }
}

extension DrMedicalSummaryVC : AddFreeNotesVCDelegate{
   
    func addFreeNotes(didCancel cancel: Bool) {
        
    }
    
    func addFreeNotes(didSubmit notes: String) {
        if indexForTextInput != nil{
            self.dataArray![self.indexForTextInput.section].childs![self.indexForTextInput.row].inputText = notes
            self.dataArray![indexForTextInput.section].childs![indexForTextInput.row].isChecked = true
            self.tableView.reloadData()
        }
    }
}

extension DrMedicalSummaryVC : DrMedicalSummaryTCDelegate
{
    func drMedicalSummaryTC(didSinceYearTap indexPath: IndexPath) {
        self.indexForTextInput = indexPath
        let vc = YearPickerVC()
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: true)
    }
}
extension DrMedicalSummaryVC : YearPickerVCDelegate{
    func yearPicker(didCancel cancel: Bool) {
        
    }
    func yearPicker(didSelect year: String) {
        self.dataArray![indexForTextInput.section].childs![indexForTextInput.row].time = "Since \(year)"
        self.tableView.reloadData()
    }
}
