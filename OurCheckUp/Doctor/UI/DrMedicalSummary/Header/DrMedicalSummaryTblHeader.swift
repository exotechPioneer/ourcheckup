//
//  SubscriberMedicalSummaryTC.swift
//  OurCheckUp
//
//  Created by Avtar Singh on 27/11/18.
//  Copyright © 2018 Avtar Singh. All rights reserved.
//

import UIKit

protocol DrMedicalSummaryTblHeaderDelegate {
    func drMedicalSummaryTblHeader(didUpdate model:SubscriberMedicalSummaryMdl,indexPath:IndexPath)
}

class DrMedicalSummaryTblHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    var model : SubscriberMedicalSummaryMdl!
    var indexPath:IndexPath!
    
    var delegate:DrMedicalSummaryTblHeaderDelegate?
    
    func configure(model:SubscriberMedicalSummaryMdl,indexPath:IndexPath){
        self.model = model
        self.indexPath = indexPath
        
        lblHeader.text = model.header
        
        if let isYes =  model.isYes,isYes{
            self.btnYes.setImage(UIImage(named: "radio_on_button"), for: .normal)
            self.btnNo.setImage(UIImage(named: "radio_off_button"), for: .normal)
        }else{
            self.btnNo.setImage(UIImage(named: "radio_on_button"), for: .normal)
            self.btnYes.setImage(UIImage(named: "radio_off_button"), for: .normal)
        }
    }
    
    @IBAction func btnYesAction(_ sender:UIButton){
        self.model.isYes = true
        self.btnYes.setImage(UIImage(named: "radio_on_button"), for: .normal)
        self.btnNo.setImage(UIImage(named: "radio_off_button"), for: .normal)
        delegate?.drMedicalSummaryTblHeader(didUpdate: model, indexPath: indexPath)
    }
    
    @IBAction func btnNoAction(_ sender:UIButton){
        self.model.isYes = false
        self.btnNo.setImage(UIImage(named: "radio_on_button"), for: .normal)
        self.btnYes.setImage(UIImage(named: "radio_off_button"), for: .normal)
        for i in 0..<model.childs!.count{
            model.childs![i].isChecked = false
        }
        delegate?.drMedicalSummaryTblHeader(didUpdate: model, indexPath: indexPath)
    }
}
